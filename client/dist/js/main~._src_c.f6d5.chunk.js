(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main~._src_c"],{

/***/ "./src/code/SMTP.ts":
/*!**************************!*\
  !*** ./src/code/SMTP.ts ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Worker = void 0;
const axios_1 = __importDefault(__webpack_require__(/*! axios */ "./node_modules/axios/index.js"));
const config_1 = __webpack_require__(/*! ./config */ "./src/code/config.ts");
class Worker {
    sendMessage(inTo, inFrom, inSubject, inMessage) {
        return __awaiter(this, void 0, void 0, function* () {
            yield axios_1.default.post(`${config_1.config.serverAddress}/messages`, {
                to: inTo,
                from: inFrom,
                subject: inSubject,
                text: inMessage
            }).then(response => response.data)
                .then(response => {
                console.log('Success:', response);
                document.documentElement.style.setProperty('--display-form', 'none');
                document.documentElement.style.setProperty('--display-success', 'block');
            })
                .catch(err => {
                console.log('Error: ', err);
                document.documentElement.style.setProperty('--display-form', 'none');
                document.documentElement.style.setProperty('--display-error', 'block');
            });
        });
    }
}
exports.Worker = Worker;


/***/ }),

/***/ "./src/code/components/BaseLayout.tsx":
/*!********************************************!*\
  !*** ./src/code/components/BaseLayout.tsx ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const react_error_boundary_1 = __webpack_require__(/*! react-error-boundary */ "./node_modules/react-error-boundary/dist/react-error-boundary.umd.js");
const Home = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./layoutComponents/Home */ "./src/code/components/layoutComponents/Home.tsx"))));
const Clients = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./layoutComponents/Clients */ "./src/code/components/layoutComponents/Clients.tsx"))));
const Sales = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./layoutComponents/Sales */ "./src/code/components/layoutComponents/Sales.tsx"))));
const Blog = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./layoutComponents/Blog */ "./src/code/components/layoutComponents/Blog.tsx"))));
const NonExistent = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./layoutComponents/NonExistent */ "./src/code/components/layoutComponents/NonExistent.tsx"))));
const state_1 = __webpack_require__(/*! ../state */ "./src/code/state.ts");
class BaseLayout extends react_1.Component {
    constructor() {
        super(...arguments);
        this.state = state_1.createState(this);
    }
    componentDidMount() {
        this.state.handleEmbla();
        window.addEventListener('resize', () => { this.state.handleEmbla(); });
        this.state.calcScreenDimensions();
        this.state.handleBaseUrl();
        this.state.handleCarouselDimensions();
        window.addEventListener('resize', () => { this.state.calcScreenDimensions('resized'); });
        let pathName = window.location.pathname;
        pathName = pathName.toString();
        this.state.handleCarouselPosition(pathName);
        if (window.location.pathname === "/") {
            this.state.slideDimensions();
            window.addEventListener('resize', () => { this.state.slideDimensions(); });
        }
        if (window.location.pathname === "/Clients" && window.innerWidth > 600) {
            this.state.handleCarouselDimensions();
        }
        this.state.handleReloadedPage();
        this.setState({ isMounted: true });
        this.setState({ isUnmounted: false });
    }
    componentWillUnmount() {
        this.setState({ isMounted: false });
        this.setState({ isUnmounted: true });
    }
    render() {
        function ErrorFallback({ error, resetErrorBoundary }) {
            return (react_1.default.createElement("div", { role: "alert" },
                react_1.default.createElement("p", null, "Something went wrong:"),
                react_1.default.createElement("pre", null, error.message),
                react_1.default.createElement("button", { onClick: resetErrorBoundary }, "Try again")));
        }
        return (react_1.default.createElement(react_error_boundary_1.ErrorBoundary, { FallbackComponent: ErrorFallback, onReset: () => { location.reload(); } },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(react_location_1.LocationProvider, null,
                    react_1.default.createElement(react_location_1.Match, { path: "/NonExistent" },
                        react_1.default.createElement(NonExistent, { state: this.state })),
                    react_1.default.createElement(react_location_1.Match, { path: "/Blog" },
                        react_1.default.createElement(Blog, { state: this.state })),
                    react_1.default.createElement(react_location_1.Match, { path: "/Sales" },
                        react_1.default.createElement(Sales, { state: this.state })),
                    react_1.default.createElement(react_location_1.Match, { path: "/Clients" },
                        react_1.default.createElement(Clients, { state: this.state })),
                    react_1.default.createElement(react_location_1.Match, { path: "/" },
                        react_1.default.createElement(Home, { state: this.state }))))));
    }
}
exports.default = BaseLayout;


/***/ }),

/***/ "./src/code/components/layoutComponents/Blog.tsx":
/*!*******************************************************!*\
  !*** ./src/code/components/layoutComponents/Blog.tsx ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Header = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Header */ "./src/code/components/layoutComponents/Header.tsx"))));
const Footer = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Footer */ "./src/code/components/layoutComponents/Footer.tsx"))));
const Box = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js"))));
const Blog = ({ state }) => (react_1.default.createElement(Grid, null,
    react_1.default.createElement(Grid, { className: "header" },
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(Header, { state: state }))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 8, lg: 8, xl: 8 },
            react_1.default.createElement("h1", { className: "introHeading blogTitle" }, "Blog"),
            state.blogPosts.map(post => {
                return (react_1.default.createElement(Grid, { container: true, item: true, key: post.title, id: post.title, className: "introStyling", xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                        react_1.default.createElement("h2", { className: "blogTitle" }, post.title)),
                    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                        react_1.default.createElement("h3", { className: "blogDate" }, post.date)),
                    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 4, lg: 4, xl: 4 },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: post.webp, type: "image/webp", width: post.width, height: post.height }),
                            react_1.default.createElement("img", { className: "blogImage", key: post.image, src: post.image, alt: post.alt, width: post.width, height: post.height }))),
                    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 4, lg: 4, xl: 4 },
                        react_1.default.createElement(Box, { p: 2, py: 0 },
                            react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${post.content}` } })))));
            })),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 1, lg: 1, xl: 1 })),
    react_1.default.createElement(Grid, { className: "Footer" },
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(Footer, { state: state })))));
exports.default = Blog;


/***/ }),

/***/ "./src/code/components/layoutComponents/Clients.tsx":
/*!**********************************************************!*\
  !*** ./src/code/components/layoutComponents/Clients.tsx ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Hidden = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Hidden */ "./node_modules/@material-ui/core/esm/Hidden/index.js"))));
const Header = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Header */ "./src/code/components/layoutComponents/Header.tsx"))));
const ClientsCarousel = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./projectsComponents/Carousels/ClientsCarousel */ "./src/code/components/layoutComponents/projectsComponents/Carousels/ClientsCarousel.tsx"))));
const ClientsIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./projectsComponents/Intros/ClientsIntro */ "./src/code/components/layoutComponents/projectsComponents/Intros/ClientsIntro.tsx"))));
const ProfileIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./profileComponents/ProfileIntro */ "./src/code/components/layoutComponents/profileComponents/ProfileIntro.tsx"))));
const Footer = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Footer */ "./src/code/components/layoutComponents/Footer.tsx"))));
const ClientsSlider = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./projectsComponents/Sliders/ClientsSlider */ "./src/code/components/layoutComponents/projectsComponents/Sliders/ClientsSlider.tsx"))));
const Clients = ({ state }) => (react_1.default.createElement(Grid, null,
    react_1.default.createElement(Grid, { className: "header" },
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(Header, { state: state }))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(ClientsIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 5, lg: 5, xl: 5 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(ClientsCarousel, { state: state }))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(ClientsSlider, { state: state })))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(ProfileIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement("picture", null,
                react_1.default.createElement("source", { srcSet: state.profile.webp, type: "image/webp", alt: state.profile.alt, width: state.profile.imageWidth, height: state.profile.imageHeight }),
                react_1.default.createElement("img", { className: "rectangleImage profileImage", key: state.profile.image, src: state.profile.image, alt: state.profile.alt, width: state.profile.imageWidth, height: state.profile.imageHeight })))),
    react_1.default.createElement(Grid, { className: "Footer" },
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(Footer, { state: state })))));
exports.default = Clients;


/***/ }),

/***/ "./src/code/components/layoutComponents/Footer.tsx":
/*!*********************************************************!*\
  !*** ./src/code/components/layoutComponents/Footer.tsx ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const ContactForm = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./footerComponents/ContactForm */ "./src/code/components/layoutComponents/footerComponents/ContactForm.tsx"))));
const FooterBlog = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./footerComponents/FooterBlog */ "./src/code/components/layoutComponents/footerComponents/FooterBlog.tsx"))));
const SocialIcons = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./footerComponents/SocialIcons */ "./src/code/components/layoutComponents/footerComponents/SocialIcons.tsx"))));
const Box = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js"))));
const Footer = ({ state }) => (react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, sm: 2, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 5, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(Grid, { container: true, direction: "column", item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                react_1.default.createElement("h2", null, "Get in touch:"),
                react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                    react_1.default.createElement(SocialIcons, { state: state })),
                react_1.default.createElement("p", null,
                    react_1.default.createElement("a", { href: "mailto:jennifer.fulya.ersoz@gmail.com" }, "jennifer.fulya.ersoz@gmail.com")),
                react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                    react_1.default.createElement(ContactForm, { state: state })))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 3, md: 3, lg: 2, xl: 3 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(FooterBlog, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, sm: 2, md: 2, lg: 2, xl: 2 })),
    react_1.default.createElement(Grid, { container: true, direction: "column", item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, alignItems: "center" },
        react_1.default.createElement(Grid, { container: true, item: true, sm: 2, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, direction: "column", item: true, sm: 8, md: 8, lg: 8, xl: 8, className: "copyright", alignItems: "center" },
            react_1.default.createElement(Box, { p: 2, py: 0 },
                react_1.default.createElement("small", null, "\u00A9 Copyright 2021, Jennifer Ersoz-Jones"))),
        react_1.default.createElement(Grid, { container: true, item: true, sm: 2, md: 2, lg: 2, xl: 2 }))));
exports.default = Footer;


/***/ }),

/***/ "./src/code/components/layoutComponents/Header.tsx":
/*!*********************************************************!*\
  !*** ./src/code/components/layoutComponents/Header.tsx ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Hidden = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Hidden */ "./node_modules/@material-ui/core/esm/Hidden/index.js"))));
const HeaderMenu = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./headerComponents/HeaderMenu */ "./src/code/components/layoutComponents/headerComponents/HeaderMenu.tsx"))));
const MobileMenu = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./headerComponents/MobileMenu */ "./src/code/components/layoutComponents/headerComponents/MobileMenu.tsx"))));
const Header = ({ state }) => (react_1.default.createElement(Grid, { container: true, className: "headerWrapper" },
    react_1.default.createElement(Grid, { container: true, direction: "column", item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, alignItems: "center" },
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(MobileMenu, { state: state }))),
        react_1.default.createElement("picture", null,
            react_1.default.createElement("source", { srcSet: state.logoImages.webp, type: "image/webp", width: state.logoImages.width, height: state.logoImages.height, className: "rectangleImage" }),
            react_1.default.createElement("img", { className: "rectangleImage", alt: "Jennifer Ersoz-Jones", src: state.logoImages.image, width: state.logoImages.width, height: state.logoImages.height })),
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(HeaderMenu, { state: state }))))));
exports.default = Header;


/***/ }),

/***/ "./src/code/components/layoutComponents/Home.tsx":
/*!*******************************************************!*\
  !*** ./src/code/components/layoutComponents/Home.tsx ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Header = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Header */ "./src/code/components/layoutComponents/Header.tsx"))));
const ProjectsSummary = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./ProjectsSummary */ "./src/code/components/layoutComponents/ProjectsSummary.tsx"))));
const Projects = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Projects */ "./src/code/components/layoutComponents/Projects.tsx"))));
const Footer = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Footer */ "./src/code/components/layoutComponents/Footer.tsx"))));
const Home = ({ state }) => (react_1.default.createElement(Grid, null,
    react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
        react_1.default.createElement(Grid, { className: "header" },
            react_1.default.createElement(Header, { state: state })),
        react_1.default.createElement(Grid, { className: "projectsSummary" },
            react_1.default.createElement(ProjectsSummary, { state: state })),
        react_1.default.createElement(Grid, { className: "projects" },
            react_1.default.createElement(Projects, { state: state })),
        react_1.default.createElement(Grid, { className: "Footer" },
            react_1.default.createElement(Footer, { state: state })))));
exports.default = Home;


/***/ }),

/***/ "./src/code/components/layoutComponents/NonExistent.tsx":
/*!**************************************************************!*\
  !*** ./src/code/components/layoutComponents/NonExistent.tsx ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const NonExistent = ({ state }) => (react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, className: 'test' }));
exports.default = NonExistent;


/***/ }),

/***/ "./src/code/components/layoutComponents/Projects.tsx":
/*!***********************************************************!*\
  !*** ./src/code/components/layoutComponents/Projects.tsx ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const ShillingtonProjects = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./projectsComponents/ShillingtonProjects */ "./src/code/components/layoutComponents/projectsComponents/ShillingtonProjects.tsx"))));
const Projects = ({ state }) => (react_1.default.createElement(Grid, { container: true, className: "projectsSummary" },
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 6, lg: 6, xl: 6 },
            react_1.default.createElement("h1", { className: "introHeading", dangerouslySetInnerHTML: { __html: `${state.portfolioTitle}` } }),
            react_1.default.createElement("p", { dangerouslySetInnerHTML: { __html: `${state.portfolioContent}` } })),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(ShillingtonProjects, { state: state })))));
exports.default = Projects;


/***/ }),

/***/ "./src/code/components/layoutComponents/ProjectsSummary.tsx":
/*!******************************************************************!*\
  !*** ./src/code/components/layoutComponents/ProjectsSummary.tsx ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Hidden = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Hidden */ "./node_modules/@material-ui/core/esm/Hidden/index.js"))));
const MobileImage = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./carouselComponents/MobileImage */ "./src/code/components/layoutComponents/carouselComponents/MobileImage.tsx"))));
const ProjectsSummary = ({ state }) => (react_1.default.createElement(Grid, { container: true, className: "projectsSummary" },
    react_1.default.createElement(Grid, { container: true, direction: "column", item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, alignItems: "center" },
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(MobileImage, { state: state }))),
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement("picture", null,
                react_1.default.createElement("source", { srcSet: state.introCarousel[0].webp, type: "image/webp", width: state.introCarousel[0].width, height: state.introCarousel[0].height, className: `${state.introSlider.temporaryImage ? 'show-temp-image' : ''}` }),
                react_1.default.createElement("img", { src: state.introCarousel[0].image, className: `${state.introSlider.temporaryImage ? 'show-temp-image' : ''}`, width: state.introCarousel[0].width, height: state.introCarousel[0].height }))))));
exports.default = ProjectsSummary;


/***/ }),

/***/ "./src/code/components/layoutComponents/Sales.tsx":
/*!********************************************************!*\
  !*** ./src/code/components/layoutComponents/Sales.tsx ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Hidden = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Hidden */ "./node_modules/@material-ui/core/esm/Hidden/index.js"))));
const Header = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Header */ "./src/code/components/layoutComponents/Header.tsx"))));
const Footer = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Footer */ "./src/code/components/layoutComponents/Footer.tsx"))));
const Sales = ({ state }) => (react_1.default.createElement(Grid, null,
    react_1.default.createElement(Grid, { className: "header" },
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(Header, { state: state }))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, className: "salesHeader" },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(Grid, { container: true, item: true, sm: 10, md: 8, lg: 8, xl: 8, alignItems: "flex-end", direction: "column", className: "salesTop" },
                react_1.default.createElement("picture", { className: "salesImage" },
                    react_1.default.createElement("source", { className: "salesImage", srcSet: state.salesImages.webp, type: "image/webp", width: state.salesImages.width, height: state.salesImages.height }),
                    react_1.default.createElement("img", { className: "salesImage", src: state.salesImages.image, width: state.salesImages.width, height: state.salesImages.height })))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(Grid, { container: true, item: true, xs: 11, alignItems: "flex-end", direction: "column", className: "salesTop" },
                react_1.default.createElement("picture", { className: "salesImage" },
                    react_1.default.createElement("source", { className: "salesImage", srcSet: state.salesImages.mobileWebp, type: "image/webp", width: state.salesImages.width, height: state.salesImages.height, alt: state.salesImages.alt }),
                    react_1.default.createElement("img", { className: "salesImage", src: state.salesImages.mobileImage, width: state.salesImages.width, height: state.salesImages.height, alt: state.salesImages.alt })))),
        react_1.default.createElement(Grid, { container: true, item: true, sm: 1, md: 2, lg: 2, xl: 2 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, className: "salesIntro" },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 7, lg: 7, xl: 7 },
            react_1.default.createElement("div", { className: "introStyling" },
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[0].title}` } }),
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[0].content}` } }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 3, lg: 3, xl: 3 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 7, lg: 7, xl: 7 },
            react_1.default.createElement("div", { className: "introStyling" },
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[1].title}` } }),
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[1].content}` } }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 3, lg: 3, xl: 3 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 7, lg: 7, xl: 7 },
            react_1.default.createElement("div", { className: "introStyling" },
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[2].title}` } }),
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[2].content}` } }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 3, lg: 3, xl: 3 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 7, lg: 7, xl: 7 },
            react_1.default.createElement("div", { className: "introStyling" },
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[3].title}` } }),
                react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.sales[3].content}` } }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 3, lg: 3, xl: 3 })),
    react_1.default.createElement(Grid, { className: "Footer" },
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(Footer, { state: state })))));
exports.default = Sales;


/***/ }),

/***/ "./src/code/components/layoutComponents/carouselComponents/MobileImage.tsx":
/*!*********************************************************************************!*\
  !*** ./src/code/components/layoutComponents/carouselComponents/MobileImage.tsx ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const MobileImage = ({ state }) => (react_1.default.createElement(Grid, { className: "mobileProjectSummary" },
    react_1.default.createElement(react_location_1.LocationProvider, null,
        react_1.default.createElement(react_location_1.Link, { to: state.introCarousel[0].link, onClick: state.handleLink },
            react_1.default.createElement("picture", null,
                react_1.default.createElement("source", { srcSet: state.introCarousel[0].mobileWebp, type: "image/webp", width: state.introCarousel[0].width, height: state.introCarousel[0].height }),
                react_1.default.createElement("img", { src: state.introCarousel[0].mobileImage, alt: state.introCarousel[0].alt, width: state.introCarousel[0].width, height: state.introCarousel[0].height }))))));
exports.default = MobileImage;


/***/ }),

/***/ "./src/code/components/layoutComponents/footerComponents/ContactForm.tsx":
/*!*******************************************************************************!*\
  !*** ./src/code/components/layoutComponents/footerComponents/ContactForm.tsx ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_material_ui_form_validator_1 = __webpack_require__(/*! react-material-ui-form-validator */ "./node_modules/react-material-ui-form-validator/lib/index.js");
const Button = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js"))));
const ContactForm = ({ state }) => (react_1.default.createElement(Grid, null,
    react_1.default.createElement(react_material_ui_form_validator_1.ValidatorForm, { onSubmit: state.sendMessage, onError: errors => console.log(errors), className: 'formDisplay' },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
            react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                react_1.default.createElement(Grid, { container: true, item: true, sm: 6, md: 6, lg: 6, xl: 6 },
                    react_1.default.createElement(react_material_ui_form_validator_1.TextValidator, { classes: { root: 'TextValidator' }, id: "filled-basic", fullWidth: true, size: "small", label: "Subject", style: { margin: 8 }, variant: "filled", onChange: state.handleChange, name: "messageSubject", value: state.messageSubject, validators: ['required', 'isString', 'maxStringLength:30'], errorMessages: ['this field is required', 'subject is not valid', 'maximum subject length 30 characters'] })),
                react_1.default.createElement(Grid, { container: true, item: true, sm: 6, md: 6, lg: 6, xl: 6 },
                    react_1.default.createElement(react_material_ui_form_validator_1.TextValidator, { classes: { root: 'TextValidator' }, id: "filled-required", fullWidth: true, size: "small", label: "Email", style: { margin: 8 }, variant: "filled", onChange: state.handleChange, name: "messageFrom", value: state.messageFrom, validators: ['required', 'isEmail'], errorMessages: ['this field is required', 'invalid email address'] }))),
            react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                react_1.default.createElement(react_material_ui_form_validator_1.TextValidator, { classes: { root: 'TextValidator' }, id: "filled-multiline-static", size: "small", fullWidth: true, multiline: true, style: { margin: 8 }, rows: 4, label: "Message", variant: "filled", onChange: state.handleChange, name: "messageBody", value: state.messageBody, validators: ['required', 'isString', 'maxStringLength:400'], errorMessages: ['this field is required', 'message is not valid', 'maximum message length 400 characters'] }),
                react_1.default.createElement(Button, { type: "submit", style: { margin: 8 }, classes: { root: 'SubmitButton' } }, "Submit")))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, className: 'successDisplay' },
        react_1.default.createElement("h3", null, "Thank you. The form has been successfully sent.")),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, className: 'errorDisplay' },
        react_1.default.createElement("h3", null, "Error. Sorry we cannot process the contact form at the moment. Please try again later."))));
exports.default = ContactForm;


/***/ }),

/***/ "./src/code/components/layoutComponents/footerComponents/FooterBlog.tsx":
/*!******************************************************************************!*\
  !*** ./src/code/components/layoutComponents/footerComponents/FooterBlog.tsx ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Box = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js"))));
const FooterBlog = ({ state }) => (react_1.default.createElement(Grid, null,
    react_1.default.createElement(Box, { p: 1, py: 0 },
        react_1.default.createElement(Grid, { container: true, className: "blogSummary", item: true, key: state.blogPosts[0].title, id: state.blogPosts[0].title, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
            react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                react_1.default.createElement("h2", { className: "blogTitle" }, "Blog:")),
            react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
                react_1.default.createElement(Grid, { onClick: () => state.handleMenuLink(3) },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: state.baseUrl + "/Blog" },
                            react_1.default.createElement("h3", { className: "blogTitle" }, state.blogPosts[0].title))),
                    react_1.default.createElement("h4", { className: "blogDate" }, state.blogPosts[0].date))),
            react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12, onClick: () => state.handleMenuLink(3) },
                react_1.default.createElement(react_location_1.LocationProvider, null,
                    react_1.default.createElement(react_location_1.Link, { to: state.baseUrl + "/Blog" },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: state.blogPosts[0].webp, type: "image/webp", width: state.blogPosts[0].width, height: state.blogPosts[0].height }),
                            react_1.default.createElement("img", { className: "blogImage", key: state.blogPosts[0].image, src: state.blogPosts[0].image, alt: state.blogPosts[0].alt, width: state.blogPosts[0].width, height: state.blogPosts[0].height })))))))));
exports.default = FooterBlog;


/***/ }),

/***/ "./src/code/components/layoutComponents/footerComponents/SocialIcons.tsx":
/*!*******************************************************************************!*\
  !*** ./src/code/components/layoutComponents/footerComponents/SocialIcons.tsx ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const SocialIcons = ({ state }) => (react_1.default.createElement(Grid, null,
    react_1.default.createElement("ul", { className: "socialIcons" }, state.socialIcons.map(icon => {
        return (react_1.default.createElement("li", { key: icon.title },
            react_1.default.createElement(react_location_1.LocationProvider, null,
                react_1.default.createElement(react_location_1.Link, { to: icon.link, target: "_blank", rel: "noopener" },
                    react_1.default.createElement("picture", null,
                        react_1.default.createElement("source", { className: "socialImage", srcSet: icon.webp, type: "image/webp", width: icon.width, height: icon.height }),
                        react_1.default.createElement("img", { className: "socialImage", key: icon.image, src: icon.image, alt: icon.alt, width: icon.width, height: icon.height }))))));
    }))));
exports.default = SocialIcons;


/***/ }),

/***/ "./src/code/components/layoutComponents/headerComponents/HeaderMenu.tsx":
/*!******************************************************************************!*\
  !*** ./src/code/components/layoutComponents/headerComponents/HeaderMenu.tsx ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const HeaderMenu = ({ state }) => (react_1.default.createElement("div", { className: "mainMenu" }, state.menu.map((menuItem, index) => {
    return (react_1.default.createElement("div", { className: "menuWrapper", key: menuItem.link, onClick: () => state.handleMenuLink(index) },
        react_1.default.createElement(react_location_1.LocationProvider, null,
            react_1.default.createElement(react_location_1.Link, { to: state.baseUrl + menuItem.link, className: `${menuItem.active ? 'active' : ''}` }, menuItem.title))));
})));
exports.default = HeaderMenu;


/***/ }),

/***/ "./src/code/components/layoutComponents/headerComponents/MenuContents.tsx":
/*!********************************************************************************!*\
  !*** ./src/code/components/layoutComponents/headerComponents/MenuContents.tsx ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Menu = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Menu */ "./node_modules/@material-ui/core/esm/Menu/index.js"))));
const MenuItem = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/MenuItem */ "./node_modules/@material-ui/core/esm/MenuItem/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const MenuContents = ({ state }) => (react_1.default.createElement(Menu, { id: "fade-menu", anchorEl: state.mobileMenu.anchorEl, open: state.mobileMenu.open, onClose: state.handleClose }, state.menu.map((menuItem, index) => {
    return (react_1.default.createElement("div", { className: "menuWrapper", key: menuItem.link, onClick: () => state.handleMenuLink(index) },
        react_1.default.createElement(MenuItem, { className: `MenuItem ${menuItem.showLink ? 'show-link' : 'hide-link'}`, onClick: state.handleClose },
            react_1.default.createElement(react_location_1.LocationProvider, null,
                react_1.default.createElement(react_location_1.Link, { to: state.baseUrl + menuItem.link, className: `${menuItem.active ? 'active' : ''}` }, menuItem.title)))));
})));
exports.default = MenuContents;


/***/ }),

/***/ "./src/code/components/layoutComponents/headerComponents/MobileMenu.tsx":
/*!******************************************************************************!*\
  !*** ./src/code/components/layoutComponents/headerComponents/MobileMenu.tsx ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Button = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Button */ "./node_modules/@material-ui/core/esm/Button/index.js"))));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Box = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Box */ "./node_modules/@material-ui/core/esm/Box/index.js"))));
const MenuContents = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./MenuContents */ "./src/code/components/layoutComponents/headerComponents/MenuContents.tsx"))));
const MobileMenu = ({ state }) => (react_1.default.createElement(Grid, { container: true, item: true, className: "menuSection", direction: "column", xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
    react_1.default.createElement(Box, { p: 1 },
        react_1.default.createElement(Button, { className: "header-menu", "aria-owns": state.mobileMenu.open ? 'fade-menu' : undefined, "aria-haspopup": "true", onClick: state.handleClick },
            react_1.default.createElement("img", { className: "rectangleImage", alt: state.hamburgerImage.alt, src: state.hamburgerImage.src, width: state.hamburgerImage.width, height: state.hamburgerImage.height })),
        react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
            react_1.default.createElement(MenuContents, { state: state })))));
exports.default = MobileMenu;


/***/ }),

/***/ "./src/code/components/layoutComponents/profileComponents/ProfileIntro.tsx":
/*!*********************************************************************************!*\
  !*** ./src/code/components/layoutComponents/profileComponents/ProfileIntro.tsx ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const ProfileIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h1", { className: "introHeading", dangerouslySetInnerHTML: { __html: `${state.profile.heading}` } }),
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.profile.title}` } }),
    react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.profile.content}` } })));
exports.default = ProfileIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Carousels/BertrandCarousel.tsx":
/*!************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Carousels/BertrandCarousel.tsx ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const BertrandCarousel = ({ state }) => (react_1.default.createElement(Grid, { id: "content-carousel" },
    react_1.default.createElement(Grid, { id: "carousel" },
        react_1.default.createElement(Grid, { id: "carousel-mask" },
            react_1.default.createElement("ul", { id: "Bertrand-Carousel", className: "Bertrand-Carousel" }, state.projectBertrand.map((projectItem, index) => {
                return (react_1.default.createElement("li", { id: projectItem.title, key: projectItem.key, className: `${projectItem.active ? 'projectBertrand-active' : 'bertrand-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}` },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: window.location.pathname, hash: "/#", onClick: (e) => state.handleCarouselLink(e, index, projectItem.projectId), className: `${projectItem.active ? 'projectBertrand-active' : 'bertrand-inactive'}` },
                            react_1.default.createElement("picture", null,
                                react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height }),
                                react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height }))))));
            }))))));
exports.default = BertrandCarousel;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Carousels/CityIdentityCarousel.tsx":
/*!****************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Carousels/CityIdentityCarousel.tsx ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const CityIdentityCarousel = ({ state }) => (react_1.default.createElement(Grid, { id: "content-carousel" },
    react_1.default.createElement(Grid, { id: "carousel" },
        react_1.default.createElement(Grid, { id: "carousel-mask" },
            react_1.default.createElement("ul", { id: "CityIdentity-Carousel", className: "CityIdentity-Carousel" }, state.projectCityIdentity.map((projectItem, index) => {
                return (react_1.default.createElement("li", { id: projectItem.title, key: projectItem.key, className: `${projectItem.active ? 'projectCityIdentity-active' : 'cityIdentity-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}` },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: window.location.pathname, hash: "/#", onClick: (e) => state.handleCarouselLink(e, index, projectItem.projectId), className: `${projectItem.active ? 'projectCityIdentity-active' : 'cityIdentity-inactive'}` },
                            react_1.default.createElement("picture", null,
                                react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height }),
                                react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height }))))));
            }))))));
exports.default = CityIdentityCarousel;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Carousels/ClientsCarousel.tsx":
/*!***********************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Carousels/ClientsCarousel.tsx ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const ClientsCarousel = ({ state }) => (react_1.default.createElement(Grid, { id: "content-carousel" },
    react_1.default.createElement(Grid, { id: "carousel" },
        react_1.default.createElement(Grid, { id: "carousel-mask" },
            react_1.default.createElement("ul", { id: "Clients-Carousel", className: "Clients-Carousel" }, state.previousClientsSites.map((projectItem, index) => {
                return (react_1.default.createElement("li", { id: projectItem.title, key: projectItem.key, className: `${projectItem.active ? 'previousClientsSites-active' : 'clients-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}` },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: window.location.pathname, hash: "/#", onClick: (e) => state.handleCarouselLink(e, index, projectItem.projectId), className: `${projectItem.active ? 'previousClientsSites-active' : 'clients-inactive'}` },
                            react_1.default.createElement("picture", null,
                                react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height }),
                                react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height })))),
                    react_1.default.createElement("span", null,
                        projectItem.hasLink && projectItem.linkActive ? react_1.default.createElement("p", null,
                            "Active URL: ",
                            react_1.default.createElement("a", { className: "clientsLink", rel: "noreferrer", href: `${projectItem.link.toString()}`, target: "_blank" }, projectItem.link)) : '',
                        projectItem.hasLink && !projectItem.linkActive ? react_1.default.createElement("p", null,
                            "Expired URL: ",
                            projectItem.link) : '',
                        projectItem.hasWayBackLink ? react_1.default.createElement("p", null,
                            "WayBackMachine link: ",
                            react_1.default.createElement("a", { className: "clientsLink", rel: "noreferrer", href: `${projectItem.wayBackLink.toString()}`, target: "_blank" }, projectItem.wayBackLink)) : '')));
            }))))));
exports.default = ClientsCarousel;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Carousels/CorporateWebsiteCarousel.tsx":
/*!********************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Carousels/CorporateWebsiteCarousel.tsx ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const CorporateWebsiteCarousel = ({ state }) => (react_1.default.createElement(Grid, { id: "content-carousel" },
    react_1.default.createElement(Grid, { id: "carousel" },
        react_1.default.createElement(Grid, { id: "carousel-mask" },
            react_1.default.createElement("ul", { id: "CorporateWebsite-Carousel", className: "CorporateWebsite-Carousel" }, state.projectCorporateWebsite.map((projectItem, index) => {
                return (react_1.default.createElement("li", { id: projectItem.title, key: projectItem.key, className: `${projectItem.active ? 'projectCorporateWebsite-active' : 'corporateWebsite-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}` },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: window.location.pathname, hash: "/#", onClick: (e) => state.handleCarouselLink(e, index, projectItem.projectId), className: `${projectItem.active ? 'projectCorporateWebsite-active' : 'corporateWebsite-inactive'}` },
                            react_1.default.createElement("picture", null,
                                react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height }),
                                react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height }))))));
            }))))));
exports.default = CorporateWebsiteCarousel;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Carousels/EPubCarousel.tsx":
/*!********************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Carousels/EPubCarousel.tsx ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const EPubCarousel = ({ state }) => (react_1.default.createElement(Grid, { id: "content-carousel" },
    react_1.default.createElement(Grid, { id: "carousel" },
        react_1.default.createElement(Grid, { id: "carousel-mask" },
            react_1.default.createElement("ul", { id: "EPub-Carousel", className: "EPub-Carousel" }, state.projectEPub.map((projectItem, index) => {
                return (react_1.default.createElement("li", { id: projectItem.title, key: projectItem.key, className: `${projectItem.active ? 'projectEPub-active' : 'ePub-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}` },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: window.location.pathname, hash: "/#", onClick: (e) => state.handleCarouselLink(e, index, projectItem.projectId), className: `${projectItem.active ? 'projectEPub-active' : 'ePub-inactive'}` },
                            react_1.default.createElement("picture", null,
                                react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height }),
                                react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height }))))));
            }))))));
exports.default = EPubCarousel;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Carousels/HandmadeCarousel.tsx":
/*!************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Carousels/HandmadeCarousel.tsx ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const HandmadeCarousel = ({ state }) => (react_1.default.createElement(Grid, { id: "content-carousel" },
    react_1.default.createElement(Grid, { id: "carousel" },
        react_1.default.createElement(Grid, { id: "carousel-mask" },
            react_1.default.createElement("ul", { id: "Handmade-Carousel", className: "Handmade-Carousel" }, state.projectHandmade.map((projectItem, index) => {
                return (react_1.default.createElement("li", { id: projectItem.title, key: projectItem.key, className: `${projectItem.active ? 'projectHandmade-active' : 'handmade-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}` },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: window.location.pathname, hash: "/#", onClick: (e) => state.handleCarouselLink(e, index, projectItem.projectId), className: `${projectItem.active ? 'projectHandmade-active' : 'handmade-inactive'}` },
                            react_1.default.createElement("picture", null,
                                react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height }),
                                react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height }))))));
            }))))));
exports.default = HandmadeCarousel;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Carousels/SmallBusinessLogoCarousel.tsx":
/*!*********************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Carousels/SmallBusinessLogoCarousel.tsx ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const react_location_1 = __webpack_require__(/*! react-location */ "./node_modules/react-location/dist/index.js");
const SmallBusinessLogoCarousel = ({ state }) => (react_1.default.createElement(Grid, { id: "content-carousel" },
    react_1.default.createElement(Grid, { id: "carousel" },
        react_1.default.createElement(Grid, { id: "carousel-mask" },
            react_1.default.createElement("ul", { id: "SmallBusinessLogo-Carousel", className: "SmallBusinessLogo-Carousel" }, state.projectSmallBusinessLogo.map((projectItem, index) => {
                return (react_1.default.createElement("li", { id: projectItem.title, key: projectItem.key, className: `${projectItem.active ? 'projectSmallBusinessLogo-active' : 'smallBusinessLogo-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}` },
                    react_1.default.createElement(react_location_1.LocationProvider, null,
                        react_1.default.createElement(react_location_1.Link, { to: window.location.pathname, hash: "/#", onClick: (e) => state.handleCarouselLink(e, index, projectItem.projectId), className: `${projectItem.active ? 'projectSmallBusinessLogo-active' : 'smallBusinessLogo-inactive'}` },
                            react_1.default.createElement("picture", null,
                                react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height }),
                                react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height }))))));
            }))))));
exports.default = SmallBusinessLogoCarousel;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Intros/BertrandIntro.tsx":
/*!******************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Intros/BertrandIntro.tsx ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const BertrandIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[1].title}` } }),
    react_1.default.createElement("p", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[1].content}` } })));
exports.default = BertrandIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Intros/CityIdentityIntro.tsx":
/*!**********************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Intros/CityIdentityIntro.tsx ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const CityIdentityIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[2].title}` } }),
    react_1.default.createElement("p", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[2].content}` } })));
exports.default = CityIdentityIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Intros/ClientsIntro.tsx":
/*!*****************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Intros/ClientsIntro.tsx ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const ClientsIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h1", { className: "introHeading", dangerouslySetInnerHTML: { __html: `${state.previousClients.heading}` } }),
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.previousClients.title}` } }),
    react_1.default.createElement("span", { dangerouslySetInnerHTML: { __html: `${state.previousClients.content}` } })));
exports.default = ClientsIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Intros/CorporateWebsiteIntro.tsx":
/*!**************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Intros/CorporateWebsiteIntro.tsx ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const CorporateWebsiteIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[4].title}` } }),
    react_1.default.createElement("p", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[4].content}` } })));
exports.default = CorporateWebsiteIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Intros/EPubIntro.tsx":
/*!**************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Intros/EPubIntro.tsx ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const EPubIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[3].title}` } }),
    react_1.default.createElement("p", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[3].content}` } })));
exports.default = EPubIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Intros/HandmadeIntro.tsx":
/*!******************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Intros/HandmadeIntro.tsx ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const HandmadeIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[0].title}` } }),
    react_1.default.createElement("p", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[0].content}` } })));
exports.default = HandmadeIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Intros/SmallBusinessLogoIntro.tsx":
/*!***************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Intros/SmallBusinessLogoIntro.tsx ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const SmallBusinessLogoIntro = ({ state }) => (react_1.default.createElement("div", { className: "introStyling" },
    react_1.default.createElement("h2", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[5].title}` } }),
    react_1.default.createElement("p", { dangerouslySetInnerHTML: { __html: `${state.projectsIntro[5].content}` } })));
exports.default = SmallBusinessLogoIntro;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/ShillingtonProjects.tsx":
/*!*****************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/ShillingtonProjects.tsx ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const Grid = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Grid */ "./node_modules/@material-ui/core/esm/Grid/index.js"))));
const Hidden = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! @material-ui/core/Hidden */ "./node_modules/@material-ui/core/esm/Hidden/index.js"))));
const HandmadeIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Intros/HandmadeIntro */ "./src/code/components/layoutComponents/projectsComponents/Intros/HandmadeIntro.tsx"))));
const BertrandIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Intros/BertrandIntro */ "./src/code/components/layoutComponents/projectsComponents/Intros/BertrandIntro.tsx"))));
const CityIdentityIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Intros/CityIdentityIntro */ "./src/code/components/layoutComponents/projectsComponents/Intros/CityIdentityIntro.tsx"))));
const EPubIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Intros/EPubIntro */ "./src/code/components/layoutComponents/projectsComponents/Intros/EPubIntro.tsx"))));
const CorporateWebsiteIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Intros/CorporateWebsiteIntro */ "./src/code/components/layoutComponents/projectsComponents/Intros/CorporateWebsiteIntro.tsx"))));
const SmallBusinessLogoIntro = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Intros/SmallBusinessLogoIntro */ "./src/code/components/layoutComponents/projectsComponents/Intros/SmallBusinessLogoIntro.tsx"))));
const HandmadeCarousel = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Carousels/HandmadeCarousel */ "./src/code/components/layoutComponents/projectsComponents/Carousels/HandmadeCarousel.tsx"))));
const BertrandCarousel = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Carousels/BertrandCarousel */ "./src/code/components/layoutComponents/projectsComponents/Carousels/BertrandCarousel.tsx"))));
const CityIdentityCarousel = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Carousels/CityIdentityCarousel */ "./src/code/components/layoutComponents/projectsComponents/Carousels/CityIdentityCarousel.tsx"))));
const EPubCarousel = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Carousels/EPubCarousel */ "./src/code/components/layoutComponents/projectsComponents/Carousels/EPubCarousel.tsx"))));
const CorporateWebsiteCarousel = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Carousels/CorporateWebsiteCarousel */ "./src/code/components/layoutComponents/projectsComponents/Carousels/CorporateWebsiteCarousel.tsx"))));
const SmallBusinessLogoCarousel = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Carousels/SmallBusinessLogoCarousel */ "./src/code/components/layoutComponents/projectsComponents/Carousels/SmallBusinessLogoCarousel.tsx"))));
const HandmadeSlider = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Sliders/HandmadeSlider */ "./src/code/components/layoutComponents/projectsComponents/Sliders/HandmadeSlider.tsx"))));
const BertrandSlider = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Sliders/BertrandSlider */ "./src/code/components/layoutComponents/projectsComponents/Sliders/BertrandSlider.tsx"))));
const CityIdentitySlider = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Sliders/CityIdentitySlider */ "./src/code/components/layoutComponents/projectsComponents/Sliders/CityIdentitySlider.tsx"))));
const EPubSlider = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Sliders/EPubSlider */ "./src/code/components/layoutComponents/projectsComponents/Sliders/EPubSlider.tsx"))));
const CorporateWebsiteSlider = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Sliders/CorporateWebsiteSlider */ "./src/code/components/layoutComponents/projectsComponents/Sliders/CorporateWebsiteSlider.tsx"))));
const SmallBusinessLogoSlider = react_1.default.lazy(() => Promise.resolve().then(() => __importStar(__webpack_require__(/*! ./Sliders/SmallBusinessLogoSlider */ "./src/code/components/layoutComponents/projectsComponents/Sliders/SmallBusinessLogoSlider.tsx"))));
const ShillingtonProjects = ({ state }) => (react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(HandmadeIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 5, lg: 5, xl: 5 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(HandmadeCarousel, { state: state }))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(HandmadeSlider, { state: state })))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(BertrandIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 5, lg: 5, xl: 5 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(BertrandCarousel, { state: state }))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(BertrandSlider, { state: state })))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(CityIdentityIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 5, lg: 5, xl: 5 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(CityIdentityCarousel, { state: state }))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(CityIdentitySlider, { state: state })))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(EPubIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 5, lg: 5, xl: 5 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(EPubCarousel, { state: state }))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(EPubSlider, { state: state })))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(CorporateWebsiteIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 5, lg: 5, xl: 5 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(CorporateWebsiteCarousel, { state: state }))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(CorporateWebsiteSlider, { state: state })))),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 2, lg: 2, xl: 2 }),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 10, sm: 10, md: 5, lg: 5, xl: 5 },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(SmallBusinessLogoIntro, { state: state }))),
        react_1.default.createElement(Grid, { container: true, item: true, xs: 1, sm: 1, md: 5, lg: 5, xl: 5 })),
    react_1.default.createElement(Grid, { container: true, item: true, xs: 12, sm: 12, md: 12, lg: 12, xl: 12 },
        react_1.default.createElement(Hidden, { only: "xs" },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(SmallBusinessLogoCarousel, { state: state }))),
        react_1.default.createElement(Hidden, { only: ['sm', 'md', 'lg', 'xl'] },
            react_1.default.createElement(react_1.Suspense, { fallback: react_1.default.createElement("div", null, "Loading...") },
                react_1.default.createElement(SmallBusinessLogoSlider, { state: state }))))));
exports.default = ShillingtonProjects;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Sliders/BertrandSlider.tsx":
/*!********************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Sliders/BertrandSlider.tsx ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_2 = __webpack_require__(/*! embla-carousel/react */ "./node_modules/embla-carousel/react.js");
const BertrandSlider = ({ state }) => {
    const [selectedIndex, setSelectedIndex] = react_1.useState(0);
    const [mainViewportRef, embla] = react_2.useEmblaCarousel({ loop: true });
    const [thumbViewportRef, emblaThumbs] = react_2.useEmblaCarousel({
        containScroll: "keepSnaps",
        selectedClass: ""
    });
    const onThumbClick = react_1.useCallback((index) => {
        if (!embla || !emblaThumbs)
            return;
        if (emblaThumbs.clickAllowed())
            embla.scrollTo(index);
    }, [embla, emblaThumbs]);
    const onSelect = react_1.useCallback(() => {
        if (!embla || !emblaThumbs)
            return;
        setSelectedIndex(embla.selectedScrollSnap());
        emblaThumbs.scrollTo(embla.selectedScrollSnap());
    }, [embla, emblaThumbs, setSelectedIndex]);
    react_1.useEffect(() => {
        if (!embla)
            return;
        onSelect();
        embla.on("select", onSelect);
    }, [embla, onSelect]);
    return (react_1.default.createElement("div", { className: "embla" },
        react_1.default.createElement("div", { className: "embla__viewport", ref: mainViewportRef },
            react_1.default.createElement("div", { className: "embla__container" }, state.projectBertrand.map((projectItem, index) => {
                return (react_1.default.createElement("div", { className: "embla__slide", key: `slide-${index}` },
                    react_1.default.createElement("div", { className: "embla__slide__inner", style: { height: projectItem.height } },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height, className: "embla__slide__img" }),
                            react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height, className: "embla__slide__img" })))));
            }))),
        react_1.default.createElement("div", { className: "embla embla--thumb" },
            react_1.default.createElement("div", { className: "embla__viewport", ref: thumbViewportRef },
                react_1.default.createElement("div", { className: "embla__container embla__container--thumb" }, state.projectBertrand.map((projectItem, index) => {
                    return (react_1.default.createElement("div", { className: `embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`, key: `${index}` },
                        react_1.default.createElement("button", { className: `embla__dot ${index === selectedIndex ? "is-selected" : ""}`, type: "button", onClick: () => onThumbClick(index) })));
                }))))));
};
exports.default = BertrandSlider;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Sliders/CityIdentitySlider.tsx":
/*!************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Sliders/CityIdentitySlider.tsx ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_2 = __webpack_require__(/*! embla-carousel/react */ "./node_modules/embla-carousel/react.js");
const CityIdentitySlider = ({ state }) => {
    const [selectedIndex, setSelectedIndex] = react_1.useState(0);
    const [mainViewportRef, embla] = react_2.useEmblaCarousel({ loop: true });
    const [thumbViewportRef, emblaThumbs] = react_2.useEmblaCarousel({
        containScroll: "keepSnaps",
        selectedClass: ""
    });
    const onThumbClick = react_1.useCallback((index) => {
        if (!embla || !emblaThumbs)
            return;
        if (emblaThumbs.clickAllowed())
            embla.scrollTo(index);
    }, [embla, emblaThumbs]);
    const onSelect = react_1.useCallback(() => {
        if (!embla || !emblaThumbs)
            return;
        setSelectedIndex(embla.selectedScrollSnap());
        emblaThumbs.scrollTo(embla.selectedScrollSnap());
    }, [embla, emblaThumbs, setSelectedIndex]);
    react_1.useEffect(() => {
        if (!embla)
            return;
        onSelect();
        embla.on("select", onSelect);
    }, [embla, onSelect]);
    return (react_1.default.createElement("div", { className: "embla" },
        react_1.default.createElement("div", { className: "embla__viewport", ref: mainViewportRef },
            react_1.default.createElement("div", { className: "embla__container" }, state.projectCityIdentity.slice(0, 4).map((projectItem, index) => {
                return (react_1.default.createElement("div", { className: "embla__slide", key: `slide-${index}` },
                    react_1.default.createElement("div", { className: "embla__slide__inner", style: { height: projectItem.height } },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height, className: "embla__slide__img" }),
                            react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height, className: "embla__slide__img" })))));
            }))),
        react_1.default.createElement("div", { className: "embla embla--thumb" },
            react_1.default.createElement("div", { className: "embla__viewport", ref: thumbViewportRef },
                react_1.default.createElement("div", { className: "embla__container embla__container--thumb" }, state.projectCityIdentity.slice(0, 4).map((projectItem, index) => {
                    return (react_1.default.createElement("div", { className: `embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`, key: `${index}` },
                        react_1.default.createElement("button", { className: `embla__dot ${index === selectedIndex ? "is-selected" : ""}`, type: "button", onClick: () => onThumbClick(index) })));
                }))))));
};
exports.default = CityIdentitySlider;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Sliders/ClientsSlider.tsx":
/*!*******************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Sliders/ClientsSlider.tsx ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_2 = __webpack_require__(/*! embla-carousel/react */ "./node_modules/embla-carousel/react.js");
const ClientsSlider = ({ state }) => {
    const [selectedIndex, setSelectedIndex] = react_1.useState(0);
    const [mainViewportRef, embla] = react_2.useEmblaCarousel({ loop: true });
    const [thumbViewportRef, emblaThumbs] = react_2.useEmblaCarousel({
        containScroll: "keepSnaps",
        selectedClass: ""
    });
    const onThumbClick = react_1.useCallback((index) => {
        if (!embla || !emblaThumbs)
            return;
        if (emblaThumbs.clickAllowed())
            embla.scrollTo(index);
    }, [embla, emblaThumbs]);
    const onSelect = react_1.useCallback(() => {
        if (!embla || !emblaThumbs)
            return;
        setSelectedIndex(embla.selectedScrollSnap());
        emblaThumbs.scrollTo(embla.selectedScrollSnap());
    }, [embla, emblaThumbs, setSelectedIndex]);
    react_1.useEffect(() => {
        if (!embla)
            return;
        onSelect();
        embla.on("select", onSelect);
    }, [embla, onSelect]);
    return (react_1.default.createElement("div", { className: "embla" },
        react_1.default.createElement("div", { className: "embla__viewport", ref: mainViewportRef },
            react_1.default.createElement("div", { className: "embla__container" }, state.previousClientsSites.map((projectItem, index) => {
                return (react_1.default.createElement("div", { className: "embla__slide", key: `slide-${index}` },
                    react_1.default.createElement("div", { className: "embla__slide__inner", style: { height: projectItem.imageHeight } },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height, className: "embla__slide__img" }),
                            react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height, className: "embla__slide__img" })))));
            }))),
        react_1.default.createElement("div", { className: "embla embla--thumb" },
            react_1.default.createElement("div", { className: "embla__viewport", ref: thumbViewportRef },
                react_1.default.createElement("div", { className: "embla__container embla__container--thumb" }, state.previousClientsSites.map((projectItem, index) => {
                    return (react_1.default.createElement("div", { className: `embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`, key: `${index}` },
                        react_1.default.createElement("button", { className: `embla__dot ${index === selectedIndex ? "is-selected" : ""}`, type: "button", onClick: () => onThumbClick(index) })));
                }))))));
};
exports.default = ClientsSlider;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Sliders/CorporateWebsiteSlider.tsx":
/*!****************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Sliders/CorporateWebsiteSlider.tsx ***!
  \****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_2 = __webpack_require__(/*! embla-carousel/react */ "./node_modules/embla-carousel/react.js");
const CorporateWebsiteSlider = ({ state }) => {
    const [selectedIndex, setSelectedIndex] = react_1.useState(0);
    const [mainViewportRef, embla] = react_2.useEmblaCarousel({ loop: true });
    const [thumbViewportRef, emblaThumbs] = react_2.useEmblaCarousel({
        containScroll: "keepSnaps",
        selectedClass: ""
    });
    const onThumbClick = react_1.useCallback((index) => {
        if (!embla || !emblaThumbs)
            return;
        if (emblaThumbs.clickAllowed())
            embla.scrollTo(index);
    }, [embla, emblaThumbs]);
    const onSelect = react_1.useCallback(() => {
        if (!embla || !emblaThumbs)
            return;
        setSelectedIndex(embla.selectedScrollSnap());
        emblaThumbs.scrollTo(embla.selectedScrollSnap());
    }, [embla, emblaThumbs, setSelectedIndex]);
    react_1.useEffect(() => {
        if (!embla)
            return;
        onSelect();
        embla.on("select", onSelect);
    }, [embla, onSelect]);
    return (react_1.default.createElement("div", { className: "embla" },
        react_1.default.createElement("div", { className: "embla__viewport", ref: mainViewportRef },
            react_1.default.createElement("div", { className: "embla__container" }, state.projectCorporateWebsite.slice(0, 4).map((projectItem, index) => {
                return (react_1.default.createElement("div", { className: "embla__slide", key: `slide-${index}` },
                    react_1.default.createElement("div", { className: "embla__slide__inner", style: { height: projectItem.height } },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height, className: "embla__slide__img" }),
                            react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height, className: "embla__slide__img" })))));
            }))),
        react_1.default.createElement("div", { className: "embla embla--thumb" },
            react_1.default.createElement("div", { className: "embla__viewport", ref: thumbViewportRef },
                react_1.default.createElement("div", { className: "embla__container embla__container--thumb" }, state.projectCorporateWebsite.slice(0, 4).map((projectItem, index) => {
                    return (react_1.default.createElement("div", { className: `embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`, key: `${index}` },
                        react_1.default.createElement("button", { className: `embla__dot ${index === selectedIndex ? "is-selected" : ""}`, type: "button", onClick: () => onThumbClick(index) })));
                }))))));
};
exports.default = CorporateWebsiteSlider;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Sliders/EPubSlider.tsx":
/*!****************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Sliders/EPubSlider.tsx ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_2 = __webpack_require__(/*! embla-carousel/react */ "./node_modules/embla-carousel/react.js");
const EPubSlider = ({ state }) => {
    const [selectedIndex, setSelectedIndex] = react_1.useState(0);
    const [mainViewportRef, embla] = react_2.useEmblaCarousel({ loop: true });
    const [thumbViewportRef, emblaThumbs] = react_2.useEmblaCarousel({
        containScroll: "keepSnaps",
        selectedClass: ""
    });
    const onThumbClick = react_1.useCallback((index) => {
        if (!embla || !emblaThumbs)
            return;
        if (emblaThumbs.clickAllowed())
            embla.scrollTo(index);
    }, [embla, emblaThumbs]);
    const onSelect = react_1.useCallback(() => {
        if (!embla || !emblaThumbs)
            return;
        setSelectedIndex(embla.selectedScrollSnap());
        emblaThumbs.scrollTo(embla.selectedScrollSnap());
    }, [embla, emblaThumbs, setSelectedIndex]);
    react_1.useEffect(() => {
        if (!embla)
            return;
        onSelect();
        embla.on("select", onSelect);
    }, [embla, onSelect]);
    return (react_1.default.createElement("div", { className: "embla" },
        react_1.default.createElement("div", { className: "embla__viewport", ref: mainViewportRef },
            react_1.default.createElement("div", { className: "embla__container" }, state.projectEPub.slice(0, 3).map((projectItem, index) => {
                return (react_1.default.createElement("div", { className: "embla__slide", key: `slide-${index}` },
                    react_1.default.createElement("div", { className: "embla__slide__inner", style: { height: projectItem.height } },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height, className: "embla__slide__img" }),
                            react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height, className: "embla__slide__img" })))));
            }))),
        react_1.default.createElement("div", { className: "embla embla--thumb" },
            react_1.default.createElement("div", { className: "embla__viewport", ref: thumbViewportRef },
                react_1.default.createElement("div", { className: "embla__container embla__container--thumb" }, state.projectEPub.slice(0, 3).map((projectItem, index) => {
                    return (react_1.default.createElement("div", { className: `embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`, key: `${index}` },
                        react_1.default.createElement("button", { className: `embla__dot ${index === selectedIndex ? "is-selected" : ""}`, type: "button", onClick: () => onThumbClick(index) })));
                }))))));
};
exports.default = EPubSlider;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Sliders/HandmadeSlider.tsx":
/*!********************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Sliders/HandmadeSlider.tsx ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_2 = __webpack_require__(/*! embla-carousel/react */ "./node_modules/embla-carousel/react.js");
const HandmadeSlider = ({ state }) => {
    const [selectedIndex, setSelectedIndex] = react_1.useState(0);
    const [mainViewportRef, embla] = react_2.useEmblaCarousel({ loop: true });
    const [thumbViewportRef, emblaThumbs] = react_2.useEmblaCarousel({
        containScroll: "keepSnaps",
        selectedClass: ""
    });
    const onThumbClick = react_1.useCallback((index) => {
        if (!embla || !emblaThumbs)
            return;
        if (emblaThumbs.clickAllowed())
            embla.scrollTo(index);
    }, [embla, emblaThumbs]);
    const onSelect = react_1.useCallback(() => {
        if (!embla || !emblaThumbs)
            return;
        setSelectedIndex(embla.selectedScrollSnap());
        emblaThumbs.scrollTo(embla.selectedScrollSnap());
    }, [embla, emblaThumbs, setSelectedIndex]);
    react_1.useEffect(() => {
        if (!embla)
            return;
        onSelect();
        embla.on("select", onSelect);
    }, [embla, onSelect]);
    return (react_1.default.createElement("div", { className: "embla" },
        react_1.default.createElement("div", { className: "embla__viewport", ref: mainViewportRef },
            react_1.default.createElement("div", { className: "embla__container" }, state.projectHandmade.slice(0, 3).map((projectItem, index) => {
                return (react_1.default.createElement("div", { className: "embla__slide", key: `slide-${index}` },
                    react_1.default.createElement("div", { className: "embla__slide__inner", style: { height: projectItem.height } },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height, className: "embla__slide__img" }),
                            react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height, className: "embla__slide__img" })))));
            }))),
        react_1.default.createElement("div", { className: "embla embla--thumb" },
            react_1.default.createElement("div", { className: "embla__viewport", ref: thumbViewportRef },
                react_1.default.createElement("div", { className: "embla__container embla__container--thumb" }, state.projectHandmade.slice(0, 3).map((projectItem, index) => {
                    return (react_1.default.createElement("div", { className: `embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`, key: `${index}` },
                        react_1.default.createElement("button", { className: `embla__dot ${index === selectedIndex ? "is-selected" : ""}`, type: "button", onClick: () => onThumbClick(index) })));
                }))))));
};
exports.default = HandmadeSlider;


/***/ }),

/***/ "./src/code/components/layoutComponents/projectsComponents/Sliders/SmallBusinessLogoSlider.tsx":
/*!*****************************************************************************************************!*\
  !*** ./src/code/components/layoutComponents/projectsComponents/Sliders/SmallBusinessLogoSlider.tsx ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const react_2 = __webpack_require__(/*! embla-carousel/react */ "./node_modules/embla-carousel/react.js");
const SmallBusinessLogoSlider = ({ state }) => {
    const [selectedIndex, setSelectedIndex] = react_1.useState(0);
    const [mainViewportRef, embla] = react_2.useEmblaCarousel({ loop: true });
    const [thumbViewportRef, emblaThumbs] = react_2.useEmblaCarousel({
        containScroll: "keepSnaps",
        selectedClass: ""
    });
    const onThumbClick = react_1.useCallback((index) => {
        if (!embla || !emblaThumbs)
            return;
        if (emblaThumbs.clickAllowed())
            embla.scrollTo(index);
    }, [embla, emblaThumbs]);
    const onSelect = react_1.useCallback(() => {
        if (!embla || !emblaThumbs)
            return;
        setSelectedIndex(embla.selectedScrollSnap());
        emblaThumbs.scrollTo(embla.selectedScrollSnap());
    }, [embla, emblaThumbs, setSelectedIndex]);
    react_1.useEffect(() => {
        if (!embla)
            return;
        onSelect();
        embla.on("select", onSelect);
    }, [embla, onSelect]);
    return (react_1.default.createElement("div", { className: "embla" },
        react_1.default.createElement("div", { className: "embla__viewport", ref: mainViewportRef },
            react_1.default.createElement("div", { className: "embla__container" }, state.projectSmallBusinessLogo.slice(0, 4).map((projectItem, index) => {
                return (react_1.default.createElement("div", { className: "embla__slide", key: `slide-${index}` },
                    react_1.default.createElement("div", { className: "embla__slide__inner", style: { height: projectItem.height } },
                        react_1.default.createElement("picture", null,
                            react_1.default.createElement("source", { srcSet: projectItem.webp, type: "image/webp", width: projectItem.width, height: projectItem.height, className: "embla__slide__img" }),
                            react_1.default.createElement("img", { src: projectItem.image, alt: projectItem.alt, width: projectItem.width, height: projectItem.height, className: "embla__slide__img" })))));
            }))),
        react_1.default.createElement("div", { className: "embla embla--thumb" },
            react_1.default.createElement("div", { className: "embla__viewport", ref: thumbViewportRef },
                react_1.default.createElement("div", { className: "embla__container embla__container--thumb" }, state.projectSmallBusinessLogo.slice(0, 4).map((projectItem, index) => {
                    return (react_1.default.createElement("div", { className: `embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`, key: `${index}` },
                        react_1.default.createElement("button", { className: `embla__dot ${index === selectedIndex ? "is-selected" : ""}`, type: "button", onClick: () => onThumbClick(index) })));
                }))))));
};
exports.default = SmallBusinessLogoSlider;


/***/ }),

/***/ "./src/code/config.ts":
/*!****************************!*\
  !*** ./src/code/config.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.config = void 0;
exports.config = {
    serverAddress: "https://jenniferersoz.com:8050",
    userEmail: "jennifer.ersoz@gmail.com"
};


/***/ }),

/***/ "./src/code/main.tsx":
/*!***************************!*\
  !*** ./src/code/main.tsx ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
__webpack_require__(/*! ../scss/appStyles.scss */ "./src/scss/appStyles.scss");
__webpack_require__(/*! ../fonts/woff/Raleway-bold-webfont.woff */ "./src/fonts/woff/Raleway-bold-webfont.woff");
__webpack_require__(/*! ../fonts/woff/raleway-regular-webfont.woff */ "./src/fonts/woff/raleway-regular-webfont.woff");
__webpack_require__(/*! ../fonts/woff2/raleway-bold-webfont.woff2 */ "./src/fonts/woff2/raleway-bold-webfont.woff2");
__webpack_require__(/*! ../fonts/woff2/raleway-regular-webfont.woff2 */ "./src/fonts/woff2/raleway-regular-webfont.woff2");
__webpack_require__(/*! ../img/clients/account-technologies.jpg */ "./src/img/clients/account-technologies.jpg");
__webpack_require__(/*! ../img/clients/arts-business-initiatives.jpg */ "./src/img/clients/arts-business-initiatives.jpg");
__webpack_require__(/*! ../img/clients/ascot-study.jpg */ "./src/img/clients/ascot-study.jpg");
__webpack_require__(/*! ../img/clients/astro-infused-water.jpg */ "./src/img/clients/astro-infused-water.jpg");
__webpack_require__(/*! ../img/clients/dimitris-papanastasiou.jpg */ "./src/img/clients/dimitris-papanastasiou.jpg");
__webpack_require__(/*! ../img/clients/dogs-and-cats-painted.jpg */ "./src/img/clients/dogs-and-cats-painted.jpg");
__webpack_require__(/*! ../img/clients/feather-flair.jpg */ "./src/img/clients/feather-flair.jpg");
__webpack_require__(/*! ../img/clients/foundation-for-circulatory-health.jpg */ "./src/img/clients/foundation-for-circulatory-health.jpg");
__webpack_require__(/*! ../img/clients/furthur-progressions.jpg */ "./src/img/clients/furthur-progressions.jpg");
__webpack_require__(/*! ../img/clients/health-n-wellbeing.jpg */ "./src/img/clients/health-n-wellbeing.jpg");
__webpack_require__(/*! ../img/clients/home-grown-textiles.jpg */ "./src/img/clients/home-grown-textiles.jpg");
__webpack_require__(/*! ../img/clients/parsley-and-thyme.jpg */ "./src/img/clients/parsley-and-thyme.jpg");
__webpack_require__(/*! ../img/clients/profile-image-square.jpg */ "./src/img/clients/profile-image-square.jpg");
__webpack_require__(/*! ../img/clients/red-wine-villas.jpg */ "./src/img/clients/red-wine-villas.jpg");
__webpack_require__(/*! ../img/clients/southern-belle-constructions.jpg */ "./src/img/clients/southern-belle-constructions.jpg");
__webpack_require__(/*! ../img/clients/tappily.jpg */ "./src/img/clients/tappily.jpg");
__webpack_require__(/*! ../img/clients/the-bridge-inn-amberley.jpg */ "./src/img/clients/the-bridge-inn-amberley.jpg");
__webpack_require__(/*! ../img/clients/vapoureze.jpg */ "./src/img/clients/vapoureze.jpg");
__webpack_require__(/*! ../img/shillington/bertrand-project-back-cover.jpg */ "./src/img/shillington/bertrand-project-back-cover.jpg");
__webpack_require__(/*! ../img/shillington/bertrand-project-front-cover.jpg */ "./src/img/shillington/bertrand-project-front-cover.jpg");
__webpack_require__(/*! ../img/shillington/bertrand-project-pages-2-3.jpg */ "./src/img/shillington/bertrand-project-pages-2-3.jpg");
__webpack_require__(/*! ../img/shillington/bertrand-project-pages-4-5.jpg */ "./src/img/shillington/bertrand-project-pages-4-5.jpg");
__webpack_require__(/*! ../img/shillington/bertrand-project-pages-6-7.jpg */ "./src/img/shillington/bertrand-project-pages-6-7.jpg");
__webpack_require__(/*! ../img/shillington/city-identity-booking-page.jpg */ "./src/img/shillington/city-identity-booking-page.jpg");
__webpack_require__(/*! ../img/shillington/city-identity-contact-page.jpg */ "./src/img/shillington/city-identity-contact-page.jpg");
__webpack_require__(/*! ../img/shillington/city-identity-homepage.jpg */ "./src/img/shillington/city-identity-homepage.jpg");
__webpack_require__(/*! ../img/shillington/city-identity-project-filters.jpg */ "./src/img/shillington/city-identity-project-filters.jpg");
__webpack_require__(/*! ../img/shillington/corporate-calculation-page.png */ "./src/img/shillington/corporate-calculation-page.png");
__webpack_require__(/*! ../img/shillington/corporate-website-homepage.png */ "./src/img/shillington/corporate-website-homepage.png");
__webpack_require__(/*! ../img/shillington/corporate-website-previous-savings.png */ "./src/img/shillington/corporate-website-previous-savings.png");
__webpack_require__(/*! ../img/shillington/corporate-website-savings-page.png */ "./src/img/shillington/corporate-website-savings-page.png");
__webpack_require__(/*! ../img/shillington/epub-magazine-feature.jpg */ "./src/img/shillington/epub-magazine-feature.jpg");
__webpack_require__(/*! ../img/shillington/epub-magazine-homepage.jpg */ "./src/img/shillington/epub-magazine-homepage.jpg");
__webpack_require__(/*! ../img/shillington/epub-magazine-introduction.jpg */ "./src/img/shillington/epub-magazine-introduction.jpg");
__webpack_require__(/*! ../img/shillington/half-epub.jpg */ "./src/img/shillington/half-epub.jpg");
__webpack_require__(/*! ../img/shillington/hamburger.svg */ "./src/img/shillington/hamburger.svg");
__webpack_require__(/*! ../img/shillington/handmade-book-cover-back.jpg */ "./src/img/shillington/handmade-book-cover-back.jpg");
__webpack_require__(/*! ../img/shillington/handmade-book-cover-front.jpg */ "./src/img/shillington/handmade-book-cover-front.jpg");
__webpack_require__(/*! ../img/shillington/handmade-book-cover-front-back.jpg */ "./src/img/shillington/handmade-book-cover-front-back.jpg");
__webpack_require__(/*! ../img/shillington/handmade-project-slide.jpg */ "./src/img/shillington/handmade-project-slide.jpg");
__webpack_require__(/*! ../img/shillington/handmade-project-slide-mobile.png */ "./src/img/shillington/handmade-project-slide-mobile.png");
__webpack_require__(/*! ../img/shillington/jennifer-ersoz-jones.png */ "./src/img/shillington/jennifer-ersoz-jones.png");
__webpack_require__(/*! ../img/shillington/quarter-size-ipad-epub.png */ "./src/img/shillington/quarter-size-ipad-epub.png");
__webpack_require__(/*! ../img/shillington/small-business-logo-login.jpg */ "./src/img/shillington/small-business-logo-login.jpg");
__webpack_require__(/*! ../img/shillington/small-business-logo-login-entered.jpg */ "./src/img/shillington/small-business-logo-login-entered.jpg");
__webpack_require__(/*! ../img/shillington/small-business-logo-login-error.jpg */ "./src/img/shillington/small-business-logo-login-error.jpg");
__webpack_require__(/*! ../img/shillington/small-business-logo-tooltips.jpg */ "./src/img/shillington/small-business-logo-tooltips.jpg");
__webpack_require__(/*! ../img/shillington/spark-2ipad-layouts.png */ "./src/img/shillington/spark-2ipad-layouts.png");
__webpack_require__(/*! ../img/shillington/SPARK3ipad-layouts.png */ "./src/img/shillington/SPARK3ipad-layouts.png");
__webpack_require__(/*! ../img/shillington/spark4ipad-layouts.png */ "./src/img/shillington/spark4ipad-layouts.png");
__webpack_require__(/*! ../img/shillington/sparkipad-layouts.png */ "./src/img/shillington/sparkipad-layouts.png");
__webpack_require__(/*! ../img/blog/gbk.jpg */ "./src/img/blog/gbk.jpg");
__webpack_require__(/*! ../img/blog/michael-craig-martin.jpg */ "./src/img/blog/michael-craig-martin.jpg");
__webpack_require__(/*! ../img/blog/my-logo-redesigned-day-1.png */ "./src/img/blog/my-logo-redesigned-day-1.png");
__webpack_require__(/*! ../img/blog/safe_image.jpg */ "./src/img/blog/safe_image.jpg");
__webpack_require__(/*! ../img/social/f_logo_RGB-Blue_100.png */ "./src/img/social/f_logo_RGB-Blue_100.png");
__webpack_require__(/*! ../img/social/linkedin.png */ "./src/img/social/linkedin.png");
__webpack_require__(/*! ../img/social/Twitter_Social_Icon_Circle_Color.png */ "./src/img/social/Twitter_Social_Icon_Circle_Color.png");
__webpack_require__(/*! ../img/favicon/android-chrome-36x36.png */ "./src/img/favicon/android-chrome-36x36.png");
__webpack_require__(/*! ../img/favicon/android-chrome-48x48.png */ "./src/img/favicon/android-chrome-48x48.png");
__webpack_require__(/*! ../img/favicon/android-chrome-72x72.png */ "./src/img/favicon/android-chrome-72x72.png");
__webpack_require__(/*! ../img/favicon/android-chrome-96x96.png */ "./src/img/favicon/android-chrome-96x96.png");
__webpack_require__(/*! ../img/favicon/android-chrome-144x144.png */ "./src/img/favicon/android-chrome-144x144.png");
__webpack_require__(/*! ../img/favicon/android-chrome-192x192.png */ "./src/img/favicon/android-chrome-192x192.png");
__webpack_require__(/*! ../img/favicon/android-chrome-256x256.png */ "./src/img/favicon/android-chrome-256x256.png");
__webpack_require__(/*! ../img/favicon/android-chrome-384x384.png */ "./src/img/favicon/android-chrome-384x384.png");
__webpack_require__(/*! ../img/favicon/android-chrome-512x512.png */ "./src/img/favicon/android-chrome-512x512.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon.png */ "./src/img/favicon/apple-touch-icon.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-57x57.png */ "./src/img/favicon/apple-touch-icon-57x57.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-60x60.png */ "./src/img/favicon/apple-touch-icon-60x60.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-72x72.png */ "./src/img/favicon/apple-touch-icon-72x72.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-76x76.png */ "./src/img/favicon/apple-touch-icon-76x76.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-114x114.png */ "./src/img/favicon/apple-touch-icon-114x114.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-120x120.png */ "./src/img/favicon/apple-touch-icon-120x120.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-144x144.png */ "./src/img/favicon/apple-touch-icon-144x144.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-152x152.png */ "./src/img/favicon/apple-touch-icon-152x152.png");
__webpack_require__(/*! ../img/favicon/apple-touch-icon-180x180.png */ "./src/img/favicon/apple-touch-icon-180x180.png");
__webpack_require__(/*! ../img/favicon/favicon.ico */ "./src/img/favicon/favicon.ico");
__webpack_require__(/*! ../img/favicon/favicon-16x16.png */ "./src/img/favicon/favicon-16x16.png");
__webpack_require__(/*! ../img/favicon/favicon-32x32.png */ "./src/img/favicon/favicon-32x32.png");
__webpack_require__(/*! ../img/favicon/favicon-194x194.png */ "./src/img/favicon/favicon-194x194.png");
__webpack_require__(/*! ../img/favicon/mstile-70x70.png */ "./src/img/favicon/mstile-70x70.png");
__webpack_require__(/*! ../img/favicon/mstile-144x144.png */ "./src/img/favicon/mstile-144x144.png");
__webpack_require__(/*! ../img/favicon/mstile-150x150.png */ "./src/img/favicon/mstile-150x150.png");
__webpack_require__(/*! ../img/favicon/mstile-310x150.png */ "./src/img/favicon/mstile-310x150.png");
__webpack_require__(/*! ../img/favicon/mstile-310x310.png */ "./src/img/favicon/mstile-310x310.png");
__webpack_require__(/*! ../img/favicon/safari-pinned-tab.svg */ "./src/img/favicon/safari-pinned-tab.svg");
const react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));
const client_1 = __webpack_require__(/*! react-dom/client */ "./node_modules/react-dom/client.js");
const BaseLayout_1 = __importDefault(__webpack_require__(/*! ./components/BaseLayout */ "./src/code/components/BaseLayout.tsx"));
const rootElement = document.getElementById("root");
const root = client_1.createRoot(rootElement);
root.render(react_1.default.createElement(react_1.default.StrictMode, null,
    react_1.default.createElement(BaseLayout_1.default, null)));


/***/ }),

/***/ "./src/code/state.ts":
/*!***************************!*\
  !*** ./src/code/state.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createState = void 0;
const SMTP = __importStar(__webpack_require__(/*! ./SMTP */ "./src/code/SMTP.ts"));
const immer_1 = __importDefault(__webpack_require__(/*! immer */ "./node_modules/immer/dist/immer.esm.js"));
function createState(inParentComponent) {
    return {
        hamburgerImage: {
            src: `./images/shillington/hamburger9525.svg`,
            height: 37,
            width: 47,
            alt: `hamburger icon for mobile menu`
        },
        carouselHeight: "440",
        state: false,
        siteLoad: {
            homeFirstLoad: true,
            clientsFirstLoad: true,
            windowResized: false,
            pageLoaded: "",
            currentProject: 0
        },
        checkIsMounted: {
            isMounted: false,
            isUnmounted: false
        },
        mobileMenu: {
            anchorEl: null,
            open: false
        },
        introSlider: {
            introHeight: "",
            introTop: "",
            temporaryImage: true
        },
        hasError: false,
        logoImages: {
            image: `./images/shillington/jennifer-ersoz-jones3cba.jpg`,
            webp: `./images/shillington/jennifer-ersoz-jones3cba.webp`,
            width: 846,
            height: 261
        },
        previousClients: {
            hidden: false,
            heading: `Designs`,
            title: `Previous Client Websites:`,
            content: `<p>Here I display a variety of websites that I have designed and built for the clients Mr Site. Taking note of customer requirements in their consultations I was able to provide customers with suitable solutions for their business needs.</p><p>In addition to these sites I include website screenshots of my own clients websites: Foundation for Circulatory Health, Ascot Study, Astro Infused and Jaylouv Studios. Further I have included screenshots of two websites I have built however not designed for Account Technologies.</p><p>I have included links to these websites, however some are no longer live or now look different to what they were as they were created some time ago. A Wayback Machine link has been included to provide physical representation of these removed sites or what the site used to look like when I had finished designing and or building.</p>`
        },
        profile: {
            heading: `Profile`,
            title: `Jennifer Ersoz-Jones`,
            content: `<p>My first client website stemmed from my BSc degree in Information Systems and Computing. One of our projects involved the creation of a website for a non-profit organisation. This organisation was the Foundation for Circulatory Health. Originally this project started off as a redesign of a one page website. The site has now grown to 40+ pages.</p><p>Following this I continued working as a freelance Web Designer and additionally pursued a Web Design career at Mr Site. From my time at Mr Site I have created 100+ websites for the company's customers. Since leaving Mr Site I went on to work as an in house Web and Email Designer and Developer. At present I am working as a Freelance Web Designer and Developer. Within this role I have worked on varied websites providing support and tutorials where possible.</p><p>My portfolio website was originally built to showcase the design skills I had learnt from taking an intensive Graphic Design course at Shillington College during 2015. The last assignment of the course was to create a website to promote the work I had completed whilst on the course. However due to time constraints of the project I decided to build the website using a popular content managment system. Some time later an opportunity aroze during April 2020 where I was put onto furlough amid the Coronavirus pandemic. Making the most of the ample time available I decided to upskill again and pushed myself forward to learn the latest fullstack development technologies. Whereby I learnt React, TypeScript, webpack, GitLab and various supporting npm packages such as Google's Material-UI and Immer. From learning these new technologies I decided to apply this new tech and rebuild my portfolio website from scratch.</p>`,
            image: `./images/clients/profile-image-squaredf96.jpg`,
            webp: `./images/clients/profile-image-squaredf96.webp`,
            alt: `profile photo infront of red brick wall`,
            imageWidth: 844,
            imageHeight: 843
        },
        salesImages: {
            image: `./images/shillington/half-epub8383.jpg`,
            webp: `./images/shillington/half-epub8385.webp`,
            width: 819,
            height: 300,
            mobileImage: `./images/shillington/quarter-size-ipad-epub30ca.png`,
            mobileWebP: `./images/shillington/quarter-size-ipad-epub30ca.webp`,
            mobileWidth: 410,
            mobileHeight: 300,
            alt: `artwork on ipad`
        },
        sales: [
            {
                title: `<h1>New year, New <strong>Website</strong>?</h1>`,
                content: `<h2>Whether you are looking to refresh the appearance of your current website or wanting a fantastic new site to start the New Year, I have a range of solutions to suit your website needs...</h2><h3><strong>Prices</strong></h3><ul><li>Whole sites <b>&pound;250</b></li><li>Includes: 5 page website, 1 domain, SEO, and 5 page mobile website (Extra pages optional, <b>&pound;60</b> each)</li><li>Individual pages <b>&pound;60</b> each</li><li>Includes: 1 webpage, 1 mobile page and SEO</li></ul><p>N.b. Hosting not included in pricing structure</p>`
            },
            {
                title: `<h2>Your website build:</h2>`,
                content: `<p>The Process for the build of a website is in several steps for completion:</p><ol class="nested-counter-list"><li>Consultation & payment<ol><li>A consultation will take place between the web designer and the customer regarding their website. Upon approval of the plan for the website a payment of 50% of the project will be made before the start of work.</li></ol></li><li>Website Assets<ol><li>The web designer received all the website materials from the customer before starting work on the website.</li></ol></li><li>First build<ol><li>The first design of the project will happen at this stage and a hidden view of this first design of the website is sent to the customer for checking and feedback before continued development of website.</li></ol></li><li>Iteration 1<ol><li>In response to the first design feedback changes are completed by the web designer and then the website is sent again to the customer for further checking and feedback. This customer feedback is to be supplied to the web designer for further development.</li></ol></li><li>Iteration 2<ol><li>Final feedback from the customer is received by the web designer in response to iteration 1. This feedback of changes is then completed by the web designer to complete the project. The website is then complete. The final 50% of payment is then made to the designer.</li></ol></li></ol>`
            },
            {
                title: `<h2>Terms & Conditions:</h2>`,
                content: `<ol class="nested-counter-list"><li>Websites<ol><li>Websites will be completed within 4 weeks</li><li>Website materials including: Photos, Text, and Videos need to be provided before website build</li></ol></li><li>Products<ol><li>Product assets must be supplied before the start of a project (including: titles of products, description of products, product variations, product images and product prices).</li><li>Only 30 products are to be uploaded by the web designer per page of the website.</li></ol></li><li>Refunds<ol><li>Full refunds shall only be given if work has not been completed. Partial refunds will be given depending upon the stage of completion in the project.</li></ol></li><li>Videos<ol><li>Videos should be hosted on youtube or vimeo by the customer before being supplied to the website designer. If this is not so an extra fee will be applied so that the videos are hosted on either youtube or vimeo before insertion into website.</li></ol></li><li>Payment<ol><li>A sum of 50% of the website fee will be taken at the start of the project as a deposit towards the work. The final 50% agreed payment amount is to be paid by the customer upon completion of the project.</li></ol></li><li>Tutorials<ol><li>At the completion of the project tutorials can be requested. Tutorials will be provided in 30 minute allocations and they can take place via Skype or Phone. The price of a tutorial is £20</li></ol></li><li>Maintenance<ol><li>Maintenance work of websites can be completed for customers at a rate of £10 per hour.</li></ol></li></ol>`
            },
            {
                title: `<h2>Guidelines:</h2>`,
                content: `<ol class="nested-counter-list"><li>Images<ol><li>Images should be supplied in the format of either PDF, PSD, PNG, AI, WebP, JPEG, JPEG2000 or JPG.</li><li>Preference of images to be supplied at the highest resolution possible i.e. 300 dpi</li><li>Images for products should ideally be taken on a solid colour background without any obstructive element taking the focus of the individual product.</li><li>To keep within legal requirements images should have copyright permission. If images are taken from google they should hold the right for reuse. When looking for suitable images in google under the images tab of the google search engine select Usage rights > Labeled for reuse. This will allow you to pick images that are suitable for your website.</li></ol></li></ol>`
            }
        ],
        blogPosts: [
            {
                image: `./images/blog/gbk1dc1.jpg`,
                webp: `./images/blog/gbk1dc1.webp`,
                width: 900,
                height: 1601,
                alt: `Gourmet Burger Kitchen - Incoming: Burger Joy`,
                title: `Clever word play.`,
                content: ``,
                date: `22 January 2016`
            },
            {
                image: `./images/blog/safe_image86e3.jpg`,
                webp: `./images/blog/safe_image86e3.webp`,
                width: 470,
                height: 245,
                alt: `Aspects of web design`,
                title: `Web Trends 2016!`,
                content: `<p>Great design trends of 2016! Minimalism, Pixellation, Video Backgrounds, Statement Typography and Grid Free...</p><a target="_blank" rel="noopener" class="clientsLink" href="https://creativemarket.com/blog/2016/01/04/top-web-design-trends-for-2016">https://creativemarket.com/blog/2016/01/04/top-web-design-trends-for-2016</a>`,
                date: `21 January 2016`
            },
            {
                image: `./images/blog/michael-craig-martine17e.jpg`,
                webp: `./images/blog/michael-craig-martine17e.webp`,
                width: 2044,
                height: 3778,
                alt: `Outline of a lightbulb in metal stood in a park`,
                title: `Craig-Martin in the park`,
                content: `<p>Michael Craig Martin (MCM) at the Serpentine Gallery, Kensington Gardens. It was great to see the work of MCM today. A designer whose work I haven&rsquo;t encountered since leaving Secondary School. Large canvases are displayed covered with illustrations of everyday electrical and household objects painted in vibrant colours. MCM's style is closely linked to that of Patrick Caulfield. Don't miss out, the exhibition ends on Valentines Day!</p>`,
                date: `18 January 2016`
            },
            {
                image: `./images/blog/my-logo-redesigned-day-10c1d.png`,
                webp: `./images/blog/my-logo-redesigned-day-10c1d.webp`,
                width: 960,
                height: 560,
                title: `My first blog post!`,
                alt: `redesign of Jennifer Ersoz-Jones logo`,
                content: `<p>To welcome my new blog I thought I'd kick off with a brand new feature to the site. This new feature is 'A logo day'. 'A logo day' posts are featured to display an individual logo that I have created based on a particular theme.</p><p>As a starting point I display my own logo redesigned to the keywords of medium, constant and helpful. These keywords depict how I visualise myself.</p><p>The logos that I create are just for fun and help to push my creative ability.</p>`,
                date: `14 January 2016`
            }
        ],
        previousClientsSites: [
            {
                image: './images/clients/account-technologies780b.jpg',
                webp: './images/clients/account-technologies780b.webp',
                width: 1366,
                height: 768,
                alt: 'account technologies website',
                active: false,
                key: 'account-technologies',
                link: 'http://www.accounttechnologies.com/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20180602220734/https://www.accounttechnologies.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/arts-business-initiatives3cad.jpg',
                webp: './images/clients/arts-business-initiatives3cad.webp',
                width: 997,
                height: 608,
                alt: 'arts business initiatives website',
                active: false,
                key: 'arts-business-initiatives',
                link: 'http://www.artsbusinessinitiatives.com/',
                hasLink: true,
                linkActive: false,
                wayBackLink: 'https://web.archive.org/web/20150505034545/http://www.artsbusinessinitiatives.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/ascot-studybe30.jpg',
                webp: './images/clients/ascot-studybe30.webp',
                width: 1121,
                height: 719,
                alt: 'ascot study website',
                active: false,
                key: 'ascot-study',
                link: 'http://ascotstudy.org.uk',
                hasLink: true,
                linkActive: true,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/astro-infused-waterb6ac.jpg',
                webp: './images/clients/astro-infused-waterb6ac.webp',
                width: 1120,
                height: 718,
                alt: 'astro infused water website',
                active: false,
                key: 'astro-infused-water',
                link: 'http://astroinfused.com/',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/dimitris-papanastasiou2041.jpg',
                webp: './images/clients/dimitris-papanastasiou2041.webp',
                width: 995,
                height: 628,
                alt: 'dimitris papanastasiou website',
                active: false,
                key: 'dimitris-papanastasiou-website',
                link: '#',
                hasLink: false,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/dogs-and-cats-painted0815.jpg',
                webp: './images/clients/dogs-and-cats-painted0815.webp',
                width: 995,
                height: 627,
                alt: 'dogs and cats painted website',
                active: false,
                key: 'dogs-and-cats-painted-website',
                link: 'http://www.dogsandcatspainted.com/home',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/feather-flair92e4.jpg',
                webp: './images/clients/feather-flair92e4.webp',
                width: 1120,
                height: 695,
                alt: 'feather flair website',
                active: false,
                key: 'feather-flair-website',
                link: 'http://www.featherflair.co.uk/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20160311005518/http://www.featherflair.co.uk/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/foundation-for-circulatory-health42d5.jpg',
                webp: './images/clients/foundation-for-circulatory-health42d5.webp',
                width: 910,
                height: 696,
                alt: 'foundation for circulatory health website',
                active: false,
                key: 'foundation-for-circulatory-health-website',
                link: 'http://www.ffch.org/',
                hasLink: true,
                linkActive: true,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/furthur-progressions2efd.jpg',
                webp: './images/clients/furthur-progressions2efd.webp',
                width: 994,
                height: 628,
                alt: 'furthur progressions website',
                active: false,
                key: 'furthur-progressions-website',
                link: 'https://www.furthurprogressions.com/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20150801164224/https://www.furthurprogressions.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/health-n-wellbeing4958.jpg',
                webp: './images/clients/health-n-wellbeing4958.webp',
                width: 1137,
                height: 868,
                alt: 'health n wellbeing website',
                active: false,
                key: 'health-n-wellbeing-website',
                link: '#',
                hasLink: false,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/home-grown-textiles1635.jpg',
                webp: './images/clients/home-grown-textiles1635.webp',
                width: 1401,
                height: 868,
                alt: 'home grown textiles website',
                active: false,
                key: 'home-grown-textiles-website',
                link: 'http://www.homegrowntextiles.co.uk/',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/parsley-and-thyme9797.jpg',
                webp: './images/clients/parsley-and-thyme9797.webp',
                width: 960,
                height: 628,
                alt: 'parsley and thyme website',
                active: true,
                key: 'parsley-and-thyme-website',
                link: 'https://www.parsleyandthymecatering.co.uk/',
                hasLink: true,
                linkActive: true,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/red-wine-villas20bb.jpg',
                webp: './images/clients/red-wine-villas20bb.webp',
                width: 1138,
                height: 683,
                alt: 'red wine villas website',
                active: false,
                key: 'red-wine-villas-website',
                link: 'http://www.redwinevillas.net/',
                hasLink: true,
                linkActive: false,
                wayBackLink: 'https://web.archive.org/web/20190727223749/http://redwinevillas.net/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/southern-belle-constructions5e56.jpg',
                webp: './images/clients/southern-belle-constructions5e56.webp',
                width: 1421,
                height: 879,
                alt: 'southern belle constructions website',
                active: false,
                key: 'southern-belle-construstions-website',
                link: 'https://sb-c.com.au/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20160314062311/http://sb-c.com.au/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/tappily975e.jpg',
                webp: './images/clients/tappily975e.webp',
                width: 1368,
                height: 772,
                alt: 'tappily',
                active: false,
                key: 'tappily-website',
                link: 'https://www.tappily.co.uk/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20181118225513/https://www.tappily.co.uk/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/the-bridge-inn-amberley29c5.jpg',
                webp: './images/clients/the-bridge-inn-amberley29c5.webp',
                width: 995,
                height: 606,
                alt: 'the bridge inn amberley website',
                active: false,
                key: 'the-bridge-inn-amberley-website',
                link: 'https://www.bridgeinnamberley.com/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20160131035045/http://bridgeinnamberley.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            {
                image: './images/clients/vapourezea52c.jpg',
                webp: './images/clients/vapourezea52c.webp',
                width: 1401,
                height: 898,
                alt: 'vapoureze website',
                active: false,
                key: 'vapoureze-website',
                link: 'http://vapoureze.eu',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            }
        ],
        error: null,
        errorInfo: null,
        messageSubject: "",
        messageFrom: "",
        messageBody: "",
        activeElement: 0,
        mobileBreakpoint: 600,
        messageTo: "jennifer.ersoz@gmail.com",
        baseUrl: '',
        portfolioTitle: `Portfolio`,
        portfolioContent: `<span class="startEmphasis">Hello</span> and welcome to my website. On this website you will find my portfolio below which is made up of varied projects I undertook whilst studying a graphic design course at Shillington College Summer 2015. In addition to the portfolio I also have examples of completed client websites. These can be found on the Clients page of this website.`,
        carouselPosition: [
            {
                title: 'projectHandmade',
                index: 0,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectHandmade-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectBertrand',
                index: 1,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectBertrand-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectCityIdentity',
                index: 2,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectCityIdentity-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectEPub',
                index: 3,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectEPub-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectCorporateWebsite',
                index: 4,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectCorporateWebsite-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectSmallBusinessLogo',
                index: 5,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectSmallBusinessLogo-active',
                windowWidthHalf: 0
            },
            {
                title: 'previousClientsSites',
                index: 6,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.previousClientsSites-active',
                windowWidthHalf: 0
            }
        ],
        menu: [
            {
                link: '/',
                title: 'Portfolio',
                active: true
            },
            {
                link: '/Clients',
                title: 'Clients & About',
                active: false
            },
            {
                link: '/Sales',
                title: 'Need a site?',
                active: false
            },
            {
                link: '/Blog',
                title: 'Blog',
                active: false
            }
        ],
        introCarousel: [
            {
                link: '#Handmade',
                title: 'Handmade',
                class: 'firstanimation',
                alt: 'front and back bookcover of birds nest',
                image: './images/shillington/handmade-project-slidec653.jpg',
                webp: `./images/shillington/handmade-project-slidec653.webp`,
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/handmade-project-slide-mobiled6d9.png',
                mobileWebp: './images/shillington/handmade-project-slide-mobiled6d9.webp',
                mobileWidth: 1500,
                mobileHeight: 704
            },
            {
                link: '#CityIdentity',
                title: 'City_Identity',
                class: 'secondanimation',
                alt: 'mobile view of tourism website',
                image: './images/shillington/city-identity-project-slide912d.jpg',
                webp: './images/shillington/city-identity-project-slide912d.webp',
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/city-identity-project-slide-mobiled7af.png',
                mobileWebp: './images/shillington/city-identity-project-slide-mobiled7af.webp',
                mobileWidth: 1500,
                mobileHeight: 681
            },
            {
                link: '#EpubMagazine',
                title: 'Epub_Magazine',
                class: 'thirdanimation',
                alt: 'epub view of cycling magazine article',
                image: './images/shillington/epub-magazine-project-slidef7fc.jpg',
                webp: './images/shillington/epub-magazine-project-slidef7fc.webp',
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/epub-magazine-project-slide-mobilea44d.png',
                mobileWebp: './images/shillington/epub-magazine-project-slide-mobilea44d.webp',
                mobileWidth: 1500,
                mobileHeight: 704
            },
            {
                link: '#SmallBusinessLogo',
                title: 'Small_Business_Logo',
                class: 'fourthanimation',
                alt: 'postcards advertising online courses',
                image: './images/shillington/small-business-logo-slidef73c.jpg',
                webp: './images/shillington/small-business-logo-slidef73c.webp',
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/small-business-logo-slide-mobileecc5.jpg',
                mobileWebp: './images/shillington/small-business-logo-slide-mobileecc5.webp',
                mobileWidth: 1500,
                mobileHeight: 704
            }
        ],
        projectHandmade: [
            {
                image: './images/shillington/handmade-book-cover-front067d.jpg',
                webp: './images/shillington/handmade-book-cover-front067d.webp',
                width: 1037,
                height: 1147,
                alt: 'front cover of handmade design on book',
                active: false,
                key: 'handmade-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            {
                image: './images/shillington/handmade-book-cover-front-back4867.jpg',
                webp: './images/shillington/handmade-book-cover-front-back4867.webp',
                width: 1024,
                height: 768,
                alt: 'front and back cover of handmade design on book',
                active: false,
                key: 'handmade-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            {
                image: './images/shillington/handmade-book-cover-back2c99.jpg',
                webp: './images/shillington/handmade-book-cover-back2c99.webp',
                width: 1296,
                height: 1434,
                alt: 'back cover of handmade design on book',
                active: false,
                key: 'handmade-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            {
                image: './images/shillington/handmade-book-cover-front067d.jpg',
                webp: './images/shillington/handmade-book-cover-front067d.webp',
                width: 1037,
                height: 1147,
                alt: 'front cover of handmade design on book',
                active: true,
                key: 'handmade-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            {
                image: './images/shillington/handmade-book-cover-front-back4867.jpg',
                webp: './images/shillington/handmade-book-cover-front-back4867.webp',
                width: 1024,
                height: 768,
                alt: 'front and back cover of handmade design on book',
                active: false,
                key: 'handmade-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            {
                image: './images/shillington/handmade-book-cover-back2c99.jpg',
                webp: './images/shillington/handmade-book-cover-back2c99.webp',
                width: 1296,
                height: 1434,
                alt: 'back cover of handmade design on book',
                active: false,
                key: 'handmade-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            }
        ],
        projectBertrand: [
            {
                image: './images/shillington/bertrand-project-front-cover01ae.jpg',
                webp: './images/shillington/bertrand-project-front-cover01ae.webp',
                width: 2048,
                height: 1536,
                alt: 'front cover design of leaflet',
                active: false,
                key: 'bertrand-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            {
                image: './images/shillington/bertrand-project-pages-2-36af4.jpg',
                webp: './images/shillington/bertrand-project-pages-2-36af4.webp',
                width: 1024,
                height: 768,
                alt: 'pages 2 and 3 of leaflet',
                active: false,
                key: 'bertrand-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            {
                image: './images/shillington/bertrand-project-pages-4-5e8e7.jpg',
                webp: './images/shillington/bertrand-project-pages-4-5e8e7.webp',
                width: 1024,
                height: 768,
                alt: 'pages 4 and 5 of leaflet',
                active: true,
                key: 'bertrand-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            {
                image: './images/shillington/bertrand-project-pages-6-7bc25.jpg',
                webp: './images/shillington/bertrand-project-pages-6-7bc25.webp',
                width: 1024,
                height: 768,
                alt: 'pages 6 and 7 of leaflet',
                active: false,
                key: 'bertrand-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            {
                image: `./images/shillington/bertrand-project-back-coverfc6d.jpg`,
                webp: './images/shillington/bertrand-project-back-coverfc6d.webp',
                width: 1024,
                height: 768,
                alt: 'back cover design of leaflet',
                active: false,
                key: 'bertrand-original-5',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            }
        ],
        projectCityIdentity: [
            {
                image: './images/shillington/city-identity-homepage91db.jpg',
                webp: './images/shillington/city-identity-homepage91db.webp',
                width: 353,
                height: 500,
                alt: 'hompage of city identity website',
                active: false,
                key: 'city-identity-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            {
                image: './images/shillington/city-identity-project-filters1edb.jpg',
                webp: './images/shillington/city-identity-project-filters1edb.webp',
                width: 353,
                height: 500,
                alt: 'filters of city identity website',
                active: false,
                key: 'city-identity-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            {
                image: './images/shillington/city-identity-booking-pagecdd3.jpg',
                webp: './images/shillington/city-identity-booking-pagecdd3.webp',
                width: 353,
                height: 500,
                alt: 'booking page of city identity website',
                active: false,
                key: 'city-identity-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            {
                image: './images/shillington/city-identity-contact-pagefce7.jpg',
                webp: './images/shillington/city-identity-contact-pagefce7.webp',
                width: 353,
                height: 500,
                alt: 'contact page of city identity website',
                active: false,
                key: 'city-identity-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            {
                image: './images/shillington/city-identity-homepage91db.jpg',
                webp: './images/shillington/city-identity-homepage91db.webp',
                idth: 353,
                height: 500,
                alt: 'hompage of city identity website',
                active: true,
                key: 'city-identity-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            {
                image: './images/shillington/city-identity-project-filters1edb.jpg',
                webp: './images/shillington/city-identity-project-filters1edb.webp',
                width: 353,
                height: 500,
                alt: 'filters of city identity website',
                active: false,
                key: 'city-identity-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            {
                image: './images/shillington/city-identity-booking-pagecdd3.jpg',
                webp: './images/shillington/city-identity-booking-pagecdd3.webp',
                width: 353,
                height: 500,
                alt: 'booking page of city identity website',
                active: false,
                key: 'city-identity-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            {
                image: './images/shillington/city-identity-contact-pagefce7.jpg',
                webp: './images/shillington/city-identity-contact-pagefce7.webp',
                width: 353,
                height: 500,
                alt: 'contact page of city identity website',
                active: false,
                key: 'city-identity-duplicate-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            }
        ],
        projectEPub: [
            {
                image: './images/shillington/epub-magazine-homepage6dec.jpg',
                webp: './images/shillington/epub-magazine-homepage6dec.webp',
                width: 819,
                height: 601,
                alt: 'homepage magazine cover',
                active: false,
                key: 'epub-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            {
                image: './images/shillington/epub-magazine-introductionb7a0.jpg',
                webp: './images/shillington/epub-magazine-introductionb7a0.webp',
                width: 600,
                height: 440,
                alt: 'introduction page of magazine cover',
                active: false,
                key: 'epub-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            {
                image: './images/shillington/epub-magazine-featurefcc8.jpg',
                webp: './images/shillington/epub-magazine-featurefcc8.webp',
                width: 819,
                height: 601,
                alt: 'feature page of magazine cover',
                active: false,
                key: 'epub-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            {
                image: './images/shillington/epub-magazine-homepage6dec.jpg',
                webp: './images/shillington/epub-magazine-homepage6dec.webp',
                width: 819,
                height: 601,
                alt: 'homepage magazine cover',
                active: true,
                key: 'epub-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            {
                image: './images/shillington/epub-magazine-introductionb7a0.jpg',
                webp: './images/shillington/epub-magazine-introductionb7a0.webp',
                width: 819,
                height: 601,
                alt: 'introduction page of magazine cover',
                active: false,
                key: 'epub-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            {
                image: './images/shillington/epub-magazine-featurefcc8.jpg',
                webp: './images/shillington/epub-magazine-featurefcc8.webp',
                width: 819,
                height: 601,
                alt: 'feature page of magazine cover',
                active: false,
                key: 'epub-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            }
        ],
        projectCorporateWebsite: [
            {
                image: './images/shillington/corporate-website-homepage82f4.png',
                webp: './images/shillington/corporate-website-homepage82f4.webp',
                width: 800,
                height: 600,
                alt: 'hompage of corporate website',
                active: false,
                key: 'corporate-website-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            {
                image: './images/shillington/corporate-calculation-page904f.png',
                webp: './images/shillington/corporate-calculation-page904f.webp',
                width: 800,
                height: 600,
                alt: 'calculation page of corporate website',
                active: false,
                key: 'corporate-website-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            {
                image: './images/shillington/corporate-website-savings-page67d8.png',
                webp: './images/shillington/corporate-website-savings-page67d8.webp',
                width: 800,
                height: 600,
                alt: 'savings page of corporate website',
                active: false,
                key: 'corporate-website-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            {
                image: './images/shillington/corporate-website-previous-savings0238.png',
                webp: './images/shillington/corporate-website-previous-savings0238.webp',
                width: 800,
                height: 600,
                alt: 'previous savings page of corporate website',
                active: false,
                key: 'corporate-website-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            {
                image: './images/shillington/corporate-website-homepage82f4.png',
                webp: './images/shillington/corporate-website-homepage82f4.webp',
                width: 800,
                height: 600,
                alt: 'hompage of corporate website',
                active: true,
                key: 'corporate-website-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            {
                image: './images/shillington/corporate-calculation-page904f.png',
                webp: './images/shillington/corporate-calculation-page904f.webp',
                width: 800,
                height: 600,
                alt: 'calculation page of corporate website',
                active: false,
                key: 'corporate-website-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            {
                image: './images/shillington/corporate-website-savings-page67d8.png',
                webp: './images/shillington/corporate-website-savings-page67d8.webp',
                width: 800,
                height: 600,
                alt: 'savings page of corporate website',
                active: false,
                key: 'corporate-website-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            {
                image: './images/shillington/corporate-website-previous-savings0238.png',
                webp: './images/shillington/corporate-website-previous-savings0238.webp',
                width: 800,
                height: 600,
                alt: 'previous savings page of corporate website',
                active: false,
                key: 'corporate-website-duplicate-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            }
        ],
        projectSmallBusinessLogo: [
            {
                image: './images/shillington/small-business-logo-loginc388.jpg',
                webp: './images/shillington/small-business-logo-loginc388.webp',
                width: 1024,
                height: 751,
                alt: 'login webpage',
                active: false,
                key: 'small-business-logo-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            {
                image: './images/shillington/small-business-logo-tooltips07bd.jpg',
                webp: './images/shillington/small-business-logo-tooltips07bd.webp',
                width: 1024,
                height: 751,
                alt: 'tooltips display on login fields of webpage',
                active: false,
                key: 'small-business-logo-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            {
                image: './images/shillington/small-business-logo-login-entered5b04.jpg',
                webp: './images/shillington/small-business-logo-login-entered5b04.webp',
                width: 1024,
                height: 751,
                alt: 'login entered on login page',
                active: false,
                key: 'small-business-logo-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            {
                image: './images/shillington/small-business-logo-login-error4bcc.jpg',
                webp: './images/shillington/small-business-logo-login-error4bcc.webp',
                width: 1024,
                height: 751,
                alt: 'fields with errors on login page',
                active: false,
                key: 'small-business-logo-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            {
                image: './images/shillington/small-business-logo-loginc388.jpg',
                webp: './images/shillington/small-business-logo-loginc388.webp',
                width: 1024,
                height: 751,
                alt: 'login webpage',
                active: true,
                key: 'small-business-logo-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            {
                image: './images/shillington/small-business-logo-tooltips07bd.jpg',
                webp: './images/shillington/small-business-logo-tooltips07bd.webp',
                width: 1024,
                height: 751,
                alt: 'tooltips display on login fields of webpage',
                active: false,
                key: 'small-business-logo-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            {
                image: './images/shillington/small-business-logo-login-entered5b04.jpg',
                webp: './images/shillington/small-business-logo-login-entered5b04.webp',
                width: 1024,
                height: 751,
                alt: 'login entered on login page',
                active: false,
                key: 'small-business-logo-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            {
                image: './images/shillington/small-business-logo-login-error4bcc.jpg',
                webp: './images/shillington/small-business-logo-login-error4bcc.webp',
                width: 1024,
                height: 751,
                alt: 'fields with errors on login page',
                active: false,
                key: 'small-business-logo-duplicate-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            }
        ],
        socialIcons: [
            {
                title: 'Twitter',
                image: './images/social/Twitter_Social_Icon_Circle_Colorbaed.png',
                webp: './images/social/Twitter_Social_Icon_Circle_Colorbaed.webp',
                width: 400,
                height: 400,
                link: 'https://twitter.com/JenniferErsoz',
                alt: 'Twitter Profile'
            },
            {
                title: 'Facebook',
                image: './images/social/f_logo_RGB-Blue_1024db47.png',
                webp: './images/social/f_logo_RGB-Blue_100db47.webp',
                width: 217,
                height: 218,
                link: 'https://www.facebook.com/JenniferErsozWebAndGraphicDesigner',
                alt: 'Facebook Page'
            },
            {
                title: 'LinkedIn',
                image: './images/social/linkedin206a.png',
                webp: './images/social/linkedin206a.webp',
                width: 382,
                height: 382,
                link: 'https://www.linkedin.com/in/jennifer-ersoz-jones/',
                alt: 'LinkedIn Profile'
            }
        ],
        projectsIntro: [
            {
                title: 'Handmade Project:',
                content: `The brief of this project was to create a handmade item for a physical end product. My interpretation of this brief was the creation of a nest for a book cover entitled Birdsong by Sebastian Faulks. The nest is symbolic of the beginnings and endings of the characters' relationships within the book.`
            },
            {
                title: `Bertrand Project:`,
                content: `The brief of this project was to create a recruitment brochure for the Bertrand company to help inspire employees and prospective applicants to join and stay with the company throughout their careers. My response to the brief was to enhance the depiction of direction and building blocks within the brochure.`
            },
            {
                title: `City Identity Project:`,
                content: `The brief of this project was to create an identity for a small city which is currently either undergoing development or reconstruction. Fethiye was chosen for the project and as a result a logo was created based on the history of the city, the relaxing atmosphere it provides and the many experiences to discover there.`
            },
            {
                title: `Epub Magazine Project:`,
                content: `The brief of this project was to create an epub magazine intended for newbies to cycling. My interpretation of this brief stemmed from the idea of participation to form the design. An end result was created whereby pieces of the design are puzzled together along with the underlying repetitive movement from the action of cycling.`
            },
            {
                title: `Corporate Website Project:`,
                content: `The brief of this project was to design a website for children aged 7-10. This site is designed to help them learn how to save money. The saving of money provides security for an unknown future. The client request for this project was to create an online calculator whereby children can easily view their savings over time. My answer to this brief was the design of an interactive game which would simulate the way in which a bank would weigh money.`
            },
            {
                title: `Small Business Logo Project:`,
                content: `The purpose of this brief was to create a logo for a small online business. The online business was for a company who wanted to provide online lifestyle courses. My answer to this brief was the creation of a logo with the word spark to enforce the idea of the desire to learn. A roll out of the identity was created for the company.`
            }
        ],
        getDerivedStateFromError: function () {
            return { hasError: true };
        }.bind(inParentComponent),
        handleMenuLink: function (i) {
            let loadedPage = '';
            sessionStorage.setItem('activePage', i);
            if (i !== null) {
                this.setState(immer_1.default(this.state, draftState => {
                    draftState.menu.map(menuLink => {
                        menuLink.active = false;
                    });
                    draftState.menu[i].active = true;
                }));
                if (i === 0) {
                    loadedPage = 'homePage';
                }
                else if (i === 1) {
                    loadedPage = 'clientsPage';
                }
                setTimeout(() => {
                    this.setState(immer_1.default(this.state, draftState => {
                        draftState.siteLoad.pageLoaded = loadedPage;
                    }));
                    this.state.handleCarouselPos(loadedPage);
                }, 1250);
            }
        }.bind(inParentComponent),
        handleReloadedPage: function () {
            const savedActivePage = sessionStorage.getItem('activePage');
            if (savedActivePage !== null) {
                this.setState(immer_1.default(this.state, draftState => {
                    draftState.menu.map(menuLink => {
                        menuLink.active = false;
                    });
                    draftState.menu[savedActivePage].active = true;
                }));
            }
        }.bind(inParentComponent),
        componentDidCatch: function (error, errorInfo) {
            return {
                error: error,
                errorInfo: errorInfo
            };
        }.bind(inParentComponent),
        handleClick: function (event) {
            this.setState(immer_1.default(this.state.mobileMenu, draftState => {
                draftState.mobileMenu = {
                    anchorEl: event.currentTarget,
                    open: true
                };
            }));
        }.bind(inParentComponent),
        handleClose: function () {
            this.setState(immer_1.default(draftState => {
                draftState.mobileMenu = {
                    anchorEl: null,
                    open: false
                };
            }));
        }.bind(inParentComponent),
        handleCarouselLink: function (event, index, projectId) {
            event.preventDefault;
            this.setState(immer_1.default(this.state, draftState => {
                draftState[`${projectId}`].map(carouselLink => {
                    carouselLink.active = false;
                });
                draftState[`${projectId}`][index].active = true;
            }));
            this.state.handleProjectId(projectId);
            this.state.handleCarouselPos(projectId, index);
        }.bind(inParentComponent),
        recalculateCarouselPosition: function (projectId) {
            if (((!this.state.siteLoad.clientsFirstLoad && !this.state.siteLoad.windowResized) && this.state.siteLoad.pageLoaded === "clientsPage") || ((!this.state.siteLoad.homeFirstLoad && !this.state.siteLoad.windowResized) && this.state.siteLoad.pageLoaded === "homePage")) {
                setTimeout(() => {
                    const activeEl = document.querySelector(this.state.carouselPosition[this.state.siteLoad.currentProject].projectActiveEl);
                    let activeElLeftPos = activeEl.getBoundingClientRect().left;
                    const halfElWidth = activeEl.getBoundingClientRect().width / 2;
                    const windowWidthHalf = window.innerWidth / 2;
                    activeElLeftPos = (activeElLeftPos - windowWidthHalf) + halfElWidth;
                    const activeElLeftPosRound = Math.round(activeElLeftPos);
                    this.setState(immer_1.default(draftState => {
                        draftState.carouselPosition[this.state.siteLoad.currentProject].previousPosition = activeElLeftPosRound;
                    }));
                    const nextPosition = this.state.carouselPosition[this.state.siteLoad.currentProject].previousPosition + activeElLeftPosRound;
                    this.setState(immer_1.default(draftState => {
                        draftState.carouselPosition[this.state.siteLoad.currentProject].nextPosition = nextPosition;
                    }));
                    if (this.state.carouselPosition[this.state.siteLoad.currentProject].previousPosition > nextPosition) {
                        const tempArray = [...this.state[`${projectId}`]];
                        tempArray.unshift(tempArray.pop());
                        this.setState(immer_1.default(draftState => {
                            draftState[`${projectId}`] = tempArray;
                        }));
                        this.setState(immer_1.default(this.state, draftState => {
                            draftState[`${projectId}`].map(carouselItems => {
                                carouselItems.goBackwards = false;
                            });
                            draftState[`${projectId}`][0].goBackwards = true;
                        }));
                    }
                    if (this.state.carouselPosition[this.state.siteLoad.currentProject].previousPosition < nextPosition) {
                        this.setState(immer_1.default(this.state, draftState => {
                            draftState[`${projectId}`][0].addAnimation = true;
                        }));
                        setTimeout(() => {
                            this.setState(immer_1.default(this.state, draftState => {
                                draftState[`${projectId}`].map(carouselItems => {
                                    carouselItems.addAnimation = false;
                                });
                            }));
                        }, 2000);
                        setTimeout(() => {
                            const tempArray = [...this.state[`${projectId}`]];
                            tempArray.push(tempArray.shift());
                            this.setState(immer_1.default(draftState => {
                                draftState[`${projectId}`] = tempArray;
                            }));
                        }, 1700);
                    }
                }, 100);
            }
            else if (this.state.siteLoad.pageLoaded === 'homePage' && this.state.siteLoad.homeFirstLoad) {
                setTimeout(() => {
                    const activeEl = document.querySelector(this.state.carouselPosition[5].projectActiveEl);
                    this.state.handleCarouselImageLoad(activeEl);
                }, 1250);
            }
            else if (this.state.siteLoad.pageLoaded === 'clientsPage' && this.state.siteLoad.clientsFirstLoad) {
                this.state.handleProjectId('previousClientsSites');
                const activeEl = document.querySelector(this.state.carouselPosition[6].projectActiveEl);
                this.state.handleCarouselImageLoad(activeEl);
            }
            else if (this.state.siteLoad.windowResized) {
                setTimeout(() => {
                    for (let projectId = 0; projectId < this.state.carouselPosition.length; projectId++) {
                        const activeEl = document.querySelector(this.state.carouselPosition[projectId].projectActiveEl);
                        let activeElLeftPos = activeEl.getBoundingClientRect().left;
                        const windowWidthHalf = window.innerWidth / 2;
                        const difference = windowWidthHalf - this.state.carouselPosition[projectId].windowWidthHalf;
                        if (this.state.carouselPosition[projectId].windowWidthHalf < windowWidthHalf) {
                            activeElLeftPos = this.state.carouselPosition[projectId].activeElLeftPosRound - difference;
                        }
                        else {
                            activeElLeftPos = this.state.carouselPosition[projectId].activeElLeftPosRound + difference;
                        }
                        const activeElLeftPosRound = Math.round(activeElLeftPos);
                        this.setState(immer_1.default(draftState => {
                            draftState.carouselPosition[projectId].nextPosition = activeElLeftPosRound;
                        }));
                    }
                    for (let projectId = 0; projectId < this.state.carouselPosition.length; projectId++) {
                        document.documentElement.style.setProperty('--carousel-right-' + projectId, this.state.carouselPosition[projectId].nextPosition);
                    }
                    this.setState(immer_1.default(this.state, draftState => {
                        draftState.siteLoad.windowResized = false;
                    }));
                }, 1250);
            }
        }.bind(inParentComponent),
        handleCarouselImageLoad: function (activeEl) {
            const activeElLeftPos = activeEl.getBoundingClientRect().left;
            if (activeElLeftPos === 0) {
                const refreshingContent = window.setInterval(function (activeElLeftPos) {
                    activeElLeftPos = activeEl.getBoundingClientRect().left;
                    if (activeElLeftPos !== 0 || activeElLeftPos > 0) {
                        window.clearInterval(refreshingContent);
                        setTimeout(() => {
                            this.state.calculateCarouselPos();
                        }, 1250);
                    }
                }.bind(inParentComponent), 1250);
            }
            else {
                this.state.calculateCarouselPos();
            }
        }.bind(inParentComponent),
        calculateCarouselPos: function () {
            let forLoopStart = 0;
            let forLoopEnd = this.state.carouselPosition.length;
            if (this.state.siteLoad.pageLoaded === 'homePage') {
                forLoopEnd = this.state.carouselPosition.length - 1;
            }
            else if (this.state.siteLoad.pageLoaded === 'clientsPage') {
                forLoopStart = this.state.carouselPosition.length - 1;
            }
            for (let imageId = forLoopStart; imageId < forLoopEnd; imageId++) {
                const activeEl = document.querySelector(this.state.carouselPosition[imageId].projectActiveEl);
                const activeElWidth = activeEl.offsetWidth;
                let activeElLeftPos = activeEl.getBoundingClientRect().left;
                const halfElWidth = activeEl.getBoundingClientRect().width / 2;
                const windowWidthHalf = window.innerWidth / 2;
                activeElLeftPos = (activeElLeftPos - windowWidthHalf) + halfElWidth;
                const activeElLeftPosRound = Math.round(activeElLeftPos);
                const activeDiv = document.getElementById('carousel');
                this.setState(immer_1.default(this.state, draftState => {
                    draftState.carouselPosition[imageId].activeDivPosition = activeDiv.scrollLeft,
                        draftState.carouselPosition[imageId].activeElLeftPosRound = activeElLeftPosRound,
                        draftState.carouselPosition[imageId].activeElLeftPosToString = activeElLeftPosRound.toString(),
                        draftState.carouselPosition[imageId].firstLoad = false,
                        draftState.carouselPosition[imageId].activeElWidth = activeElWidth,
                        draftState.carouselPosition[imageId].previousPosition = activeElLeftPosRound,
                        draftState.carouselPosition[imageId].windowWidthHalf = windowWidthHalf;
                }));
                document.documentElement.style.setProperty('--carousel-right-' + imageId, this.state.carouselPosition[imageId].activeElLeftPosToString);
            }
        }.bind(inParentComponent),
        handleCarouselPos: function (projectId, projectIndex) {
            const currentWindowWidth = window.innerWidth;
            if (currentWindowWidth > 600) {
                if (projectId === 'homePage') {
                    if (this.state.siteLoad.homeFirstLoad) {
                        this.state.recalculateCarouselPosition();
                        this.setState(immer_1.default(this.state, draftState => {
                            draftState.siteLoad.homeFirstLoad = false;
                        }));
                    }
                }
                else if (projectId === 'clientsPage') {
                    if (this.state.siteLoad.clientsFirstLoad) {
                        this.state.recalculateCarouselPosition();
                        this.setState(immer_1.default(this.state, draftState => {
                            draftState.siteLoad.clientsFirstLoad = false;
                        }));
                    }
                }
                else if (projectId === 'resized') {
                    if (!this.state.siteLoad.windowResized) {
                        this.setState(immer_1.default(this.state, draftState => {
                            draftState.siteLoad.windowResized = true;
                        }));
                        this.state.recalculateCarouselPosition();
                    }
                }
                else {
                    this.state.recalculateCarouselPosition(projectId, projectIndex);
                }
            }
        }.bind(inParentComponent),
        handleProjectId: function (projectId) {
            switch (projectId) {
                case 'projectHandmade':
                    this.setState(immer_1.default(draftState => {
                        draftState.siteLoad.currentProject = 0;
                    }));
                    break;
                case 'projectBertrand':
                    this.setState(immer_1.default(draftState => {
                        draftState.siteLoad.currentProject = 1;
                    }));
                    break;
                case 'projectCityIdentity':
                    this.setState(immer_1.default(draftState => {
                        draftState.siteLoad.currentProject = 2;
                    }));
                    break;
                case 'projectEPub':
                    this.setState(immer_1.default(draftState => {
                        draftState.siteLoad.currentProject = 3;
                    }));
                    break;
                case 'projectCorporateWebsite':
                    this.setState(immer_1.default(draftState => {
                        draftState.siteLoad.currentProject = 4;
                    }));
                    break;
                case 'projectSmallBusinessLogo':
                    this.setState(immer_1.default(draftState => {
                        draftState.siteLoad.currentProject = 5;
                    }));
                    break;
                case 'previousClientsSites':
                    this.setState(immer_1.default(draftState => {
                        draftState.siteLoad.currentProject = 6;
                    }));
                    break;
            }
        }.bind(inParentComponent),
        handleCarouselPosition: function (loadedPage) {
            if (loadedPage === '/') {
                loadedPage = 'homePage';
            }
            else if (loadedPage === '/Clients') {
                loadedPage = 'clientsPage';
            }
            setTimeout(() => {
                this.setState(immer_1.default(this.state, draftState => {
                    draftState.siteLoad.pageLoaded = loadedPage;
                }));
                this.state.handleCarouselPos(loadedPage);
            }, 1250);
        }.bind(inParentComponent),
        handleCarouselDimensions: function () {
            document.documentElement.style.setProperty('--carousel-height', this.state.carouselHeight);
        }.bind(inParentComponent),
        slideDimensions: function () {
            var _a;
            const image = (_a = document.getElementById('Handmade')) === null || _a === void 0 ? void 0 : _a.getElementsByTagName("a")[0].getElementsByTagName("img")[0];
            const height = image === null || image === void 0 ? void 0 : image.offsetHeight;
            if (height === 0 || height === undefined) {
                const refreshContent = setInterval(function (height) {
                    var _a;
                    const image = (_a = document.getElementById('Handmade')) === null || _a === void 0 ? void 0 : _a.getElementsByTagName("a")[0].getElementsByTagName("img")[0];
                    height = image === null || image === void 0 ? void 0 : image.offsetHeight;
                    if (height !== undefined && height !== 0) {
                        clearInterval(refreshContent);
                        this.state.checkHeight(height);
                    }
                }.bind(inParentComponent), 1250);
            }
            else {
                this.state.checkHeight(height);
            }
        }.bind(inParentComponent),
        checkHeight: function (height) {
            this.setState(immer_1.default(this.state.introSlider, draftState => {
                draftState.introSlider = {
                    introHeight: height + 'px',
                    introTop: '-' + height + 'px',
                    temporaryImage: false
                };
            }));
            const slideHeight = height.toString();
            document.documentElement.style.setProperty('--slide-height', slideHeight);
        }.bind(inParentComponent),
        handleChange: function (event) {
            if (event.currentTarget.name === "messageSubject") {
                this.setState({ messageSubject: event.currentTarget.value });
            }
            else if (event.currentTarget.name === "messageFrom") {
                this.setState({ messageFrom: event.currentTarget.value });
            }
            else if (event.currentTarget.name === "messageBody") {
                this.setState({ messageBody: event.currentTarget.value });
            }
        }.bind(inParentComponent),
        calcScreenDimensions: function (eventType) {
            if (eventType === 'resized') {
                this.setState(immer_1.default(this.state, draftState => {
                    draftState.previousClients.hidden = true;
                }));
                this.state.handleScreenWidth();
                this.setState(immer_1.default(this.state, draftState => {
                    draftState.previousClients.hidden = false;
                }));
            }
            else {
                this.state.handleScreenWidth();
            }
        }.bind(inParentComponent),
        handleScreenWidth: function () {
            const currentWindowWidth = window.innerWidth;
            const currentWindowWidthToString = currentWindowWidth.toString();
            document.documentElement.style.setProperty('--screen-width', currentWindowWidthToString);
        }.bind(inParentComponent),
        handleBaseUrl: function () {
            switch (window.location.host) {
                case 'http:':
                    this.setState({ baseUrl: "http://jenniferersoz.com" });
                    break;
                case 'https:':
                    this.setState({ baseUrl: "https://jenniferersoz.com" });
                    break;
                case 'localhost:9000':
                    this.setState({ baseUrl: "https://localhost:9000" });
                    break;
                default:
                    this.setState({ baseUrl: "https://jenniferersoz.com" });
                    break;
            }
        }.bind(inParentComponent),
        sendMessage: function (event) {
            return __awaiter(this, void 0, void 0, function* () {
                event.preventDefault();
                const smtpWorker = new SMTP.Worker();
                yield smtpWorker.sendMessage(this.state.messageTo, this.state.messageFrom, this.state.messageSubject, this.state.messageBody);
            });
        }.bind(inParentComponent),
        handleEmblaImages: function (project, windowWidth) {
            for (let imageId = 0; imageId < [...this.state[`${project}`]].length; imageId++) {
                let imageHeight = [...this.state[`${project}`]][imageId].height;
                let imageDimensionRatio = 100 / [...this.state[`${project}`]][imageId].width;
                imageDimensionRatio = imageDimensionRatio * window.innerWidth;
                imageDimensionRatio = imageDimensionRatio / 100;
                imageHeight = imageHeight * imageDimensionRatio;
                setTimeout(() => {
                    this.setState(immer_1.default(this.state, draftState => {
                        draftState[`${project}`][imageId].width = windowWidth;
                        draftState[`${project}`][imageId].height = imageHeight;
                    }));
                }, 1250);
            }
        }.bind(inParentComponent),
        handleEmbla: function () {
            const windowWidth = window.innerWidth;
            if (windowWidth < 600) {
                console.log('test');
                this.state.handleEmblaImages('projectHandmade', windowWidth);
                this.state.handleEmblaImages('projectBertrand', windowWidth);
                this.state.handleEmblaImages('projectCityIdentity', windowWidth);
                this.state.handleEmblaImages('projectEPub', windowWidth);
                this.state.handleEmblaImages('projectCorporateWebsite', windowWidth);
                this.state.handleEmblaImages('projectSmallBusinessLogo', windowWidth);
                this.state.handleEmblaImages('previousClientsSites', windowWidth);
            }
        }.bind(inParentComponent)
    };
}
exports.createState = createState;


/***/ }),

/***/ "./src/fonts/woff/Raleway-bold-webfont.woff":
/*!**************************************************!*\
  !*** ./src/fonts/woff/Raleway-bold-webfont.woff ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "fonts/Raleway-bold-webfont.woff");

/***/ }),

/***/ "./src/fonts/woff/raleway-regular-webfont.woff":
/*!*****************************************************!*\
  !*** ./src/fonts/woff/raleway-regular-webfont.woff ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "fonts/raleway-regular-webfont.woff");

/***/ }),

/***/ "./src/fonts/woff2/raleway-bold-webfont.woff2":
/*!****************************************************!*\
  !*** ./src/fonts/woff2/raleway-bold-webfont.woff2 ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "fonts/raleway-bold-webfont.woff2");

/***/ }),

/***/ "./src/fonts/woff2/raleway-regular-webfont.woff2":
/*!*******************************************************!*\
  !*** ./src/fonts/woff2/raleway-regular-webfont.woff2 ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "fonts/raleway-regular-webfont.woff2");

/***/ }),

/***/ "./src/img/blog/gbk.jpg":
/*!******************************!*\
  !*** ./src/img/blog/gbk.jpg ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/blog/gbk1dc1.jpg");

/***/ }),

/***/ "./src/img/blog/michael-craig-martin.jpg":
/*!***********************************************!*\
  !*** ./src/img/blog/michael-craig-martin.jpg ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/blog/michael-craig-martine17e.jpg");

/***/ }),

/***/ "./src/img/blog/my-logo-redesigned-day-1.png":
/*!***************************************************!*\
  !*** ./src/img/blog/my-logo-redesigned-day-1.png ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/blog/my-logo-redesigned-day-10c1d.png");

/***/ }),

/***/ "./src/img/blog/safe_image.jpg":
/*!*************************************!*\
  !*** ./src/img/blog/safe_image.jpg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/blog/safe_image86e3.jpg");

/***/ }),

/***/ "./src/img/clients/account-technologies.jpg":
/*!**************************************************!*\
  !*** ./src/img/clients/account-technologies.jpg ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/account-technologies780b.jpg");

/***/ }),

/***/ "./src/img/clients/arts-business-initiatives.jpg":
/*!*******************************************************!*\
  !*** ./src/img/clients/arts-business-initiatives.jpg ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/arts-business-initiatives3cad.jpg");

/***/ }),

/***/ "./src/img/clients/ascot-study.jpg":
/*!*****************************************!*\
  !*** ./src/img/clients/ascot-study.jpg ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/ascot-studybe30.jpg");

/***/ }),

/***/ "./src/img/clients/astro-infused-water.jpg":
/*!*************************************************!*\
  !*** ./src/img/clients/astro-infused-water.jpg ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/astro-infused-waterb6ac.jpg");

/***/ }),

/***/ "./src/img/clients/dimitris-papanastasiou.jpg":
/*!****************************************************!*\
  !*** ./src/img/clients/dimitris-papanastasiou.jpg ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/dimitris-papanastasiou2041.jpg");

/***/ }),

/***/ "./src/img/clients/dogs-and-cats-painted.jpg":
/*!***************************************************!*\
  !*** ./src/img/clients/dogs-and-cats-painted.jpg ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/dogs-and-cats-painted0815.jpg");

/***/ }),

/***/ "./src/img/clients/feather-flair.jpg":
/*!*******************************************!*\
  !*** ./src/img/clients/feather-flair.jpg ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/feather-flair92e4.jpg");

/***/ }),

/***/ "./src/img/clients/foundation-for-circulatory-health.jpg":
/*!***************************************************************!*\
  !*** ./src/img/clients/foundation-for-circulatory-health.jpg ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/foundation-for-circulatory-health42d5.jpg");

/***/ }),

/***/ "./src/img/clients/furthur-progressions.jpg":
/*!**************************************************!*\
  !*** ./src/img/clients/furthur-progressions.jpg ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/furthur-progressions2efd.jpg");

/***/ }),

/***/ "./src/img/clients/health-n-wellbeing.jpg":
/*!************************************************!*\
  !*** ./src/img/clients/health-n-wellbeing.jpg ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/health-n-wellbeing4958.jpg");

/***/ }),

/***/ "./src/img/clients/home-grown-textiles.jpg":
/*!*************************************************!*\
  !*** ./src/img/clients/home-grown-textiles.jpg ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/home-grown-textiles1635.jpg");

/***/ }),

/***/ "./src/img/clients/parsley-and-thyme.jpg":
/*!***********************************************!*\
  !*** ./src/img/clients/parsley-and-thyme.jpg ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/parsley-and-thyme9797.jpg");

/***/ }),

/***/ "./src/img/clients/profile-image-square.jpg":
/*!**************************************************!*\
  !*** ./src/img/clients/profile-image-square.jpg ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/profile-image-squaredf96.jpg");

/***/ }),

/***/ "./src/img/clients/red-wine-villas.jpg":
/*!*********************************************!*\
  !*** ./src/img/clients/red-wine-villas.jpg ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/red-wine-villas20bb.jpg");

/***/ }),

/***/ "./src/img/clients/southern-belle-constructions.jpg":
/*!**********************************************************!*\
  !*** ./src/img/clients/southern-belle-constructions.jpg ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/southern-belle-constructions5e56.jpg");

/***/ }),

/***/ "./src/img/clients/tappily.jpg":
/*!*************************************!*\
  !*** ./src/img/clients/tappily.jpg ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/tappily975e.jpg");

/***/ }),

/***/ "./src/img/clients/the-bridge-inn-amberley.jpg":
/*!*****************************************************!*\
  !*** ./src/img/clients/the-bridge-inn-amberley.jpg ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/the-bridge-inn-amberley29c5.jpg");

/***/ }),

/***/ "./src/img/clients/vapoureze.jpg":
/*!***************************************!*\
  !*** ./src/img/clients/vapoureze.jpg ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/clients/vapourezea52c.jpg");

/***/ }),

/***/ "./src/img/favicon/android-chrome-144x144.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/android-chrome-144x144.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-144x1442e5a.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-192x192.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/android-chrome-192x192.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-192x1923dba.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-256x256.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/android-chrome-256x256.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-256x256ca13.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-36x36.png":
/*!**************************************************!*\
  !*** ./src/img/favicon/android-chrome-36x36.png ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-36x36f3cb.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-384x384.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/android-chrome-384x384.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-384x3843c68.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-48x48.png":
/*!**************************************************!*\
  !*** ./src/img/favicon/android-chrome-48x48.png ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-48x48dabb.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-512x512.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/android-chrome-512x512.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-512x512bc81.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-72x72.png":
/*!**************************************************!*\
  !*** ./src/img/favicon/android-chrome-72x72.png ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-72x728291.png");

/***/ }),

/***/ "./src/img/favicon/android-chrome-96x96.png":
/*!**************************************************!*\
  !*** ./src/img/favicon/android-chrome-96x96.png ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/android-chrome-96x96bafe.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-114x114.png":
/*!******************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-114x114.png ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-114x11433e1.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-120x120.png":
/*!******************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-120x120.png ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-120x120c540.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-144x144.png":
/*!******************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-144x144.png ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-144x1445408.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-152x152.png":
/*!******************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-152x152.png ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-152x152a658.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-180x180.png":
/*!******************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-180x180.png ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-180x180276c.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-57x57.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-57x57.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-57x5720c9.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-60x60.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-60x60.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-60x60dcce.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-72x72.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-72x72.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-72x721b68.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon-76x76.png":
/*!****************************************************!*\
  !*** ./src/img/favicon/apple-touch-icon-76x76.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon-76x7683d2.png");

/***/ }),

/***/ "./src/img/favicon/apple-touch-icon.png":
/*!**********************************************!*\
  !*** ./src/img/favicon/apple-touch-icon.png ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/apple-touch-icon276c.png");

/***/ }),

/***/ "./src/img/favicon/favicon-16x16.png":
/*!*******************************************!*\
  !*** ./src/img/favicon/favicon-16x16.png ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/favicon-16x16cff0.png");

/***/ }),

/***/ "./src/img/favicon/favicon-194x194.png":
/*!*********************************************!*\
  !*** ./src/img/favicon/favicon-194x194.png ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/favicon-194x194922e.png");

/***/ }),

/***/ "./src/img/favicon/favicon-32x32.png":
/*!*******************************************!*\
  !*** ./src/img/favicon/favicon-32x32.png ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/favicon-32x32b9dd.png");

/***/ }),

/***/ "./src/img/favicon/favicon.ico":
/*!*************************************!*\
  !*** ./src/img/favicon/favicon.ico ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/faviconc6d1.ico");

/***/ }),

/***/ "./src/img/favicon/mstile-144x144.png":
/*!********************************************!*\
  !*** ./src/img/favicon/mstile-144x144.png ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/mstile-144x144e677.png");

/***/ }),

/***/ "./src/img/favicon/mstile-150x150.png":
/*!********************************************!*\
  !*** ./src/img/favicon/mstile-150x150.png ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/mstile-150x150571b.png");

/***/ }),

/***/ "./src/img/favicon/mstile-310x150.png":
/*!********************************************!*\
  !*** ./src/img/favicon/mstile-310x150.png ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/mstile-310x150662b.png");

/***/ }),

/***/ "./src/img/favicon/mstile-310x310.png":
/*!********************************************!*\
  !*** ./src/img/favicon/mstile-310x310.png ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/mstile-310x3103fa3.png");

/***/ }),

/***/ "./src/img/favicon/mstile-70x70.png":
/*!******************************************!*\
  !*** ./src/img/favicon/mstile-70x70.png ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/mstile-70x700045.png");

/***/ }),

/***/ "./src/img/favicon/safari-pinned-tab.svg":
/*!***********************************************!*\
  !*** ./src/img/favicon/safari-pinned-tab.svg ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/favicon/safari-pinned-tab2f8a.svg");

/***/ }),

/***/ "./src/img/shillington/SPARK3ipad-layouts.png":
/*!****************************************************!*\
  !*** ./src/img/shillington/SPARK3ipad-layouts.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/SPARK3ipad-layouts2d58.png");

/***/ }),

/***/ "./src/img/shillington/bertrand-project-back-cover.jpg":
/*!*************************************************************!*\
  !*** ./src/img/shillington/bertrand-project-back-cover.jpg ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/bertrand-project-back-coverfc6d.jpg");

/***/ }),

/***/ "./src/img/shillington/bertrand-project-front-cover.jpg":
/*!**************************************************************!*\
  !*** ./src/img/shillington/bertrand-project-front-cover.jpg ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/bertrand-project-front-cover01ae.jpg");

/***/ }),

/***/ "./src/img/shillington/bertrand-project-pages-2-3.jpg":
/*!************************************************************!*\
  !*** ./src/img/shillington/bertrand-project-pages-2-3.jpg ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/bertrand-project-pages-2-36af4.jpg");

/***/ }),

/***/ "./src/img/shillington/bertrand-project-pages-4-5.jpg":
/*!************************************************************!*\
  !*** ./src/img/shillington/bertrand-project-pages-4-5.jpg ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/bertrand-project-pages-4-5e8e7.jpg");

/***/ }),

/***/ "./src/img/shillington/bertrand-project-pages-6-7.jpg":
/*!************************************************************!*\
  !*** ./src/img/shillington/bertrand-project-pages-6-7.jpg ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/bertrand-project-pages-6-7bc25.jpg");

/***/ }),

/***/ "./src/img/shillington/city-identity-booking-page.jpg":
/*!************************************************************!*\
  !*** ./src/img/shillington/city-identity-booking-page.jpg ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/city-identity-booking-pagecdd3.jpg");

/***/ }),

/***/ "./src/img/shillington/city-identity-contact-page.jpg":
/*!************************************************************!*\
  !*** ./src/img/shillington/city-identity-contact-page.jpg ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/city-identity-contact-pagefce7.jpg");

/***/ }),

/***/ "./src/img/shillington/city-identity-homepage.jpg":
/*!********************************************************!*\
  !*** ./src/img/shillington/city-identity-homepage.jpg ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/city-identity-homepage91db.jpg");

/***/ }),

/***/ "./src/img/shillington/city-identity-project-filters.jpg":
/*!***************************************************************!*\
  !*** ./src/img/shillington/city-identity-project-filters.jpg ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/city-identity-project-filters1edb.jpg");

/***/ }),

/***/ "./src/img/shillington/corporate-calculation-page.png":
/*!************************************************************!*\
  !*** ./src/img/shillington/corporate-calculation-page.png ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/corporate-calculation-page904f.png");

/***/ }),

/***/ "./src/img/shillington/corporate-website-homepage.png":
/*!************************************************************!*\
  !*** ./src/img/shillington/corporate-website-homepage.png ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/corporate-website-homepage82f4.png");

/***/ }),

/***/ "./src/img/shillington/corporate-website-previous-savings.png":
/*!********************************************************************!*\
  !*** ./src/img/shillington/corporate-website-previous-savings.png ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/corporate-website-previous-savings0238.png");

/***/ }),

/***/ "./src/img/shillington/corporate-website-savings-page.png":
/*!****************************************************************!*\
  !*** ./src/img/shillington/corporate-website-savings-page.png ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/corporate-website-savings-page67d8.png");

/***/ }),

/***/ "./src/img/shillington/epub-magazine-feature.jpg":
/*!*******************************************************!*\
  !*** ./src/img/shillington/epub-magazine-feature.jpg ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/epub-magazine-featurefcc8.jpg");

/***/ }),

/***/ "./src/img/shillington/epub-magazine-homepage.jpg":
/*!********************************************************!*\
  !*** ./src/img/shillington/epub-magazine-homepage.jpg ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/epub-magazine-homepage6dec.jpg");

/***/ }),

/***/ "./src/img/shillington/epub-magazine-introduction.jpg":
/*!************************************************************!*\
  !*** ./src/img/shillington/epub-magazine-introduction.jpg ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/epub-magazine-introductionb7a0.jpg");

/***/ }),

/***/ "./src/img/shillington/half-epub.jpg":
/*!*******************************************!*\
  !*** ./src/img/shillington/half-epub.jpg ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/half-epub8385.jpg");

/***/ }),

/***/ "./src/img/shillington/hamburger.svg":
/*!*******************************************!*\
  !*** ./src/img/shillington/hamburger.svg ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/hamburgerffd3.svg");

/***/ }),

/***/ "./src/img/shillington/handmade-book-cover-back.jpg":
/*!**********************************************************!*\
  !*** ./src/img/shillington/handmade-book-cover-back.jpg ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/handmade-book-cover-back2c99.jpg");

/***/ }),

/***/ "./src/img/shillington/handmade-book-cover-front-back.jpg":
/*!****************************************************************!*\
  !*** ./src/img/shillington/handmade-book-cover-front-back.jpg ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/handmade-book-cover-front-back4867.jpg");

/***/ }),

/***/ "./src/img/shillington/handmade-book-cover-front.jpg":
/*!***********************************************************!*\
  !*** ./src/img/shillington/handmade-book-cover-front.jpg ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/handmade-book-cover-front067d.jpg");

/***/ }),

/***/ "./src/img/shillington/handmade-project-slide-mobile.png":
/*!***************************************************************!*\
  !*** ./src/img/shillington/handmade-project-slide-mobile.png ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/handmade-project-slide-mobiled6d9.png");

/***/ }),

/***/ "./src/img/shillington/handmade-project-slide.jpg":
/*!********************************************************!*\
  !*** ./src/img/shillington/handmade-project-slide.jpg ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/handmade-project-slidec653.jpg");

/***/ }),

/***/ "./src/img/shillington/jennifer-ersoz-jones.png":
/*!******************************************************!*\
  !*** ./src/img/shillington/jennifer-ersoz-jones.png ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/jennifer-ersoz-jones3cba.png");

/***/ }),

/***/ "./src/img/shillington/quarter-size-ipad-epub.png":
/*!********************************************************!*\
  !*** ./src/img/shillington/quarter-size-ipad-epub.png ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/quarter-size-ipad-epub30ca.png");

/***/ }),

/***/ "./src/img/shillington/small-business-logo-login-entered.jpg":
/*!*******************************************************************!*\
  !*** ./src/img/shillington/small-business-logo-login-entered.jpg ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/small-business-logo-login-entered5b04.jpg");

/***/ }),

/***/ "./src/img/shillington/small-business-logo-login-error.jpg":
/*!*****************************************************************!*\
  !*** ./src/img/shillington/small-business-logo-login-error.jpg ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/small-business-logo-login-error4bcc.jpg");

/***/ }),

/***/ "./src/img/shillington/small-business-logo-login.jpg":
/*!***********************************************************!*\
  !*** ./src/img/shillington/small-business-logo-login.jpg ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/small-business-logo-loginc388.jpg");

/***/ }),

/***/ "./src/img/shillington/small-business-logo-tooltips.jpg":
/*!**************************************************************!*\
  !*** ./src/img/shillington/small-business-logo-tooltips.jpg ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/small-business-logo-tooltips07bd.jpg");

/***/ }),

/***/ "./src/img/shillington/spark-2ipad-layouts.png":
/*!*****************************************************!*\
  !*** ./src/img/shillington/spark-2ipad-layouts.png ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/spark-2ipad-layouts0837.png");

/***/ }),

/***/ "./src/img/shillington/spark4ipad-layouts.png":
/*!****************************************************!*\
  !*** ./src/img/shillington/spark4ipad-layouts.png ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/spark4ipad-layouts6ed5.png");

/***/ }),

/***/ "./src/img/shillington/sparkipad-layouts.png":
/*!***************************************************!*\
  !*** ./src/img/shillington/sparkipad-layouts.png ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/shillington/sparkipad-layouts8328.png");

/***/ }),

/***/ "./src/img/social/Twitter_Social_Icon_Circle_Color.png":
/*!*************************************************************!*\
  !*** ./src/img/social/Twitter_Social_Icon_Circle_Color.png ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/social/Twitter_Social_Icon_Circle_Colorbaed.png");

/***/ }),

/***/ "./src/img/social/f_logo_RGB-Blue_100.png":
/*!************************************************!*\
  !*** ./src/img/social/f_logo_RGB-Blue_100.png ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/social/f_logo_RGB-Blue_100db47.png");

/***/ }),

/***/ "./src/img/social/linkedin.png":
/*!*************************************!*\
  !*** ./src/img/social/linkedin.png ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (__webpack_require__.p + "images/social/linkedin206a.png");

/***/ }),

/***/ "./src/scss/appStyles.scss":
/*!*********************************!*\
  !*** ./src/scss/appStyles.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./src/code/main.tsx","runtime~main","npm.mui~._node_modules_@mui_base_A","npm.mui~._node_modules_@mui_base_S","npm.mui~._node_modules_@mui_base_i","npm.mui~._node_modules_@mui_material_B","npm.mui~._node_modules_@mui_material_N","npm.mui~._node_modules_@mui_p","npm.mui~._node_modules_@mui_s","npm.material-ui~._node_modules_@material-ui_core_esm_B","npm.material-ui~._node_modules_@material-ui_s","npm.axios~._node_modules_axios_i","npm.emotion~._node_modules_@emotion_c","npm.react-transition-group~._node_modules_react-transition-group_esm_C","npm.react-form-validator-core~._node_modules_react-form-validator-core_lib_V","npm.prop-types~._node_modules_prop-types_c","npm.react~._node_modules_react_c","npm.react-dom~._node_modules_react-dom_c","npm.scheduler~._node_modules_scheduler_c","npm.css-vendor~._node_modules_css-vendor_dist_css-vendor.esm.js~78f38ba0","npm.embla-carousel~._node_modules_embla-carousel_react.js~234be603","npm.immer~._node_modules_immer_dist_immer.esm.js~adfae5a5","npm.jss~._node_modules_jss_dist_jss.esm.js~fdc78cd9","npm.react-location~._node_modules_react-location_dist_index.js~c050a888","main~._node_modules_@"]]]);
//# sourceMappingURL=main~._src_c.f6d5.chunk.js.map