const commonConfig = require('./webpack.config.common');
const { merge } = require('webpack-merge');
const path = require('path');

module.exports = merge(commonConfig, {
    mode: 'production',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, "../dist/"),
        allowedHosts: [
            'jenniferersoz.com',
            'http://www.jenniferersoz.com',
            'https://www.jenniferersoz.com'
          ], 
        host: '0.0.0.0',
        port: 9000,
        https: true,
        hot: true, 
        inline: false, 
        disableHostCheck: true,
        historyApiFallback: true,
        writeToDisk: true, 
        compress: true,
        watchOptions: {
			poll: true  
		}
    }

});