const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const ImageminWebpWebpackPlugin = require("imagemin-webp-webpack-plugin");
const TerserPlugin = require('terser-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CompressionPlugin = require("compression-webpack-plugin");
const htmlWebpackInjectAttributesPlugin = require("html-webpack-inject-attributes-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = {
  entry: {
    main: "./src/code/main.tsx",
  },
  output: {
    path: path.join(__dirname, "dist"),
    chunkFilename: 'js/[name].[hash:4].chunk.js',
    filename: 'js/[name].[hash:4].js'
  },
  externals: {
    //'immer': 'immer'
  },
  resolve: { extensions: [ ".ts", ".tsx", ".js" ] },
  plugins: [
    new CleanWebpackPlugin({
        // verbose: true,
        // dry: true,
        // cleanStaleWebpackAssets: true,
        // cleanOnceBeforeBuildPatterns: [path.join(__dirname, '../dist/*')]
    }),
    new HtmlWebpackPlugin({ 
        template: "./src/index.html", 
        path: path.join(__dirname, "../dist/"),
        filename: "index.html",
        favicon: "./src/img/favicon/favicon.ico",
        minify: {
            collapseWhitespace: true,
            minifyCSS: true
        },
        inject: true,
        attributes: {
            'as': function (tag) { 
                if(tag.attributes.src){
                    var newTag = tag.attributes.src;
                    var tagExt = newTag.substr((newTag.lastIndexOf('.') + 1));
                    if(tagExt === 'js'){
                        return 'script';
                    }
                    return tagExt;
                }else if(tag.attributes.href){
                    var newTag = tag.attributes.href;
                    var tagExt = newTag.substr((newTag.lastIndexOf('.') + 1));
                    if(tagExt === 'css'){
                        return 'style';
                    }else if(tagExt === 'woff' || tagExt === 'woff2'){
                        return 'font';
                    }
                    return tagExt;
                }
            },
            'media': function (tag, compilation, index) {
                if (tag.attributes.href) {
                    var newTag = tag.attributes.href;
                    var tagExt = newTag.substr((newTag.lastIndexOf('.') + 1));
                    if(tagExt === 'css'){
                        return 'screen';
                    }
                    return tagExt;
                }
            },
            'rel': function (tag, compilation, index) {
                if (tag.attributes.href) {
                    var newTag = tag.attributes.href;
                    var tagExt = newTag.substr((newTag.lastIndexOf('.') + 1));
                    if(tagExt === 'css'){
                        return 'stylesheet preload';
                    }
                    return tagExt;
                }else if(tag.attributes.src){
                    var newTag = tag.attributes.src;
                    var tagExt = newTag.substr((newTag.lastIndexOf('.') + 1));
                    if(tagExt === 'js'){
                        return 'preconnect';
                    } 
                    return tagExt;
                }
            }
        },
     }),
     new htmlWebpackInjectAttributesPlugin({
         async: true,
         defer: true
     }),
     new MiniCssExtractPlugin({
        filename: 'css/[name].[hash:4].css',
        chunkFilename: 'css/[name].[hash:4].css'
      }),
      new PreloadWebpackPlugin({
        rel: 'preload',
        as: 'font', 
        include: 'allAssets',
        fileWhitelist: [/\.(woff2?|eot|ttf|otf)(\?.*)?$/i],
    }),
    new OptimizeCSSAssetsPlugin({}),
      new ImageminWebpWebpackPlugin({
        config: [{
            test: /\.(jpe?g|png)$/,
            options: { quality: 60 }
        }],
        overrideExtension: true,
        silent: false
    }),
    new CopyPlugin({
       patterns: [ 
            { from: "./src/seo/",  to: "../dist/"}
        ]},
        { copyUnmodified: true, force: true } 
    ),
    new CompressionPlugin({
        filename: "js/[name][fragment][ext].gz",
        algorithm: "gzip",
        test: /\.js$/,
        minRatio: 0.8,
        compressionOptions: { level: 4 }
    }),

  ],
  optimization: {
    minimize: false,
    minimizer: [
        new TerserPlugin({
            // Use multi-process parallel running to improve the build speed
            // Default number of concurrent runs: os.cpus().length - 1
            parallel: true,
            // Enable file caching
            cache: true,
            sourceMap: true,
            terserOptions: {
                format: {
                  comments: false,
                },
              },
              extractComments: false,
        }),
        new CssMinimizerPlugin({
            test: /\.s?[ac]ss$/, 
        }),  
    ],
    runtimeChunk: 'multiple',
        splitChunks: {
            maxInitialRequests: Infinity,
            minSize: 10000,
            maxSize: 249856,
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module){
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

                        return `npm.${packageName.replace('@', '')}`;
                    },
                    chunks: 'all'
                }
            }
        }
  },
  module: {
    rules: [
        {
            test: /\.(ico|png|jp(e*)g|svg|gif)$/,
            use: [
                { 
                    loader: "file-loader",
                    options: { 
                        name: 'images/[folder]/[name][hash:4].[ext]'
                    }
                }
            ]
        },
        { test: /\.html$/, use: {loader: "html-loader" } },
        { 
            test: /\.s?[ac]ss$/, 
            use: [

                { loader: MiniCssExtractPlugin.loader, options: { hmr: process.env.NODE_ENV === 'development'}},
                { loader: 'css-loader', options: { url: false, sourceMap: true } },
                { loader: 'postcss-loader', options: { plugins: function() { return [ require('autoprefixer')];} }},
                { loader: 'sass-loader', options: { sourceMap: true}}
            ] 
        },
        {   test: /\.tsx?$/, 
            loader: 'awesome-typescript-loader'
        },
        {
            test: /\.(woff|woff2|eot|ttf|otf)$/,
            use: [{
                loader: "file-loader",
                options: {
                    name: 'fonts/[name].[ext]'
                }
            }]
        }
    ]
  },
}