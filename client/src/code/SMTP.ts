import axios from "axios";
import { config } from "./config";

export class Worker {
    public async sendMessage(
        inTo: string,
        inFrom: string,
        inSubject: string,
        inMessage: string
    ): Promise<void> {
        
        await axios.post(`${config.serverAddress}/messages`, {
            to: inTo,
            from: inFrom,
            subject: inSubject,
            text: inMessage
        }).then(response => response.data)
        .then(response => { 
            console.log('Success:', response);
            document.documentElement.style.setProperty(
                '--display-form', 'none'
            );
            document.documentElement.style.setProperty(
                '--display-success', 'block'
            );
        })
        .catch(err => {
            console.log('Error: ', err);
            document.documentElement.style.setProperty(
                '--display-form', 'none'
            );
            document.documentElement.style.setProperty(
                '--display-error', 'block'
            );
        });
    }
}