import * as SMTP from "./SMTP";
import produce from "immer";

interface IMenu { link: string, title: string, active: boolean}
interface IIntroCarousel { link: string, title: string, class: string, active: boolean, alt: string, image: string, webp: string, width: number, height: number, mobileImage?: string, mobileWebp: string, mobileWidth: number, mobileHeight: number }
interface IProjectImages { image: string, webp: string, width: number, height: number, alt: string, active?: boolean, key?: string, link: string, wayBackLink?: string, goBackwards: boolean, addAnimation: boolean, projectId: string }
interface IClientImages { image: string, webp: string, width: number, height: number, alt: string, active?: boolean, key?: string, link?: string, hasLink: boolean, linkActive: boolean, wayBackLink?: string, hasWayBackLink: boolean, goBackwards: boolean, addAnimation: boolean, projectId: string }
interface IProjectsIntro { title: string, content: string }
interface IBlogPosts {title: string, content: string, webp: string, image: string, width: number, height: number, date: string, alt: string }
interface ICarouselPosition { title: string, index: number, activeDivPosition: number, activeElLeftPosToString: string, activeElWidth: number, previousPosition: number, firstLoad: boolean, scrollTo: number, nextPosition: number, projectActiveEl: string, windowWidthHalf: number }
interface ISocialIcons { title: string, image: string, webp: string, width: number, height: number, link: string, alt: string }

export function createState(inParentComponent: React.Component): any{
    return {
        hamburgerImage: {
            src: <string>`./images/shillington/hamburger9525.svg`,
            height: <number>37,
            width: <number>47,
            alt: <string>`hamburger icon for mobile menu`
        },
        carouselHeight: <string>"440",
        state: <boolean>false,
        siteLoad: {
            homeFirstLoad: <boolean>true,
            clientsFirstLoad: <boolean>true,
            windowResized: <boolean>false,
            pageLoaded: <string>"",
            currentProject: <number>0
        },
        checkIsMounted: {
            isMounted: <boolean>false,
            isUnmounted: <boolean>false
        },
        mobileMenu: {
            anchorEl: null,
            open: <boolean>false
        },
        introSlider: {
            introHeight: <string>"",
            introTop: <string>"",
            temporaryImage: <boolean>true
        },
        hasError: <boolean>false, 
        logoImages: {
            image: <string>`./images/shillington/jennifer-ersoz-jones3cba.jpg`,
            webp: <string>`./images/shillington/jennifer-ersoz-jones3cba.webp`,
            width: <number>846,
            height: <number>261
        },
        previousClients: {
            hidden: <boolean>false,
            heading: <string>`Designs`,
            title: <string>`Previous Client Websites:`,
            content: <string>`<p>Here I display a variety of websites that I have designed and built for the clients Mr Site. Taking note of customer requirements in their consultations I was able to provide customers with suitable solutions for their business needs.</p><p>In addition to these sites I include website screenshots of my own clients websites: Foundation for Circulatory Health, Ascot Study, Astro Infused and Jaylouv Studios. Further I have included screenshots of two websites I have built however not designed for Account Technologies.</p><p>I have included links to these websites, however some are no longer live or now look different to what they were as they were created some time ago. A Wayback Machine link has been included to provide physical representation of these removed sites or what the site used to look like when I had finished designing and or building.</p>`
        },
        profile: {
            heading: <string>`Profile`,
            title: <string>`Jennifer Ersoz-Jones`,
            content: <string>`<p>My first client website stemmed from my BSc degree in Information Systems and Computing. One of our projects involved the creation of a website for a non-profit organisation. This organisation was the Foundation for Circulatory Health. Originally this project started off as a redesign of a one page website. The site has now grown to 40+ pages.</p><p>Following this I continued working as a freelance Web Designer and additionally pursued a Web Design career at Mr Site. From my time at Mr Site I have created 100+ websites for the company's customers. Since leaving Mr Site I went on to work as an in house Web and Email Designer and Developer. At present I am working as a Freelance Web Designer and Developer. Within this role I have worked on varied websites providing support and tutorials where possible.</p><p>My portfolio website was originally built to showcase the design skills I had learnt from taking an intensive Graphic Design course at Shillington College during 2015. The last assignment of the course was to create a website to promote the work I had completed whilst on the course. However due to time constraints of the project I decided to build the website using a popular content managment system. Some time later an opportunity aroze during April 2020 where I was put onto furlough amid the Coronavirus pandemic. Making the most of the ample time available I decided to upskill again and pushed myself forward to learn the latest fullstack development technologies. Whereby I learnt React, TypeScript, webpack, GitLab and various supporting npm packages such as Google's Material-UI and Immer. From learning these new technologies I decided to apply this new tech and rebuild my portfolio website from scratch.</p>`,
            image: <string>`./images/clients/profile-image-squaredf96.jpg`,
            webp: <string>`./images/clients/profile-image-squaredf96.webp`,
            alt: <string>`profile photo infront of red brick wall`,
            imageWidth: <number>844,
            imageHeight: <number>843
        },
        salesImages: {
            image: <string>`./images/shillington/half-epub8383.jpg`,
            webp: <string>`./images/shillington/half-epub8385.webp`,
            width: <number>819,
            height: <number>300,
            mobileImage: <string>`./images/shillington/quarter-size-ipad-epub30ca.png`,
            mobileWebP: <string>`./images/shillington/quarter-size-ipad-epub30ca.webp`,
            mobileWidth: 410,
            mobileHeight: 300,
            alt: `artwork on ipad`
        },
        sales: <IProjectsIntro[]> [
            {
                title: `<h1>New year, New <strong>Website</strong>?</h1>`,
                content: `<h2>Whether you are looking to refresh the appearance of your current website or wanting a fantastic new site to start the New Year, I have a range of solutions to suit your website needs...</h2><h3><strong>Prices</strong></h3><ul><li>Whole sites <b>&pound;250</b></li><li>Includes: 5 page website, 1 domain, SEO, and 5 page mobile website (Extra pages optional, <b>&pound;60</b> each)</li><li>Individual pages <b>&pound;60</b> each</li><li>Includes: 1 webpage, 1 mobile page and SEO</li></ul><p>N.b. Hosting not included in pricing structure</p>`
            },
            {
                title: `<h2>Your website build:</h2>`,
                content: `<p>The Process for the build of a website is in several steps for completion:</p><ol class="nested-counter-list"><li>Consultation & payment<ol><li>A consultation will take place between the web designer and the customer regarding their website. Upon approval of the plan for the website a payment of 50% of the project will be made before the start of work.</li></ol></li><li>Website Assets<ol><li>The web designer received all the website materials from the customer before starting work on the website.</li></ol></li><li>First build<ol><li>The first design of the project will happen at this stage and a hidden view of this first design of the website is sent to the customer for checking and feedback before continued development of website.</li></ol></li><li>Iteration 1<ol><li>In response to the first design feedback changes are completed by the web designer and then the website is sent again to the customer for further checking and feedback. This customer feedback is to be supplied to the web designer for further development.</li></ol></li><li>Iteration 2<ol><li>Final feedback from the customer is received by the web designer in response to iteration 1. This feedback of changes is then completed by the web designer to complete the project. The website is then complete. The final 50% of payment is then made to the designer.</li></ol></li></ol>`
            },
            {
                title: `<h2>Terms & Conditions:</h2>`,
                content: `<ol class="nested-counter-list"><li>Websites<ol><li>Websites will be completed within 4 weeks</li><li>Website materials including: Photos, Text, and Videos need to be provided before website build</li></ol></li><li>Products<ol><li>Product assets must be supplied before the start of a project (including: titles of products, description of products, product variations, product images and product prices).</li><li>Only 30 products are to be uploaded by the web designer per page of the website.</li></ol></li><li>Refunds<ol><li>Full refunds shall only be given if work has not been completed. Partial refunds will be given depending upon the stage of completion in the project.</li></ol></li><li>Videos<ol><li>Videos should be hosted on youtube or vimeo by the customer before being supplied to the website designer. If this is not so an extra fee will be applied so that the videos are hosted on either youtube or vimeo before insertion into website.</li></ol></li><li>Payment<ol><li>A sum of 50% of the website fee will be taken at the start of the project as a deposit towards the work. The final 50% agreed payment amount is to be paid by the customer upon completion of the project.</li></ol></li><li>Tutorials<ol><li>At the completion of the project tutorials can be requested. Tutorials will be provided in 30 minute allocations and they can take place via Skype or Phone. The price of a tutorial is £20</li></ol></li><li>Maintenance<ol><li>Maintenance work of websites can be completed for customers at a rate of £10 per hour.</li></ol></li></ol>`
            },
            {
                title: `<h2>Guidelines:</h2>`,
                content: `<ol class="nested-counter-list"><li>Images<ol><li>Images should be supplied in the format of either PDF, PSD, PNG, AI, WebP, JPEG, JPEG2000 or JPG.</li><li>Preference of images to be supplied at the highest resolution possible i.e. 300 dpi</li><li>Images for products should ideally be taken on a solid colour background without any obstructive element taking the focus of the individual product.</li><li>To keep within legal requirements images should have copyright permission. If images are taken from google they should hold the right for reuse. When looking for suitable images in google under the images tab of the google search engine select Usage rights > Labeled for reuse. This will allow you to pick images that are suitable for your website.</li></ol></li></ol>`
            }
        ],
        blogPosts: <IBlogPosts[]> [
            {
                image: `./images/blog/gbk1dc1.jpg`,
                webp: `./images/blog/gbk1dc1.webp`,
                width: 900,
                height: 1601,
                alt: `Gourmet Burger Kitchen - Incoming: Burger Joy`,
                title: `Clever word play.`, 
                content: ``,
                date: `22 January 2016`
            },
            {
                image: `./images/blog/safe_image86e3.jpg`,
                webp: `./images/blog/safe_image86e3.webp`,
                width: 470,
                height: 245,
                alt: `Aspects of web design`,
                title: `Web Trends 2016!`, 
                content: `<p>Great design trends of 2016! Minimalism, Pixellation, Video Backgrounds, Statement Typography and Grid Free...</p><a target="_blank" rel="noopener" class="clientsLink" href="https://creativemarket.com/blog/2016/01/04/top-web-design-trends-for-2016">https://creativemarket.com/blog/2016/01/04/top-web-design-trends-for-2016</a>`,
                date: `21 January 2016`
            },
            {
                image: `./images/blog/michael-craig-martine17e.jpg`,
                webp: `./images/blog/michael-craig-martine17e.webp`,
                width: 2044,
                height: 3778,
                alt: `Outline of a lightbulb in metal stood in a park`,
                title: `Craig-Martin in the park`, 
                content: `<p>Michael Craig Martin (MCM) at the Serpentine Gallery, Kensington Gardens. It was great to see the work of MCM today. A designer whose work I haven&rsquo;t encountered since leaving Secondary School. Large canvases are displayed covered with illustrations of everyday electrical and household objects painted in vibrant colours. MCM's style is closely linked to that of Patrick Caulfield. Don't miss out, the exhibition ends on Valentines Day!</p>`,
                date: `18 January 2016`
            },
            {
                image: `./images/blog/my-logo-redesigned-day-10c1d.png`,
                webp: `./images/blog/my-logo-redesigned-day-10c1d.webp`,
                width: 960,
                height: 560,
                title: `My first blog post!`,
                alt: `redesign of Jennifer Ersoz-Jones logo`, 
                content: `<p>To welcome my new blog I thought I'd kick off with a brand new feature to the site. This new feature is 'A logo day'. 'A logo day' posts are featured to display an individual logo that I have created based on a particular theme.</p><p>As a starting point I display my own logo redesigned to the keywords of medium, constant and helpful. These keywords depict how I visualise myself.</p><p>The logos that I create are just for fun and help to push my creative ability.</p>`,
                date: `14 January 2016`
            }
        ],
        previousClientsSites: <IClientImages[]> [
            { 
                image: './images/clients/account-technologies780b.jpg', 
                webp: './images/clients/account-technologies780b.webp', 
                width: 1366,
                height: 768,
                alt: 'account technologies website',
                active: false,
                key: 'account-technologies',
                link: 'http://www.accounttechnologies.com/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20180602220734/https://www.accounttechnologies.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/arts-business-initiatives3cad.jpg', 
                webp: './images/clients/arts-business-initiatives3cad.webp', 
                width: 997,
                height: 608,
                alt: 'arts business initiatives website',
                active: false,
                key: 'arts-business-initiatives',
                link: 'http://www.artsbusinessinitiatives.com/',
                hasLink: true,
                linkActive: false,
                wayBackLink: 'https://web.archive.org/web/20150505034545/http://www.artsbusinessinitiatives.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/ascot-studybe30.jpg', 
                webp: './images/clients/ascot-studybe30.webp', 
                width: 1121,
                height: 719,
                alt: 'ascot study website',
                active: false,
                key: 'ascot-study',
                link: 'http://ascotstudy.org.uk',
                hasLink: true,
                linkActive: true,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/astro-infused-waterb6ac.jpg', 
                webp: './images/clients/astro-infused-waterb6ac.webp',
                width: 1120,
                height: 718, 
                alt: 'astro infused water website',
                active: false,
                key: 'astro-infused-water',
                link: 'http://astroinfused.com/',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/dimitris-papanastasiou2041.jpg', 
                webp: './images/clients/dimitris-papanastasiou2041.webp', 
                width: 995,
                height: 628,
                alt: 'dimitris papanastasiou website',
                active: false,
                key: 'dimitris-papanastasiou-website',
                link: '#',
                hasLink: false,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/dogs-and-cats-painted0815.jpg', 
                webp: './images/clients/dogs-and-cats-painted0815.webp', 
                width: 995,
                height: 627,
                alt: 'dogs and cats painted website',
                active: false,
                key: 'dogs-and-cats-painted-website',
                link: 'http://www.dogsandcatspainted.com/home',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/feather-flair92e4.jpg', 
                webp: './images/clients/feather-flair92e4.webp',
                width: 1120,
                height: 695, 
                alt: 'feather flair website',
                active: false,
                key: 'feather-flair-website',
                link: 'http://www.featherflair.co.uk/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20160311005518/http://www.featherflair.co.uk/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/foundation-for-circulatory-health42d5.jpg', 
                webp: './images/clients/foundation-for-circulatory-health42d5.webp', 
                width: 910,
                height: 696,
                alt: 'foundation for circulatory health website',
                active: false,
                key: 'foundation-for-circulatory-health-website',
                link: 'http://www.ffch.org/',
                hasLink: true,
                linkActive: true,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/furthur-progressions2efd.jpg', 
                webp: './images/clients/furthur-progressions2efd.webp',
                width: 994,
                height: 628, 
                alt: 'furthur progressions website',
                active: false,
                key: 'furthur-progressions-website',
                link: 'https://www.furthurprogressions.com/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20150801164224/https://www.furthurprogressions.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/health-n-wellbeing4958.jpg', 
                webp: './images/clients/health-n-wellbeing4958.webp', 
                width: 1137,
                height: 868,
                alt: 'health n wellbeing website',
                active: false,
                key: 'health-n-wellbeing-website',
                link: '#',
                hasLink: false,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/home-grown-textiles1635.jpg', 
                webp: './images/clients/home-grown-textiles1635.webp',
                width: 1401,
                height: 868, 
                alt: 'home grown textiles website',
                active: false,
                key: 'home-grown-textiles-website',
                link: 'http://www.homegrowntextiles.co.uk/',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/parsley-and-thyme9797.jpg', 
                webp: './images/clients/parsley-and-thyme9797.webp', 
                width: 960,
                height: 628,
                alt: 'parsley and thyme website',
                active: true,
                key: 'parsley-and-thyme-website',
                link: 'https://www.parsleyandthymecatering.co.uk/',
                hasLink: true,
                linkActive: true,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/red-wine-villas20bb.jpg', 
                webp: './images/clients/red-wine-villas20bb.webp', 
                width: 1138,
                height: 683,
                alt: 'red wine villas website',
                active: false,
                key: 'red-wine-villas-website',
                link: 'http://www.redwinevillas.net/',
                hasLink: true,
                linkActive: false,
                wayBackLink: 'https://web.archive.org/web/20190727223749/http://redwinevillas.net/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/southern-belle-constructions5e56.jpg', 
                webp: './images/clients/southern-belle-constructions5e56.webp', 
                width: 1421,
                height: 879,
                alt: 'southern belle constructions website',
                active: false,
                key: 'southern-belle-construstions-website',
                link: 'https://sb-c.com.au/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20160314062311/http://sb-c.com.au/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/tappily975e.jpg', 
                webp: './images/clients/tappily975e.webp', 
                width: 1368,
                height: 772,
                alt: 'tappily',
                active: false,
                key: 'tappily-website',
                link: 'https://www.tappily.co.uk/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20181118225513/https://www.tappily.co.uk/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/the-bridge-inn-amberley29c5.jpg', 
                webp: './images/clients/the-bridge-inn-amberley29c5.webp', 
                width: 995,
                height: 606,
                alt: 'the bridge inn amberley website',
                active: false,
                key: 'the-bridge-inn-amberley-website',
                link: 'https://www.bridgeinnamberley.com/',
                hasLink: true,
                linkActive: true,
                wayBackLink: 'https://web.archive.org/web/20160131035045/http://bridgeinnamberley.com/',
                hasWayBackLink: true,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            },
            { 
                image: './images/clients/vapourezea52c.jpg', 
                webp: './images/clients/vapourezea52c.webp', 
                width: 1401,
                height: 898,
                alt: 'vapoureze website',
                active: false,
                key: 'vapoureze-website',
                link: 'http://vapoureze.eu',
                hasLink: true,
                linkActive: false,
                wayBackLink: '',
                hasWayBackLink: false,
                goBackwards: false,
                addAnimation: false,
                projectId: 'previousClientsSites'
            }
        ],
        error: null,
        errorInfo: null,
        messageSubject: <string>"",
        messageFrom: <string>"",
        messageBody: <string>"",
        activeElement: <number>0,
        mobileBreakpoint: <number>600,
        messageTo: <string>"jennifer.ersoz@gmail.com",
        baseUrl: <string>'',
        portfolioTitle: <string>`Portfolio`,
        portfolioContent: <string>`<span class="startEmphasis">Hello</span> and welcome to my website. On this website you will find my portfolio below which is made up of varied projects I undertook whilst studying a graphic design course at Shillington College Summer 2015. In addition to the portfolio I also have examples of completed client websites. These can be found on the Clients page of this website.`,
        carouselPosition: <ICarouselPosition[]>[
            {
                title: 'projectHandmade',
                index: 0,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectHandmade-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectBertrand',
                index: 1,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectBertrand-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectCityIdentity',
                index: 2,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectCityIdentity-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectEPub',
                index: 3,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectEPub-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectCorporateWebsite',
                index: 4,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectCorporateWebsite-active',
                windowWidthHalf: 0
            },
            {
                title: 'projectSmallBusinessLogo',
                index: 5,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.projectSmallBusinessLogo-active',
                windowWidthHalf: 0
            },
            {
                title: 'previousClientsSites',
                index: 6,
                activeDivPosition: 0,
                activeElLeftPosToString: "",
                previousPosition: 0,
                firstLoad: true,
                scrollTo: 0,
                activeElWidth: 0,
                nextPosition: 0,
                projectActiveEl: 'a.previousClientsSites-active',
                windowWidthHalf: 0
            }           
        ],
        menu:<IMenu[]> [
            {
                link: '/',
                title: 'Portfolio',
                active: true
            },
            {
                link: '/Clients',
                title: 'Clients & About',
                active: false
            },
            {
                link: '/Sales',
                title: 'Need a site?',
                active: false
            }, 
            {
                link: '/Blog',
                title: 'Blog',
                active: false
            }
        ],
        introCarousel:<IIntroCarousel[]> [
            {
                link: '#Handmade',
                title: 'Handmade',
                class: 'firstanimation',
                alt: 'front and back bookcover of birds nest',
                image: './images/shillington/handmade-project-slidec653.jpg',
                webp: `./images/shillington/handmade-project-slidec653.webp`,
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/handmade-project-slide-mobiled6d9.png',
                mobileWebp: './images/shillington/handmade-project-slide-mobiled6d9.webp', 
                mobileWidth: 1500,
                mobileHeight: 704
            },
            {
                link: '#CityIdentity',
                title: 'City_Identity',
                class: 'secondanimation',
                alt: 'mobile view of tourism website',
                image: './images/shillington/city-identity-project-slide912d.jpg',
                webp: './images/shillington/city-identity-project-slide912d.webp', 
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/city-identity-project-slide-mobiled7af.png',
                mobileWebp: './images/shillington/city-identity-project-slide-mobiled7af.webp',
                mobileWidth: 1500,
                mobileHeight: 681
            },
            {
                link: '#EpubMagazine',
                title: 'Epub_Magazine',
                class: 'thirdanimation',
                alt: 'epub view of cycling magazine article',
                image: './images/shillington/epub-magazine-project-slidef7fc.jpg',
                webp: './images/shillington/epub-magazine-project-slidef7fc.webp',
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/epub-magazine-project-slide-mobilea44d.png',
                mobileWebp: './images/shillington/epub-magazine-project-slide-mobilea44d.webp',
                mobileWidth: 1500,
                mobileHeight: 704
            },
            {
                link: '#SmallBusinessLogo',
                title: 'Small_Business_Logo',
                class: 'fourthanimation',
                alt: 'postcards advertising online courses',
                image: './images/shillington/small-business-logo-slidef73c.jpg',
                webp: './images/shillington/small-business-logo-slidef73c.webp',
                width: 2000,
                height: 704,
                mobileImage: './images/shillington/small-business-logo-slide-mobileecc5.jpg',
                mobileWebp: './images/shillington/small-business-logo-slide-mobileecc5.webp',
                mobileWidth: 1500,
                mobileHeight: 704
            }
        ],
        projectHandmade:<IProjectImages[]> [
            { 
                image: './images/shillington/handmade-book-cover-front067d.jpg', 
                webp: './images/shillington/handmade-book-cover-front067d.webp', 
                width: 1037,
                height: 1147,
                alt: 'front cover of handmade design on book',
                active: false,
                key: 'handmade-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false, 
                projectId: 'projectHandmade'
            },
            { 
                image: './images/shillington/handmade-book-cover-front-back4867.jpg', 
                webp: './images/shillington/handmade-book-cover-front-back4867.webp',
                width: 1024,
                height: 768, 
                alt: 'front and back cover of handmade design on book',
                active: false,
                key: 'handmade-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            { 
                image: './images/shillington/handmade-book-cover-back2c99.jpg', 
                webp: './images/shillington/handmade-book-cover-back2c99.webp', 
                width: 1296,
                height: 1434,
                alt: 'back cover of handmade design on book',
                active: false,
                key: 'handmade-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            { 
                image: './images/shillington/handmade-book-cover-front067d.jpg', 
                webp: './images/shillington/handmade-book-cover-front067d.webp', 
                width: 1037,
                height: 1147,
                alt: 'front cover of handmade design on book',
                active: true,
                key: 'handmade-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            { 
                image: './images/shillington/handmade-book-cover-front-back4867.jpg', 
                webp: './images/shillington/handmade-book-cover-front-back4867.webp',
                width: 1024,
                height: 768, 
                alt: 'front and back cover of handmade design on book',
                active: false,
                key: 'handmade-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            },
            { 
                image: './images/shillington/handmade-book-cover-back2c99.jpg', 
                webp: './images/shillington/handmade-book-cover-back2c99.webp', 
                width: 1296,
                height: 1434,
                alt: 'back cover of handmade design on book',
                active: false,
                key: 'handmade-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectHandmade'
            }
        ],
        projectBertrand:<IProjectImages[]> [
            { 
                image: './images/shillington/bertrand-project-front-cover01ae.jpg', 
                webp: './images/shillington/bertrand-project-front-cover01ae.webp', 
                width: 2048,
                height: 1536,
                alt: 'front cover design of leaflet',
                active: false,
                key: 'bertrand-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            { 
                image: './images/shillington/bertrand-project-pages-2-36af4.jpg', 
                webp: './images/shillington/bertrand-project-pages-2-36af4.webp', 
                width: 1024,
                height: 768,
                alt: 'pages 2 and 3 of leaflet',
                active: false,
                key: 'bertrand-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            { 
                image: './images/shillington/bertrand-project-pages-4-5e8e7.jpg', 
                webp: './images/shillington/bertrand-project-pages-4-5e8e7.webp', 
                width: 1024,
                height: 768,
                alt: 'pages 4 and 5 of leaflet',
                active: true,
                key: 'bertrand-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            { 
                image: './images/shillington/bertrand-project-pages-6-7bc25.jpg', 
                webp: './images/shillington/bertrand-project-pages-6-7bc25.webp', 
                width: 1024,
                height: 768,
                alt: 'pages 6 and 7 of leaflet',
                active: false,
                key: 'bertrand-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            },
            { 
                image: `./images/shillington/bertrand-project-back-coverfc6d.jpg`, 
                webp: './images/shillington/bertrand-project-back-coverfc6d.webp', 
                width: 1024,
                height: 768,
                alt: 'back cover design of leaflet',
                active: false,
                key: 'bertrand-original-5',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectBertrand'
            }         
        ],
        projectCityIdentity:<IProjectImages[]> [
            { 
                image: './images/shillington/city-identity-homepage91db.jpg', 
                webp: './images/shillington/city-identity-homepage91db.webp', 
                width: 353,
                height: 500,
                alt: 'hompage of city identity website',
                active: false,
                key: 'city-identity-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            { 
                image: './images/shillington/city-identity-project-filters1edb.jpg', 
                webp: './images/shillington/city-identity-project-filters1edb.webp', 
                width: 353,
                height: 500,
                alt: 'filters of city identity website',
                active: false,
                key: 'city-identity-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'  
            },
            { 
                image: './images/shillington/city-identity-booking-pagecdd3.jpg', 
                webp: './images/shillington/city-identity-booking-pagecdd3.webp',
                width: 353,
                height: 500, 
                alt: 'booking page of city identity website',
                active: false,
                key: 'city-identity-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            { 
                image: './images/shillington/city-identity-contact-pagefce7.jpg', 
                webp: './images/shillington/city-identity-contact-pagefce7.webp', 
                width: 353,
                height: 500,
                alt: 'contact page of city identity website',
                active: false,
                key: 'city-identity-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            { 
                image: './images/shillington/city-identity-homepage91db.jpg', 
                webp: './images/shillington/city-identity-homepage91db.webp', 
                idth: 353,
                height: 500,
                alt: 'hompage of city identity website',
                active: true,
                key: 'city-identity-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            },
            { 
                image: './images/shillington/city-identity-project-filters1edb.jpg', 
                webp: './images/shillington/city-identity-project-filters1edb.webp', 
                width: 353,
                height: 500,
                alt: 'filters of city identity website',
                active: false,
                key: 'city-identity-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'  
            },
            { 
                image: './images/shillington/city-identity-booking-pagecdd3.jpg', 
                webp: './images/shillington/city-identity-booking-pagecdd3.webp', 
                width: 353,
                height: 500,
                alt: 'booking page of city identity website',
                active: false,
                key: 'city-identity-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity' 
            },
            { 
                image: './images/shillington/city-identity-contact-pagefce7.jpg', 
                webp: './images/shillington/city-identity-contact-pagefce7.webp', 
                width: 353,
                height: 500,
                alt: 'contact page of city identity website',
                active: false,
                key: 'city-identity-duplicate-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCityIdentity'
            }          
        ],
        projectEPub:<IProjectImages[]> [
            { 
                image: './images/shillington/epub-magazine-homepage6dec.jpg', 
                webp: './images/shillington/epub-magazine-homepage6dec.webp',
                width: 819,
                height: 601, 
                alt: 'homepage magazine cover',
                active: false,
                key: 'epub-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            { 
                image: './images/shillington/epub-magazine-introductionb7a0.jpg', 
                webp: './images/shillington/epub-magazine-introductionb7a0.webp',
                width: 600,
                height: 440, 
                alt: 'introduction page of magazine cover',
                active: false,
                key: 'epub-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub' 
            },
            { 
                image: './images/shillington/epub-magazine-featurefcc8.jpg', 
                webp: './images/shillington/epub-magazine-featurefcc8.webp', 
                width: 819,
                height: 601,
                alt: 'feature page of magazine cover',
                active: false,
                key: 'epub-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            { 
                image: './images/shillington/epub-magazine-homepage6dec.jpg', 
                webp: './images/shillington/epub-magazine-homepage6dec.webp', 
                width: 819,
                height: 601,
                alt: 'homepage magazine cover',
                active: true,
                key: 'epub-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            },
            { 
                image: './images/shillington/epub-magazine-introductionb7a0.jpg', 
                webp: './images/shillington/epub-magazine-introductionb7a0.webp', 
                width: 819,
                height: 601,
                alt: 'introduction page of magazine cover',
                active: false,
                key: 'epub-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub' 
            },
            { 
                image: './images/shillington/epub-magazine-featurefcc8.jpg', 
                webp: './images/shillington/epub-magazine-featurefcc8.webp', 
                width: 819,
                height: 601,
                alt: 'feature page of magazine cover',
                active: false,
                key: 'epub-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectEPub'
            }           
        ],
        projectCorporateWebsite:<IProjectImages[]> [
            { 
                image: './images/shillington/corporate-website-homepage82f4.png', 
                webp: './images/shillington/corporate-website-homepage82f4.webp',
                width: 800,
                height: 600, 
                alt: 'hompage of corporate website',
                active: false,
                key: 'corporate-website-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            { 
                image: './images/shillington/corporate-calculation-page904f.png', 
                webp: './images/shillington/corporate-calculation-page904f.webp', 
                width: 800,
                height: 600,
                alt: 'calculation page of corporate website',
                active: false,
                key: 'corporate-website-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            { 
                image: './images/shillington/corporate-website-savings-page67d8.png', 
                webp: './images/shillington/corporate-website-savings-page67d8.webp', 
                width: 800,
                height: 600,
                alt: 'savings page of corporate website',
                active: false,
                key: 'corporate-website-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            { 
                image: './images/shillington/corporate-website-previous-savings0238.png', 
                webp: './images/shillington/corporate-website-previous-savings0238.webp', 
                width: 800,
                height: 600,
                alt: 'previous savings page of corporate website',
                active: false,
                key: 'corporate-website-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            { 
                image: './images/shillington/corporate-website-homepage82f4.png', 
                webp: './images/shillington/corporate-website-homepage82f4.webp', 
                width: 800,
                height: 600,
                alt: 'hompage of corporate website',
                active: true,
                key: 'corporate-website-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            { 
                image: './images/shillington/corporate-calculation-page904f.png', 
                webp: './images/shillington/corporate-calculation-page904f.webp', 
                width: 800,
                height: 600,
                alt: 'calculation page of corporate website',
                active: false,
                key: 'corporate-website-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            { 
                image: './images/shillington/corporate-website-savings-page67d8.png', 
                webp: './images/shillington/corporate-website-savings-page67d8.webp', 
                width: 800,
                height: 600,
                alt: 'savings page of corporate website',
                active: false,
                key: 'corporate-website-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            },
            { 
                image: './images/shillington/corporate-website-previous-savings0238.png', 
                webp: './images/shillington/corporate-website-previous-savings0238.webp',
                width: 800,
                height: 600, 
                alt: 'previous savings page of corporate website',
                active: false,
                key: 'corporate-website-duplicate-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectCorporateWebsite'
            }
        ],
        projectSmallBusinessLogo:<IProjectImages[]> [
            { 
                image: './images/shillington/small-business-logo-loginc388.jpg', 
                webp: './images/shillington/small-business-logo-loginc388.webp', 
                width: 1024,
                height: 751,
                alt: 'login webpage',
                active: false,
                key: 'small-business-logo-original-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            { 
                image: './images/shillington/small-business-logo-tooltips07bd.jpg', 
                webp: './images/shillington/small-business-logo-tooltips07bd.webp',
                width: 1024,
                height: 751, 
                alt: 'tooltips display on login fields of webpage',
                active: false,
                key: 'small-business-logo-original-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            { 
                image: './images/shillington/small-business-logo-login-entered5b04.jpg', 
                webp: './images/shillington/small-business-logo-login-entered5b04.webp', 
                width: 1024,
                height: 751,
                alt: 'login entered on login page',
                active: false,
                key: 'small-business-logo-original-3',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo' 
            },
            { 
                image: './images/shillington/small-business-logo-login-error4bcc.jpg', 
                webp: './images/shillington/small-business-logo-login-error4bcc.webp',
                width: 1024,
                height: 751, 
                alt: 'fields with errors on login page',
                active: false,
                key: 'small-business-logo-original-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            { 
                image: './images/shillington/small-business-logo-loginc388.jpg', 
                webp: './images/shillington/small-business-logo-loginc388.webp', 
                width: 1024,
                height: 751,
                alt: 'login webpage',
                active: true,
                key: 'small-business-logo-duplicate-1',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            { 
                image: './images/shillington/small-business-logo-tooltips07bd.jpg', 
                webp: './images/shillington/small-business-logo-tooltips07bd.webp', 
                width: 1024,
                height: 751,
                alt: 'tooltips display on login fields of webpage',
                active: false,
                key: 'small-business-logo-duplicate-2',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            },
            { 
                image: './images/shillington/small-business-logo-login-entered5b04.jpg', 
                webp: './images/shillington/small-business-logo-login-entered5b04.webp',
                width: 1024,
                height: 751, 
                alt: 'login entered on login page',
                active: false,
                key: 'small-business-logo-duplicate-3',
                link: '#',
                goBackwards: false,
                addAnimation: false ,
                projectId: 'projectSmallBusinessLogo'
            },
            { 
                image: './images/shillington/small-business-logo-login-error4bcc.jpg', 
                webp: './images/shillington/small-business-logo-login-error4bcc.webp', 
                width: 1024,
                height: 751,
                alt: 'fields with errors on login page',
                active: false,
                key: 'small-business-logo-duplicate-4',
                link: '#',
                goBackwards: false,
                addAnimation: false,
                projectId: 'projectSmallBusinessLogo'
            }
        ],
        socialIcons: <ISocialIcons[]>[
            {
                title: 'Twitter',
                image: './images/social/Twitter_Social_Icon_Circle_Colorbaed.png',
                webp: './images/social/Twitter_Social_Icon_Circle_Colorbaed.webp',
                width: 400,
                height: 400,
                link: 'https://twitter.com/JenniferErsoz', 
                alt: 'Twitter Profile'
            },
            {
                title: 'Facebook',
                image: './images/social/f_logo_RGB-Blue_1024db47.png',
                webp: './images/social/f_logo_RGB-Blue_100db47.webp',
                width: 217,
                height: 218,
                link: 'https://www.facebook.com/JenniferErsozWebAndGraphicDesigner',
                alt: 'Facebook Page'
            },
            {
                title: 'LinkedIn',
                image: './images/social/linkedin206a.png',
                webp: './images/social/linkedin206a.webp',
                width: 382,
                height: 382,
                link: 'https://www.linkedin.com/in/jennifer-ersoz-jones/',
                alt: 'LinkedIn Profile'
            }
        ],
        projectsIntro: <IProjectsIntro[]>[
            {
                title: 'Handmade Project:',
                content: `The brief of this project was to create a handmade item for a physical end product. My interpretation of this brief was the creation of a nest for a book cover entitled Birdsong by Sebastian Faulks. The nest is symbolic of the beginnings and endings of the characters' relationships within the book.`
            }, 
            {
                title: `Bertrand Project:`,
                content: `The brief of this project was to create a recruitment brochure for the Bertrand company to help inspire employees and prospective applicants to join and stay with the company throughout their careers. My response to the brief was to enhance the depiction of direction and building blocks within the brochure.`
            },
            {
                title: `City Identity Project:`,
                content: `The brief of this project was to create an identity for a small city which is currently either undergoing development or reconstruction. Fethiye was chosen for the project and as a result a logo was created based on the history of the city, the relaxing atmosphere it provides and the many experiences to discover there.`
            },
            {
                title: `Epub Magazine Project:`,
                content: `The brief of this project was to create an epub magazine intended for newbies to cycling. My interpretation of this brief stemmed from the idea of participation to form the design. An end result was created whereby pieces of the design are puzzled together along with the underlying repetitive movement from the action of cycling.`
            }, 
            {
                title: `Corporate Website Project:`,
                content: `The brief of this project was to design a website for children aged 7-10. This site is designed to help them learn how to save money. The saving of money provides security for an unknown future. The client request for this project was to create an online calculator whereby children can easily view their savings over time. My answer to this brief was the design of an interactive game which would simulate the way in which a bank would weigh money.`
            },
            {
                title: `Small Business Logo Project:`,
                content: `The purpose of this brief was to create a logo for a small online business. The online business was for a company who wanted to provide online lifestyle courses. My answer to this brief was the creation of a logo with the word spark to enforce the idea of the desire to learn. A roll out of the identity was created for the company.`
            }
        ],

        getDerivedStateFromError: function(){
            return{ hasError: true };
        }.bind(inParentComponent),

        handleMenuLink: function(i): void{
            let loadedPage = '';
            sessionStorage.setItem('activePage', i);
            if(i !== null){
                this.setState(produce(this.state, draftState => {
                    draftState.menu.map(menuLink => {
                        menuLink.active = false;
                    });
                    draftState.menu[i].active = true;
                }));
                
                if(i === 0){
                    loadedPage = 'homePage';    
                }else if(i === 1){
                    loadedPage = 'clientsPage';
                }

                setTimeout(() => {
                    this.setState(produce(this.state, draftState => {
                        draftState.siteLoad.pageLoaded =  loadedPage
                    }));
                    this.state.handleCarouselPos(loadedPage);
                }, 1250);   
            }
        }.bind(inParentComponent),

        handleReloadedPage: function(): void{
            const savedActivePage:any = sessionStorage.getItem('activePage');

            if(savedActivePage !== null){
                this.setState(produce(this.state, draftState => {
                    draftState.menu.map(menuLink => {
                        menuLink.active = false;
                    });
                    draftState.menu[savedActivePage].active = true;
                }));
            }
        }.bind(inParentComponent),

        componentDidCatch: function(error, errorInfo) {
            return {
                error: error,
                errorInfo: errorInfo
            }
        }.bind(inParentComponent),

        handleClick: function(event): void{            
            this.setState(produce(this.state.mobileMenu, draftState => {
                draftState.mobileMenu = {
                    anchorEl: event.currentTarget,
                    open: true
                }
            }))
        }.bind(inParentComponent),

        handleClose: function(): void{
            this.setState(produce(draftState => {
                draftState.mobileMenu = {
                    anchorEl: null,
                    open: false
                }
            }))
        }.bind(inParentComponent),

        handleCarouselLink: function(event, index, projectId): void{
            event.preventDefault;

            this.setState(produce(this.state, draftState => {
                draftState[`${projectId}`].map(carouselLink => {
                    carouselLink.active = false
                }); 
                draftState[`${projectId}`][index].active = true
            }));

            this.state.handleProjectId(projectId);
            
            this.state.handleCarouselPos(projectId, index);
        }.bind(inParentComponent),

        recalculateCarouselPosition: function(projectId: string){
            if(((!this.state.siteLoad.clientsFirstLoad && !this.state.siteLoad.windowResized) && this.state.siteLoad.pageLoaded === "clientsPage") || ((!this.state.siteLoad.homeFirstLoad && !this.state.siteLoad.windowResized) && this.state.siteLoad.pageLoaded === "homePage")){ 

                setTimeout(() => {

                const activeEl = <HTMLElement> document.querySelector(this.state.carouselPosition[this.state.siteLoad.currentProject].projectActiveEl);
                let activeElLeftPos = activeEl.getBoundingClientRect().left;
                const halfElWidth = activeEl.getBoundingClientRect().width / 2;
                const windowWidthHalf = window.innerWidth / 2;
                activeElLeftPos = (activeElLeftPos - windowWidthHalf) + halfElWidth;
                const activeElLeftPosRound = Math.round(activeElLeftPos);

                this.setState(produce(draftState => {
                    draftState.carouselPosition[this.state.siteLoad.currentProject].previousPosition = activeElLeftPosRound;
                }));

                const nextPosition = this.state.carouselPosition[this.state.siteLoad.currentProject].previousPosition + activeElLeftPosRound;                      

                this.setState(produce(draftState => {
                    draftState.carouselPosition[this.state.siteLoad.currentProject].nextPosition = nextPosition;
                }))

                if(this.state.carouselPosition[this.state.siteLoad.currentProject].previousPosition > nextPosition){
                    const tempArray = [...this.state[`${projectId}`]];
                    tempArray.unshift(tempArray.pop());
                    
                    this.setState(produce(draftState => {
                        draftState[`${projectId}`] = tempArray;
                    }));

                    this.setState(produce(this.state, draftState => {
                        draftState[`${projectId}`].map(carouselItems => {
                            carouselItems.goBackwards = false
                        }); 
                        draftState[`${projectId}`][0].goBackwards = true;
                    }))
                }
                if(this.state.carouselPosition[this.state.siteLoad.currentProject].previousPosition < nextPosition){
                        this.setState(produce(this.state, draftState => {
                            draftState[`${projectId}`][0].addAnimation = true;                            
                        }));

                    setTimeout(() => {
                        this.setState(produce(this.state, draftState => {
                            draftState[`${projectId}`].map(carouselItems => {
                                carouselItems.addAnimation = false
                            }); 
                        }))
                    }, 2000); 

                    setTimeout(() => {
                        const tempArray = [...this.state[`${projectId}`]];
                        tempArray.push(tempArray.shift());
                    
                        this.setState(produce(draftState => {
                            draftState[`${projectId}`] = tempArray;
                        }));
                    }, 1700);     
                }
            }, 100); 
        }
                else if(this.state.siteLoad.pageLoaded === 'homePage' && this.state.siteLoad.homeFirstLoad){
                    setTimeout(() => {
                        
                    const activeEl = <HTMLElement> document.querySelector(this.state.carouselPosition[5].projectActiveEl);
                    this.state.handleCarouselImageLoad(activeEl);  
                }, 1250);   
                }
                else if(this.state.siteLoad.pageLoaded === 'clientsPage' && this.state.siteLoad.clientsFirstLoad){
                    this.state.handleProjectId('previousClientsSites');
                    const activeEl = <HTMLElement> document.querySelector(this.state.carouselPosition[6].projectActiveEl);
                    this.state.handleCarouselImageLoad(activeEl);                    
                }
                else if(this.state.siteLoad.windowResized){

                    setTimeout(() => {
                        for(let projectId = 0; projectId < this.state.carouselPosition.length; projectId++){
                            const activeEl = <HTMLElement> document.querySelector(this.state.carouselPosition[projectId].projectActiveEl);
                            let activeElLeftPos = activeEl.getBoundingClientRect().left;
                            const windowWidthHalf = window.innerWidth / 2;
                            const difference = windowWidthHalf - this.state.carouselPosition[projectId].windowWidthHalf;
                            if(this.state.carouselPosition[projectId].windowWidthHalf < windowWidthHalf){  
                                activeElLeftPos = this.state.carouselPosition[projectId].activeElLeftPosRound - difference;
                            }else{
                                activeElLeftPos = this.state.carouselPosition[projectId].activeElLeftPosRound + difference;
                            }
                            const activeElLeftPosRound = Math.round(activeElLeftPos);

                            this.setState(produce(draftState => {
                                draftState.carouselPosition[projectId].nextPosition = activeElLeftPosRound
                            }));
                        }

                        for(let projectId = 0; projectId < this.state.carouselPosition.length; projectId++){
                            document.documentElement.style.setProperty(
                                '--carousel-right-' + projectId, this.state.carouselPosition[projectId].nextPosition
                            );
                        }

                        this.setState(produce(this.state, draftState => {
                            draftState.siteLoad.windowResized = false
                        }));
                    },1250);
                }
            }.bind(inParentComponent),

        handleCarouselImageLoad: function(activeEl): void{
            const activeElLeftPos = activeEl.getBoundingClientRect().left;
                    
            if(activeElLeftPos === 0){
                const refreshingContent = window.setInterval(function(activeElLeftPos) {
                    activeElLeftPos = activeEl.getBoundingClientRect().left;
                    if(activeElLeftPos !== 0 || activeElLeftPos > 0){
                            window.clearInterval(refreshingContent);
                        setTimeout(() => {
                            this.state.calculateCarouselPos();
                        },1250);
                    }
                }.bind(inParentComponent),1250);
            }else{
                this.state.calculateCarouselPos();
            }
        }.bind(inParentComponent),

        calculateCarouselPos: function(): void{
            let forLoopStart = 0;
            let forLoopEnd = this.state.carouselPosition.length;
            if(this.state.siteLoad.pageLoaded === 'homePage'){
                forLoopEnd = this.state.carouselPosition.length - 1;
            }else if(this.state.siteLoad.pageLoaded === 'clientsPage'){
                forLoopStart = this.state.carouselPosition.length - 1;
            }

            for(let imageId = forLoopStart; imageId < forLoopEnd; imageId++){
                const activeEl = <HTMLElement> document.querySelector(this.state.carouselPosition[imageId].projectActiveEl);
                const activeElWidth = activeEl.offsetWidth;
                let activeElLeftPos = activeEl.getBoundingClientRect().left;
                const halfElWidth = activeEl.getBoundingClientRect().width / 2;
                const windowWidthHalf = window.innerWidth / 2;
                activeElLeftPos = (activeElLeftPos - windowWidthHalf) + halfElWidth;
                const activeElLeftPosRound = Math.round(activeElLeftPos);
                const activeDiv = <HTMLElement> document.getElementById('carousel');

                this.setState(produce(this.state, draftState => {
                    draftState.carouselPosition[imageId].activeDivPosition = activeDiv.scrollLeft,
                    draftState.carouselPosition[imageId].activeElLeftPosRound = activeElLeftPosRound,
                    draftState.carouselPosition[imageId].activeElLeftPosToString = activeElLeftPosRound.toString(),
                    draftState.carouselPosition[imageId].firstLoad = false,
                    draftState.carouselPosition[imageId].activeElWidth = activeElWidth,
                    draftState.carouselPosition[imageId].previousPosition = activeElLeftPosRound,
                    draftState.carouselPosition[imageId].windowWidthHalf = windowWidthHalf
                }));

                document.documentElement.style.setProperty(
                    '--carousel-right-' + imageId, this.state.carouselPosition[imageId].activeElLeftPosToString
                );
            }
        }.bind(inParentComponent),

        handleCarouselPos: function(projectId, projectIndex): void{
            const currentWindowWidth = window.innerWidth; 

            if(currentWindowWidth > 600){
                if(projectId === 'homePage'){
                    if(this.state.siteLoad.homeFirstLoad){
                        this.state.recalculateCarouselPosition();
                        
                        this.setState(produce(this.state, draftState => {
                            draftState.siteLoad.homeFirstLoad = false
                        }));
                    }
                }else 
                if(projectId === 'clientsPage'){
                    if(this.state.siteLoad.clientsFirstLoad){                   
                            this.state.recalculateCarouselPosition();
         
                        this.setState(produce(this.state, draftState => {
                            draftState.siteLoad.clientsFirstLoad = false
                        }));
                    }
                }
                else if(projectId === 'resized'){
                    if(!this.state.siteLoad.windowResized){
                        this.setState(produce(this.state, draftState => {
                            draftState.siteLoad.windowResized = true
                        }));
    
                        this.state.recalculateCarouselPosition();
                    }    
                }else{
                    this.state.recalculateCarouselPosition(projectId, projectIndex);
                }
            }
        }.bind(inParentComponent),

        handleProjectId: function(projectId): void{
            switch(projectId){
                case 'projectHandmade':
                    this.setState(produce(draftState => {
                        draftState.siteLoad.currentProject =  0
                    }));
                    break;
                case 'projectBertrand':
                    this.setState(produce(draftState => {
                        draftState.siteLoad.currentProject =  1
                    }));
                    break;
                case 'projectCityIdentity':
                    this.setState(produce(draftState => {
                        draftState.siteLoad.currentProject =  2
                    }));
                    break;
                case 'projectEPub':
                    this.setState(produce(draftState => {
                        draftState.siteLoad.currentProject =  3
                    }));
                    break;
                case 'projectCorporateWebsite':
                    this.setState(produce(draftState => {
                        draftState.siteLoad.currentProject =  4
                    }));
                    break;
                case 'projectSmallBusinessLogo':
                    this.setState(produce(draftState => {
                        draftState.siteLoad.currentProject =  5
                    }));
                    break;
                case 'previousClientsSites':
                    this.setState(produce(draftState => {
                        draftState.siteLoad.currentProject =  6
                    }));
                    break;
            }
        }.bind(inParentComponent),

        handleCarouselPosition: function(loadedPage): void{
            if(loadedPage === '/'){
                loadedPage = 'homePage';
            }else if(loadedPage === '/Clients'){
                loadedPage = 'clientsPage'
            }
            
            setTimeout(() => {
                this.setState(produce(this.state, draftState => {
                    draftState.siteLoad.pageLoaded =  loadedPage
                }));
            
                this.state.handleCarouselPos(loadedPage);
            }, 1250);
        }.bind(inParentComponent),

        handleCarouselDimensions: function(): void {
            document.documentElement.style.setProperty(
                '--carousel-height',
                this.state.carouselHeight
            );
        }.bind(inParentComponent),

        slideDimensions: function(): void{
            const image = document.getElementById('Handmade')?.getElementsByTagName("a")[0].getElementsByTagName("img")[0];
            const height = image?.offsetHeight;

            if(height === 0 || height === undefined){
                const refreshContent = setInterval(function(height) {
                    const image = document.getElementById('Handmade')?.getElementsByTagName("a")[0].getElementsByTagName("img")[0];
                    height = image?.offsetHeight;
                    if(height !== undefined && height !== 0){
                        clearInterval(refreshContent);
                        this.state.checkHeight(height);
                    }
                }.bind(inParentComponent),1250);
            }else{
                this.state.checkHeight(height);
            }
        }.bind(inParentComponent),

        checkHeight: function(height): void{
            this.setState(produce(this.state.introSlider, draftState => {
                draftState.introSlider = {
                    introHeight: height + 'px',
                    introTop: '-' + height + 'px',
                    temporaryImage: false
                }
            }));
        
        const slideHeight:string = height.toString();
        document.documentElement.style.setProperty(
            '--slide-height',
            slideHeight
        );
    }.bind(inParentComponent),

    handleChange: function(event) {
        if(event.currentTarget.name === "messageSubject"){
            this.setState({ messageSubject: event.currentTarget.value});
        }else if(event.currentTarget.name === "messageFrom"){
            this.setState({ messageFrom: event.currentTarget.value });
        }else if(event.currentTarget.name === "messageBody"){
            this.setState({ messageBody: event.currentTarget.value });
        }
    }.bind(inParentComponent),

    calcScreenDimensions: function(eventType){
        if(eventType === 'resized'){
            this.setState(produce(this.state, draftState => {
                draftState.previousClients.hidden =  true
            }));

            this.state.handleScreenWidth();

            this.setState(produce(this.state, draftState => {
                draftState.previousClients.hidden =  false
            }));
        }else{
            this.state.handleScreenWidth();
        }
    }.bind(inParentComponent),

    handleScreenWidth: function(){
        const currentWindowWidth = window.innerWidth;
        const currentWindowWidthToString:string = currentWindowWidth.toString();

        document.documentElement.style.setProperty(
            '--screen-width', currentWindowWidthToString
        );
    }.bind(inParentComponent),

    handleBaseUrl: function(){
        switch(window.location.host) {
            case 'http:': 
            this.setState({ baseUrl: "http://jenniferersoz.com"});
            break;
            case 'https:': 
            this.setState({ baseUrl: "https://jenniferersoz.com"});
            break;
            case 'localhost:9000': 
            this.setState({ baseUrl: "https://localhost:9000"});
            break;
            default:
            this.setState({ baseUrl: "https://jenniferersoz.com"});
            break;
        }
    }.bind(inParentComponent),

        sendMessage: async function(event): Promise<void> {
            event.preventDefault();
            const smtpWorker: SMTP.Worker = new SMTP.Worker();
            await smtpWorker.sendMessage(
                this.state.messageTo,
                this.state.messageFrom,
                this.state.messageSubject,
                this.state.messageBody
            );
        }.bind(inParentComponent),

        handleEmblaImages: function(project, windowWidth){
            
            for(let imageId = 0; imageId < [...this.state[`${project}`]].length; imageId++){

                let imageHeight = [...this.state[`${project}`]][imageId].height;

                let imageDimensionRatio = 100 / [...this.state[`${project}`]][imageId].width;

                imageDimensionRatio = imageDimensionRatio * window.innerWidth;

                imageDimensionRatio = imageDimensionRatio / 100;
                imageHeight = imageHeight * imageDimensionRatio;


                setTimeout(() => {
                    this.setState(produce(this.state, draftState => {
                        draftState[`${project}`][imageId].width = windowWidth;
                        draftState[`${project}`][imageId].height = imageHeight;
                    }));
                }, 1250);
            }
        }.bind(inParentComponent),

        handleEmbla: function() {
            const windowWidth = window.innerWidth;
            if(windowWidth < 600){
                console.log('test');

                this.state.handleEmblaImages('projectHandmade', windowWidth);
                this.state.handleEmblaImages('projectBertrand', windowWidth);
                this.state.handleEmblaImages('projectCityIdentity', windowWidth);
                this.state.handleEmblaImages('projectEPub', windowWidth);
                this.state.handleEmblaImages('projectCorporateWebsite', windowWidth);
                this.state.handleEmblaImages('projectSmallBusinessLogo', windowWidth);
                this.state.handleEmblaImages('previousClientsSites', windowWidth);
            }
        }.bind(inParentComponent)
    }
}