import "../scss/appStyles.scss";

import "../fonts/woff/Raleway-bold-webfont.woff";
import "../fonts/woff/raleway-regular-webfont.woff";
import "../fonts/woff2/raleway-bold-webfont.woff2";
import "../fonts/woff2/raleway-regular-webfont.woff2";

import "../img/clients/account-technologies.jpg"; 
import "../img/clients/arts-business-initiatives.jpg";
import "../img/clients/ascot-study.jpg";
import "../img/clients/astro-infused-water.jpg";
import "../img/clients/dimitris-papanastasiou.jpg";
import "../img/clients/dogs-and-cats-painted.jpg";
import "../img/clients/feather-flair.jpg";
import "../img/clients/foundation-for-circulatory-health.jpg";
import "../img/clients/furthur-progressions.jpg";
import "../img/clients/health-n-wellbeing.jpg";
import "../img/clients/home-grown-textiles.jpg";
import "../img/clients/parsley-and-thyme.jpg";
import "../img/clients/profile-image-square.jpg";
import "../img/clients/red-wine-villas.jpg";
import "../img/clients/southern-belle-constructions.jpg";
import "../img/clients/tappily.jpg";
import "../img/clients/the-bridge-inn-amberley.jpg";
import "../img/clients/vapoureze.jpg";

import "../img/shillington/bertrand-project-back-cover.jpg";
import "../img/shillington/bertrand-project-front-cover.jpg";
import "../img/shillington/bertrand-project-pages-2-3.jpg";
import "../img/shillington/bertrand-project-pages-4-5.jpg";
import "../img/shillington/bertrand-project-pages-6-7.jpg";
import "../img/shillington/city-identity-booking-page.jpg";
import "../img/shillington/city-identity-contact-page.jpg";
import "../img/shillington/city-identity-homepage.jpg";
import "../img/shillington/city-identity-project-filters.jpg";
import "../img/shillington/corporate-calculation-page.png";
import "../img/shillington/corporate-website-homepage.png";
import "../img/shillington/corporate-website-previous-savings.png";
import "../img/shillington/corporate-website-savings-page.png";
import "../img/shillington/epub-magazine-feature.jpg";
import "../img/shillington/epub-magazine-homepage.jpg";
import "../img/shillington/epub-magazine-introduction.jpg";
import "../img/shillington/half-epub.jpg";
import "../img/shillington/hamburger.svg";
import "../img/shillington/handmade-book-cover-back.jpg";
import "../img/shillington/handmade-book-cover-front.jpg";
import "../img/shillington/handmade-book-cover-front-back.jpg";
import "../img/shillington/handmade-project-slide.jpg";
import "../img/shillington/handmade-project-slide-mobile.png";
import "../img/shillington/jennifer-ersoz-jones.png";
import "../img/shillington/quarter-size-ipad-epub.png";
import "../img/shillington/small-business-logo-login.jpg";
import "../img/shillington/small-business-logo-login-entered.jpg";
import "../img/shillington/small-business-logo-login-error.jpg";
import "../img/shillington/small-business-logo-tooltips.jpg";
import "../img/shillington/spark-2ipad-layouts.png";
import "../img/shillington/SPARK3ipad-layouts.png";
import "../img/shillington/spark4ipad-layouts.png";
import "../img/shillington/sparkipad-layouts.png";

import "../img/blog/gbk.jpg";
import "../img/blog/michael-craig-martin.jpg";
import "../img/blog/my-logo-redesigned-day-1.png";
import "../img/blog/safe_image.jpg";

//social image imports
import "../img/social/f_logo_RGB-Blue_100.png";
import "../img/social/linkedin.png";
import "../img/social/Twitter_Social_Icon_Circle_Color.png";

//import favicons
import "../img/favicon/android-chrome-36x36.png";
import "../img/favicon/android-chrome-48x48.png";
import "../img/favicon/android-chrome-72x72.png";
import "../img/favicon/android-chrome-96x96.png";
import "../img/favicon/android-chrome-144x144.png";
import "../img/favicon/android-chrome-192x192.png";
import "../img/favicon/android-chrome-256x256.png";
import "../img/favicon/android-chrome-384x384.png";
import "../img/favicon/android-chrome-512x512.png";
import "../img/favicon/apple-touch-icon.png";
import "../img/favicon/apple-touch-icon-57x57.png";
import "../img/favicon/apple-touch-icon-60x60.png";
import "../img/favicon/apple-touch-icon-72x72.png";
import "../img/favicon/apple-touch-icon-76x76.png";
import "../img/favicon/apple-touch-icon-114x114.png";
import "../img/favicon/apple-touch-icon-120x120.png";
import "../img/favicon/apple-touch-icon-144x144.png";
import "../img/favicon/apple-touch-icon-152x152.png";
import "../img/favicon/apple-touch-icon-180x180.png";
import "../img/favicon/favicon.ico";
import "../img/favicon/favicon-16x16.png";
import "../img/favicon/favicon-32x32.png";
import "../img/favicon/favicon-194x194.png";
import "../img/favicon/mstile-70x70.png";
import "../img/favicon/mstile-144x144.png";
import "../img/favicon/mstile-150x150.png";
import "../img/favicon/mstile-310x150.png";
import "../img/favicon/mstile-310x310.png";
import "../img/favicon/safari-pinned-tab.svg";

import React from "react";
import { createRoot } from "react-dom/client";

import BaseLayout from "./components/BaseLayout";

const rootElement = document.getElementById("root");

const root = createRoot(rootElement);

root.render(
    <React.StrictMode>
        <BaseLayout />
    </React.StrictMode> 
);