

declare module "*.ico" {
    const value: string;
    export = value;
}

declare module '*.png' {
    const value: string;
    export = value;
  }

  declare module '*.jpg' {
    const value: string;
    export = value;
  }

  declare module '*.svg' {
    const value: string;
    export = value;
  }

  declare module '*.woff'  {
    const value: string;
    export = value;
  }
  declare module '*.woff2' {
    const value: string;
    export = value;
  }
  declare module '*.ttf' {
    const value: string;
    export = value;
  }

