import { HTMLAttributes } from "react";

declare module 'react' {
    interface SourceHTMLAttributes<T> extends HTMLAttributes<T> {
      // extends React's HTMLAttributes
      width?: number;
      height?: number;
      alt?: string; 
    }
    

}