import React, { Component, Suspense } from "react";
import { LocationProvider, Match } from 'react-location';
import {ErrorBoundary} from 'react-error-boundary'
const Home = React.lazy(() => import("./layoutComponents/Home"));
const Clients = React.lazy(() => import("./layoutComponents/Clients"));
const Sales = React.lazy(() => import("./layoutComponents/Sales"));
const Blog = React.lazy(() => import("./layoutComponents/Blog"));
const NonExistent = React.lazy(() => import("./layoutComponents/NonExistent"));
import { createState } from "../state";

class BaseLayout extends Component {

    state = createState(this);

    componentDidMount(): void{
        this.state.handleEmbla();
        window.addEventListener('resize', () =>{this.state.handleEmbla();});
        this.state.calcScreenDimensions(); 
        this.state.handleBaseUrl();
        this.state.handleCarouselDimensions();
        window.addEventListener('resize', () =>{this.state.calcScreenDimensions('resized');});
        let pathName = window.location.pathname;
        pathName = pathName.toString();
        this.state.handleCarouselPosition(pathName);
        if(window.location.pathname === "/"){
            this.state.slideDimensions(); 
            window.addEventListener('resize', () =>{this.state.slideDimensions();});
        }
        if(window.location.pathname === "/Clients" && window.innerWidth > 600){
                this.state.handleCarouselDimensions();
        }
        this.state.handleReloadedPage();
        this.setState({isMounted: true});
        this.setState({isUnmounted: false});
    }

    componentWillUnmount(): void{
        this.setState({isMounted: false});
        this.setState({isUnmounted: true});
    }



    render(): any{

        function ErrorFallback({error, resetErrorBoundary}){
            return (
                <div role="alert">
                    <p>Something went wrong:</p>
                    <pre>{error.message}</pre>
                    <button onClick={resetErrorBoundary}>Try again</button>
                </div>
            )
        }

        return(
            <ErrorBoundary FallbackComponent={ErrorFallback} onReset={() => { location.reload(); }}>
                <Suspense fallback={<div>Loading...</div>}>
                <LocationProvider>
                    <Match path="/NonExistent">
                        <NonExistent state={this.state} />
                    </Match>
                    <Match path="/Blog">
                        <Blog state={this.state} />
                    </Match>
                    <Match path="/Sales">
                        <Sales state={this.state} />
                    </Match>
                    <Match path="/Clients">
                        <Clients state={this.state} />
                    </Match>
                    <Match path="/">
                        <Home state={this.state} />
                    </Match>
                </LocationProvider>
                </Suspense>
            </ErrorBoundary>
        )
    }
}

export default BaseLayout;