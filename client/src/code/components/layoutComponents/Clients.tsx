import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Hidden = React.lazy(() => import('@material-ui/core/Hidden'));
const Header = React.lazy(() => import("./Header"));
const ClientsCarousel = React.lazy(() => import('./projectsComponents/Carousels/ClientsCarousel'));
const ClientsIntro = React.lazy(() => import('./projectsComponents/Intros/ClientsIntro'));
const ProfileIntro = React.lazy(() => import('./profileComponents/ProfileIntro'));
const Footer = React.lazy(() => import("./Footer"));
const ClientsSlider = React.lazy(() => import('./projectsComponents/Sliders/ClientsSlider'));

const Clients = ({state}:any): any => (
    <Grid>
        <Grid className="header">
            <Suspense fallback={<div>Loading...</div>}>
                <Header state={ state } />
            </Suspense>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <ClientsIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={5} lg={5} xl={5}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <ClientsCarousel state={state} />
                </Suspense>
            </Hidden> 
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <ClientsSlider state={state} /> 
                </Suspense>
            </Hidden>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <ProfileIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <picture>
                    <source srcSet={state.profile.webp} type="image/webp" alt={state.profile.alt} width={state.profile.imageWidth} height={state.profile.imageHeight} />
                    <img className="rectangleImage profileImage" key={state.profile.image} src={state.profile.image} alt={state.profile.alt} width={state.profile.imageWidth} height={state.profile.imageHeight} />
                </picture>
            </Grid>
        </Grid>
        <Grid className="Footer">
            <Suspense fallback={<div>Loading...</div>}>
                <Footer state={ state } />
            </Suspense>
        </Grid>
    </Grid>
);

export default Clients;