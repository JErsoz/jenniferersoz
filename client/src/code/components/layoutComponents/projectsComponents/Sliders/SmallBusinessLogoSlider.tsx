import React, { useEffect, useState, useCallback } from "react";
import { useEmblaCarousel } from 'embla-carousel/react';

const SmallBusinessLogoSlider = ({state}: any): any => {
  const [selectedIndex, setSelectedIndex] = useState(0);
  const [mainViewportRef, embla] = useEmblaCarousel({ loop: true });
  const [thumbViewportRef, emblaThumbs] = useEmblaCarousel({
    containScroll: "keepSnaps",
    selectedClass: ""
  });

  const onThumbClick = useCallback(
    (index) => {
      if (!embla || !emblaThumbs) return;
      if (emblaThumbs.clickAllowed()) embla.scrollTo(index);
    },
    [embla, emblaThumbs]
  );

  const onSelect = useCallback(() => {
    if (!embla || !emblaThumbs) return;
    setSelectedIndex(embla.selectedScrollSnap());
    emblaThumbs.scrollTo(embla.selectedScrollSnap());
  }, [embla, emblaThumbs, setSelectedIndex]);

  useEffect(() => {
    if (!embla) return;
    onSelect();
    embla.on("select", onSelect);
  }, [embla, onSelect]);

  return (
    <div className="embla">
    <div className="embla__viewport" ref={mainViewportRef}>
      <div className="embla__container">
      {state.projectSmallBusinessLogo.slice(0, 4).map((projectItem, index) => {
          return (
            <div className="embla__slide" key={`slide-${index}`}>
            <div className="embla__slide__inner" style={{ height: projectItem.height}} >   
              <picture>
                  <source srcSet={projectItem.webp} type="image/webp" width={projectItem.width} height={projectItem.height} className="embla__slide__img" />
                  <img src={projectItem.image} alt={projectItem.alt} width={projectItem.width} height={projectItem.height} className="embla__slide__img" />
              </picture>
            </div>
          </div>
          )
          })}
      </div>
    </div>
    <div className="embla embla--thumb">
        <div className="embla__viewport" ref={thumbViewportRef}>
          <div className="embla__container embla__container--thumb">
          {state.projectSmallBusinessLogo.slice(0, 4).map((projectItem, index) => {
            return (
                <div className={`embla__slide embla__slide--thumb ${index === selectedIndex ? "is-selected" : ""}`} key={`${index}`}>
                <button className={`embla__dot ${index === selectedIndex ? "is-selected" : ""}`} type="button" onClick={() => onThumbClick(index)} ></button>
              </div>
            )
          })}
          </div>
        </div>
      </div>
  </div>
  );
}

export default SmallBusinessLogoSlider;
