import React from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
import { LocationProvider, Link } from 'react-location';

const ClientsCarousel = ({state}: any): any => (
<Grid id="content-carousel">
    <Grid id="carousel">
        <Grid id="carousel-mask">
            <ul id="Clients-Carousel" className="Clients-Carousel">
                {state.previousClientsSites.map((projectItem, index) => {
                    return (     
                        <li id={projectItem.title} key={projectItem.key} className={`${projectItem.active ? 'previousClientsSites-active' : 'clients-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}`}>
                            <LocationProvider>
                                <Link to={window.location.pathname} hash="/#" onClick={(e) => state.handleCarouselLink(e, index, projectItem.projectId)} className={`${projectItem.active ? 'previousClientsSites-active' : 'clients-inactive'}`}> 
                                    <picture>
                                        <source srcSet={projectItem.webp} type="image/webp" width={projectItem.width} height={projectItem.height} />
                                        <img src={projectItem.image} alt={projectItem.alt} width={projectItem.width} height={projectItem.height} />
                                    </picture>
                                </Link>
                            </LocationProvider>
                            <span>
                            { projectItem.hasLink && projectItem.linkActive? <p>Active URL: <a className="clientsLink" rel="noreferrer" href={`${projectItem.link.toString()}`} target="_blank">{projectItem.link}</a></p>: ''}
                            { projectItem.hasLink && !projectItem.linkActive? <p>Expired URL: {projectItem.link}</p>: ''}
                            { projectItem.hasWayBackLink? <p>WayBackMachine link: <a className="clientsLink" rel="noreferrer" href={`${projectItem.wayBackLink.toString()}`} target="_blank">{projectItem.wayBackLink}</a></p> : '' }
                            </span>
                        </li>
                    )
                })}
            </ul>
        </Grid>
    </Grid>
</Grid>
);

export default ClientsCarousel;