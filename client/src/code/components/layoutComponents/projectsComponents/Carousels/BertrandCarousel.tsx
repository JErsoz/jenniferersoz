import React from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
import { LocationProvider, Link } from 'react-location';

const BertrandCarousel = ({state}: any): any => (
<Grid id="content-carousel">
    <Grid id="carousel">
        <Grid id="carousel-mask">
            <ul id="Bertrand-Carousel" className="Bertrand-Carousel">
                {state.projectBertrand.map((projectItem, index) => {
                    return (     
                        <li id={projectItem.title} key={projectItem.key} className={`${projectItem.active ? 'projectBertrand-active' : 'bertrand-inactive'} ${projectItem.goBackwards ? 'expand-to-right-li' : ''} ${projectItem.addAnimation ? 'detract-to-left-li' : ''}`}>
                            <LocationProvider>
                                <Link to={window.location.pathname} hash="/#" onClick={(e) => state.handleCarouselLink(e, index, projectItem.projectId)} className={`${projectItem.active ? 'projectBertrand-active' : 'bertrand-inactive'}`}>  
                                    <picture>
                                        <source srcSet={projectItem.webp} type="image/webp" width={projectItem.width} height={projectItem.height} />
                                        <img src={projectItem.image} alt={projectItem.alt} width={projectItem.width} height={projectItem.height} />
                                    </picture>
                                </Link>
                            </LocationProvider>
                        </li>
                    )
                })}
            </ul>
        </Grid>
    </Grid>
</Grid>
);

export default BertrandCarousel;