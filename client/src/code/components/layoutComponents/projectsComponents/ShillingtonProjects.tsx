import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Hidden = React.lazy(() => import('@material-ui/core/Hidden'));
const HandmadeIntro = React.lazy(() => import('./Intros/HandmadeIntro'));
const BertrandIntro = React.lazy(() => import('./Intros/BertrandIntro'));
const CityIdentityIntro = React.lazy(() => import('./Intros/CityIdentityIntro'));
const EPubIntro = React.lazy(() => import('./Intros/EPubIntro'));
const CorporateWebsiteIntro = React.lazy(() => import('./Intros/CorporateWebsiteIntro'));
const SmallBusinessLogoIntro = React.lazy(() => import('./Intros/SmallBusinessLogoIntro'));
const HandmadeCarousel = React.lazy(() => import('./Carousels/HandmadeCarousel'));
const BertrandCarousel = React.lazy(() => import('./Carousels/BertrandCarousel'));
const CityIdentityCarousel = React.lazy(() => import('./Carousels/CityIdentityCarousel'));
const EPubCarousel = React.lazy(() => import('./Carousels/EPubCarousel'));
const CorporateWebsiteCarousel = React.lazy(() => import('./Carousels/CorporateWebsiteCarousel'));
const SmallBusinessLogoCarousel = React.lazy(() => import('./Carousels/SmallBusinessLogoCarousel'));
const HandmadeSlider = React.lazy(() => import('./Sliders/HandmadeSlider'));
const BertrandSlider = React.lazy(() => import('./Sliders/BertrandSlider'));
const CityIdentitySlider = React.lazy(() => import('./Sliders/CityIdentitySlider'));
const EPubSlider = React.lazy(() => import('./Sliders/EPubSlider'));
const CorporateWebsiteSlider = React.lazy(() => import('./Sliders/CorporateWebsiteSlider'));
const SmallBusinessLogoSlider = React.lazy(() => import('./Sliders/SmallBusinessLogoSlider'));



const ShillingtonProjects = ({state}: any): any => (
    <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>

        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <HandmadeIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={5} lg={5} xl={5}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <HandmadeCarousel state={state} />
                </Suspense>
            </Hidden> 
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <HandmadeSlider state={state} /> 
                </Suspense>
            </Hidden>
        </Grid>

        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <BertrandIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={5} lg={5} xl={5}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <BertrandCarousel state={state} />
                </Suspense>
            </Hidden> 
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <BertrandSlider state={state} /> 
                </Suspense>
            </Hidden>
        </Grid>

        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <CityIdentityIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={5} lg={5} xl={5}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <CityIdentityCarousel state={state} />
                </Suspense>
            </Hidden> 
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <CityIdentitySlider state={state} /> 
                </Suspense>
            </Hidden>
        </Grid>

        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <EPubIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={5} lg={5} xl={5}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <EPubCarousel state={state} />
                </Suspense>
            </Hidden> 
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <EPubSlider state={state} /> 
                </Suspense>
            </Hidden>
        </Grid>


        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <CorporateWebsiteIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={5} lg={5} xl={5}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <CorporateWebsiteCarousel state={state} />
                </Suspense>
            </Hidden> 
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <CorporateWebsiteSlider state={state} />
                </Suspense> 
            </Hidden>
        </Grid>

        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={5} lg={5} xl={5}>
                <Suspense fallback={<div>Loading...</div>}>
                    <SmallBusinessLogoIntro state={state} />
                </Suspense>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={5} lg={5} xl={5}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <SmallBusinessLogoCarousel state={state} />
                </Suspense>
            </Hidden> 
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <SmallBusinessLogoSlider state={state} /> 
                </Suspense>
            </Hidden>
        </Grid>
    </Grid>
);

export default ShillingtonProjects;