import React from "react";

const BertrandIntro = ({state}: any): any => (
    <div className="introStyling">
        <h2 dangerouslySetInnerHTML={{__html: `${state.projectsIntro[1].title}`}} />
        <p dangerouslySetInnerHTML={{__html: `${state.projectsIntro[1].content}`}} />
    </div>
);

export default BertrandIntro;