import React from "react";

const EPubIntro = ({state}: any): any => (
    <div className="introStyling">
        <h2 dangerouslySetInnerHTML={{__html: `${state.projectsIntro[3].title}`}} />
        <p dangerouslySetInnerHTML={{__html: `${state.projectsIntro[3].content}`}} />
    </div>
);

export default EPubIntro;