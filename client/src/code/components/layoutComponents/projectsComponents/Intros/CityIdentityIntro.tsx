import React from "react";

const CityIdentityIntro = ({state}: any): any => (
    <div className="introStyling">
        <h2 dangerouslySetInnerHTML={{__html: `${state.projectsIntro[2].title}`}} />
        <p dangerouslySetInnerHTML={{__html: `${state.projectsIntro[2].content}`}} />
    </div>
);

export default CityIdentityIntro;