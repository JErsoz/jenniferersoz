import React from "react";

const HandmadeIntro = ({state}: any): any => (
    <div className="introStyling">
        <h2 dangerouslySetInnerHTML={{__html: `${state.projectsIntro[0].title}`}} />
        <p dangerouslySetInnerHTML={{__html: `${state.projectsIntro[0].content}`}} />
    </div>
);

export default HandmadeIntro;