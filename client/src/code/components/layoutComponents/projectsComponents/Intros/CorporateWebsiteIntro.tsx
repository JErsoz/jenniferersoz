import React from "react";

const CorporateWebsiteIntro = ({state}: any): any => (
    <div className="introStyling">
        <h2 dangerouslySetInnerHTML={{__html: `${state.projectsIntro[4].title}`}} />
        <p dangerouslySetInnerHTML={{__html: `${state.projectsIntro[4].content}`}} />
    </div>
);

export default CorporateWebsiteIntro;