import React from "react";

const ClientsIntro = ({state}: any): any => (
    <div className="introStyling">
        <h1 className="introHeading" dangerouslySetInnerHTML={{__html: `${state.previousClients.heading}`}} />
        <h2 dangerouslySetInnerHTML={{__html: `${state.previousClients.title}`}} />
        <span dangerouslySetInnerHTML={{__html: `${state.previousClients.content}`}} />
    </div>
);

export default ClientsIntro;