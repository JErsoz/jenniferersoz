import React from "react";

const SmallBusinessLogoIntro = ({state}: any): any => (
    <div className="introStyling">
        <h2 dangerouslySetInnerHTML={{__html: `${state.projectsIntro[5].title}`}} />
        <p dangerouslySetInnerHTML={{__html: `${state.projectsIntro[5].content}`}} />
    </div>
);

export default SmallBusinessLogoIntro;