import React from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
import { LocationProvider, Link } from 'react-location';

const MobileImage = ({state}: any): any => (
<Grid className="mobileProjectSummary">
    <LocationProvider>
        <Link to={state.introCarousel[0].link} onClick={state.handleLink}>            
            <picture>
                <source srcSet={state.introCarousel[0].mobileWebp} type="image/webp" width={state.introCarousel[0].width} height={state.introCarousel[0].height} />
                <img src={state.introCarousel[0].mobileImage} alt={state.introCarousel[0].alt} width={state.introCarousel[0].width} height={state.introCarousel[0].height}/>
            </picture>
        </Link>
    </LocationProvider>
</Grid>
);

export default MobileImage;