import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Hidden = React.lazy(() => import('@material-ui/core/Hidden'));
const HeaderMenu = React.lazy(() => import("./headerComponents/HeaderMenu"));
const MobileMenu = React.lazy(() => import("./headerComponents/MobileMenu"));

const Header = ({state}: any): any => (
    <Grid container className="headerWrapper">
        <Grid container direction="column" item={true} xs={12} sm={12} md={12} lg={12} xl={12} alignItems="center">
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <MobileMenu state={state} /> 
                </Suspense>
            </Hidden>
            <picture>
                <source srcSet={state.logoImages.webp} type="image/webp" width={state.logoImages.width} height={state.logoImages.height} className="rectangleImage" />
                <img className="rectangleImage" alt="Jennifer Ersoz-Jones" src={state.logoImages.image} width={state.logoImages.width} height={state.logoImages.height} />
            </picture>
            
            <Hidden only="xs">
                <Suspense fallback={<div>Loading...</div>}>
                    <HeaderMenu state={state} />
                </Suspense>
            </Hidden> 
        </Grid>
    </Grid>
);

export default Header;