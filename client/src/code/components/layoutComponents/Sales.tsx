import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Hidden = React.lazy(() => import('@material-ui/core/Hidden'));
const Header = React.lazy(() => import("./Header"));
const Footer = React.lazy(() => import("./Footer"));

const Sales = ({state}: any): any => (
    <Grid>
        <Grid className="header">
            <Suspense fallback={<div>Loading...</div>}>
                <Header state={ state } />
            </Suspense>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12} className="salesHeader">
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Hidden only="xs">
                <Grid container item={true} sm={10} md={8} lg={8} xl={8} alignItems="flex-end" direction="column" className="salesTop">
                    <picture className="salesImage">
                        <source className="salesImage" srcSet={state.salesImages.webp} type="image/webp" width={state.salesImages.width} height={state.salesImages.height} />
                        <img className="salesImage" src={state.salesImages.image} width={state.salesImages.width} height={state.salesImages.height} />
                    </picture>
                </Grid>
            </Hidden>
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Grid container item={true} xs={11} alignItems="flex-end" direction="column" className="salesTop">
                    <picture className="salesImage">
                        <source className="salesImage" srcSet={state.salesImages.mobileWebp} type="image/webp" width={state.salesImages.width} height={state.salesImages.height} alt={state.salesImages.alt} />
                        <img className="salesImage" src={state.salesImages.mobileImage} width={state.salesImages.width} height={state.salesImages.height} alt={state.salesImages.alt} />
                    </picture>
                </Grid>
            </Hidden>
            <Grid container item={true} sm={1} md={2} lg={2} xl={2}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12} className="salesIntro">
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={7} lg={7} xl={7}>
                <div className="introStyling">
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[0].title}`}} />
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[0].content}`}} />
                </div>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={3} lg={3} xl={3}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={7} lg={7} xl={7}>
                <div className="introStyling">
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[1].title}`}} />
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[1].content}`}} />
                </div>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={3} lg={3} xl={3}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={7} lg={7} xl={7}>
                <div className="introStyling">
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[2].title}`}} />
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[2].content}`}} />
                </div>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={3} lg={3} xl={3}></Grid>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item={true} xs={10} sm={10} md={7} lg={7} xl={7}>
                <div className="introStyling">
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[3].title}`}} />
                    <span dangerouslySetInnerHTML={{__html: `${state.sales[3].content}`}} />
                </div>
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={3} lg={3} xl={3}></Grid>
        </Grid>
        <Grid className="Footer">
            <Suspense fallback={<div>Loading...</div>}>
                <Footer state={ state } />
            </Suspense>
        </Grid>
    </Grid>
);

export default Sales;