import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Header = React.lazy(() => import("./Header"));
const ProjectsSummary = React.lazy(() => import("./ProjectsSummary"));
const Projects = React.lazy(() => import("./Projects"));
const Footer = React.lazy(() => import("./Footer"));

const Home = ({state}: any): any => (
    <Grid>
        <Suspense fallback={<div>Loading...</div>}>
            <Grid className="header">
                <Header state={ state } />
            </Grid>
            <Grid className="projectsSummary">
                <ProjectsSummary state={ state } />
            </Grid>
            <Grid className="projects">
                <Projects state={ state } />
            </Grid>
            <Grid className="Footer">
                <Footer state={ state } />
            </Grid>
        </Suspense>
    </Grid>
);

export default Home;