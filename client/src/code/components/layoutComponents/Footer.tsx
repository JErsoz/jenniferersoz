import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const ContactForm = React.lazy(() => import('./footerComponents/ContactForm'));
const FooterBlog = React.lazy(() => import('./footerComponents/FooterBlog'));
const SocialIcons = React.lazy(() => import('./footerComponents/SocialIcons'));
const Box = React.lazy(() => import('@material-ui/core/Box'));

const Footer = ({ state }: any):any => (
<Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
	<Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
		<Grid container item={true} sm={2} md={2} lg={2} xl={2}></Grid>
		<Grid container item={true} xs={12} sm={5} md={5} lg={5} xl={5}>
			<Grid container direction="column" item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
				<h2>Get in touch:</h2>
				<Suspense fallback={<div>Loading...</div>}>
					<SocialIcons state={state} />
				</Suspense>
				<p><a href="mailto:jennifer.fulya.ersoz@gmail.com">jennifer.fulya.ersoz@gmail.com</a></p>
				<Suspense fallback={<div>Loading...</div>}>
					<ContactForm state={state} />
				</Suspense>
			</Grid>
		</Grid>
		<Grid container item={true} xs={12} sm={3} md={3} lg={2} xl={3}>
			<Suspense fallback={<div>Loading...</div>}>
				<FooterBlog state={state} />
			</Suspense>
		</Grid>
		<Grid container item={true} sm={2} md={2} lg={2} xl={2} ></Grid>
	</Grid>
		<Grid container direction="column" item={true} xs={12} sm={12} md={12} lg={12} xl={12} alignItems="center">
			<Grid container item={true} sm={2} md={2} lg={2} xl={2}></Grid>
			<Grid container direction="column" item={true} sm={8} md={8} lg={8} xl={8} className="copyright" alignItems="center">
				<Box p={2} py={0}>
					<small >&copy; Copyright 2021, Jennifer Ersoz-Jones</small>
				</Box>
			</Grid>
			<Grid container item={true} sm={2} md={2} lg={2} xl={2}></Grid>
		</Grid>	
	</Grid>
);

export default Footer;