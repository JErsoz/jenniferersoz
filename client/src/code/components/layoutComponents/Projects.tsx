import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const ShillingtonProjects = React.lazy(() => import('./projectsComponents/ShillingtonProjects'));

const Projects = ({state}: any): any => (
<Grid container className="projectsSummary">
    <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
        <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
        <Grid container item={true} xs={10} sm={10} md={6} lg={6} xl={6}>
            <h1 className="introHeading" dangerouslySetInnerHTML={{__html: `${state.portfolioTitle}`}} />
            <p dangerouslySetInnerHTML={{__html: `${state.portfolioContent}`}} />
        </Grid>
        <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid> 
    </Grid>
    <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
        <Suspense fallback={<div>Loading...</div>}>
            <ShillingtonProjects state={state} /> 
        </Suspense>
    </Grid>
</Grid>
);

export default Projects;