import React from "react";

const ProfileIntro = ({state}: any): any => (
    <div className="introStyling">
        <h1 className="introHeading" dangerouslySetInnerHTML={{__html: `${state.profile.heading}`}} />
        <h2 dangerouslySetInnerHTML={{__html: `${state.profile.title}`}} />
        <span dangerouslySetInnerHTML={{__html: `${state.profile.content}`}} />
    </div>
);

export default ProfileIntro;