import React, {Suspense} from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Header = React.lazy(() => import("./Header")); 
const Footer = React.lazy(() => import("./Footer"));
const Box = React.lazy(() => import('@material-ui/core/Box'));

const Blog = ({state}: any): any => (
    <Grid>
        <Grid className="header">
            <Suspense fallback={<div>Loading...</div>}>
                <Header state={ state } />
            </Suspense>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid container item={true} xs={1} sm={1} md={2} lg={2} xl={2}></Grid>
            <Grid container item xs={10} sm={10} md={8} lg={8} xl={8}>
            <h1 className="introHeading blogTitle">Blog</h1>
                {state.blogPosts.map(post => {
                    return (                
                    <Grid container item key={post.title} id={post.title} className="introStyling" xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Grid container item xs={12} sm={12} md={12} lg={12} xl={12}>
                            <h2 className="blogTitle">{post.title}</h2>
                        </Grid>
                        <Grid container item xs={12} sm={12} md={12} lg={12} xl={12}>
                            <h3 className="blogDate">{post.date}</h3>
                        </Grid>
                        <Grid container item xs={12} sm={12} md={4} lg={4} xl={4}>    
                            <picture>
                                <source srcSet={post.webp} type="image/webp" width={post.width} height={post.height} />
                                <img className="blogImage" key={post.image} src={post.image} alt={post.alt} width={post.width} height={post.height} />
                            </picture>
                        </Grid>
                        <Grid container item xs={12} sm={12} md={4} lg={4} xl={4}>
                            <Box p={2} py={0}>
                                <span dangerouslySetInnerHTML={{__html: `${post.content}`}} />
                            </Box>
                        </Grid>
                    </Grid>
                    )
                })}
            </Grid>
            <Grid container item={true} xs={1} sm={1} md={1} lg={1} xl={1}></Grid>
        </Grid>
        <Grid className="Footer">
            <Suspense fallback={<div>Loading...</div>}>
                <Footer state={ state } />
            </Suspense>
        </Grid>
    </Grid>
);

export default Blog;