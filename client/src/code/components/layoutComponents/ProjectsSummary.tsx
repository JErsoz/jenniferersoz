import React, { Suspense } from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Hidden = React.lazy(() => import('@material-ui/core/Hidden'));
const MobileImage = React.lazy(() => import('./carouselComponents/MobileImage'));

const ProjectsSummary = ({state}: any): any => (
    <Grid container className="projectsSummary">
        <Grid container direction="column" item={true} xs={12} sm={12} md={12} lg={12} xl={12} alignItems="center">
            <Hidden only={['sm', 'md', 'lg', 'xl']}>
                <Suspense fallback={<div>Loading...</div>}>
                    <MobileImage state={state} />
                </Suspense> 
            </Hidden>
            <Hidden only="xs">
                <picture>
                    <source srcSet={state.introCarousel[0].webp} type="image/webp" width={state.introCarousel[0].width} height={state.introCarousel[0].height} className={`${state.introSlider.temporaryImage ? 'show-temp-image': '' }`} />
                    <img src={state.introCarousel[0].image} className={`${state.introSlider.temporaryImage ? 'show-temp-image': '' }`} width={state.introCarousel[0].width} height={state.introCarousel[0].height} />
                </picture>   
            </Hidden> 
        </Grid>
    </Grid>
);

export default ProjectsSummary;