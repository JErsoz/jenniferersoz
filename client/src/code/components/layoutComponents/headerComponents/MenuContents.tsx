import React from "react";
const Menu = React.lazy(() => import('@material-ui/core/Menu'));
const MenuItem = React.lazy(() => import('@material-ui/core/MenuItem'));
import { LocationProvider, Link } from 'react-location';

const MenuContents = ({state}: any): any => (
    <Menu id="fade-menu" anchorEl={state.mobileMenu.anchorEl} open={state.mobileMenu.open} onClose={state.handleClose}>
        {state.menu.map((menuItem, index) => {
            return(
                <div className="menuWrapper" key={menuItem.link} onClick={() => state.handleMenuLink(index)}>
                    <MenuItem className={ `MenuItem ${menuItem.showLink ? 'show-link' : 'hide-link'}`} onClick={state.handleClose}>
                        <LocationProvider>
                            <Link to={state.baseUrl + menuItem.link} className={ `${menuItem.active ? 'active': ''}` }>
                                {menuItem.title}
                            </Link>
                        </LocationProvider>
                    </MenuItem>  
                </div>
            )
        })

        }
    </Menu>
);

export default MenuContents;