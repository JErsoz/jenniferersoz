import React from "react";
import { LocationProvider, Link } from 'react-location';

const HeaderMenu = ({state}: any): any => (
    <div className="mainMenu">
        {state.menu.map((menuItem, index) => {
            return(
                <div className="menuWrapper" key={menuItem.link} onClick={() => state.handleMenuLink(index)}>
                    <LocationProvider>
                        <Link to={state.baseUrl + menuItem.link} className={  `${menuItem.active ? 'active': ''}` } >
                            {menuItem.title}
                        </Link>
                    </LocationProvider>    
                </div>
            )
        })        
        }
    </div>
);

export default HeaderMenu;