import React, { Suspense } from "react";
const Button = React.lazy(() => import('@material-ui/core/Button'));
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Box = React.lazy(() => import('@material-ui/core/Box'));
const MenuContents = React.lazy(() => import("./MenuContents"));

const MobileMenu = ({state}: any): any => (
    <Grid container item={true} className="menuSection" direction="column" xs={12} sm={12} md={12} lg={12} xl={12}>
        <Box p={1}>
            <Button className="header-menu" aria-owns={state.mobileMenu.open ? 'fade-menu': undefined} aria-haspopup="true" onClick={state.handleClick}>
                <img className="rectangleImage" alt={state.hamburgerImage.alt} src={state.hamburgerImage.src} width={state.hamburgerImage.width} height={state.hamburgerImage.height} />   
            </Button>
            <Suspense fallback={<div>Loading...</div>}>
                <MenuContents state={state} />
            </Suspense> 
        </Box>
    </Grid>
);

export default MobileMenu;