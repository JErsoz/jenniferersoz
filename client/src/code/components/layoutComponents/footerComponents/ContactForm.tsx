import React from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
const Button = React.lazy(() => import('@material-ui/core/Button'));

const ContactForm = ({ state }:any):any => (
    <Grid>
        <ValidatorForm onSubmit={state.sendMessage} onError={errors => console.log(errors)} className='formDisplay' >
            <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
                <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
                    <Grid container item={true} sm={6} md={6} lg={6} xl={6}>
                        <TextValidator classes={{ root: 'TextValidator'}} id="filled-basic" fullWidth size="small" label="Subject" style={{ margin: 8 }} variant="filled" onChange={state.handleChange} name="messageSubject" value={state.messageSubject} validators={['required', 'isString', 'maxStringLength:30']} errorMessages={['this field is required', 'subject is not valid', 'maximum subject length 30 characters']} />
                    </Grid>
                    <Grid container item={true} sm={6} md={6} lg={6} xl={6}>
                        <TextValidator classes={{ root: 'TextValidator'}} id="filled-required" fullWidth size="small" label="Email" style={{ margin: 8 }} variant="filled" onChange={state.handleChange} name="messageFrom" value={state.messageFrom} validators={['required', 'isEmail']} errorMessages={['this field is required', 'invalid email address']} />
                    </Grid>
                </Grid>
                <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12}>
                     <TextValidator classes={{ root: 'TextValidator'}} id="filled-multiline-static" size="small" fullWidth multiline style={{ margin: 8 }} rows={4} label="Message" variant="filled" onChange={state.handleChange} name="messageBody" value={state.messageBody}  validators={['required', 'isString', 'maxStringLength:400']} errorMessages={['this field is required', 'message is not valid', 'maximum message length 400 characters']} />
                     <Button type="submit" style={{ margin: 8}} classes={{ root: 'SubmitButton'}}>Submit</Button>
                </Grid>
</Grid> 
        </ValidatorForm>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12} className='successDisplay'>
            <h3>Thank you. The form has been successfully sent.</h3>
        </Grid>
        <Grid container item={true} xs={12} sm={12} md={12} lg={12} xl={12} className='errorDisplay'>
            <h3>Error. Sorry we cannot process the contact form at the moment. Please try again later.</h3>
        </Grid> 
    </Grid>
);

export default ContactForm;