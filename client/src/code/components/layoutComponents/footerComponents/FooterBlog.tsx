
import React from "react";

import { LocationProvider, Link } from 'react-location';
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
const Box = React.lazy(() => import('@material-ui/core/Box'));

const FooterBlog = ({state}: any): any => (
  <Grid>
<Box p={1} py={0}>              
    <Grid container className="blogSummary" item key={state.blogPosts[0].title} id={state.blogPosts[0].title} xs={12} sm={12} md={12} lg={12} xl={12}>
        <Grid container item xs={12} sm={12} md={12} lg={12} xl={12}>
            <h2 className="blogTitle">Blog:</h2>
        </Grid>
        <Grid container item xs={12} sm={12} md={12} lg={12} xl={12}>
            <Grid onClick={() => state.handleMenuLink(3)}>
                <LocationProvider>
                    <Link to={state.baseUrl + "/Blog"} >
                        <h3 className="blogTitle">{state.blogPosts[0].title}</h3>
                    </Link>
                </LocationProvider>
                <h4 className="blogDate">{state.blogPosts[0].date}</h4>
            </Grid>
        </Grid>
        <Grid container item xs={12} sm={12} md={12} lg={12} xl={12} onClick={() => state.handleMenuLink(3)}>    
            <LocationProvider>
                <Link to={state.baseUrl + "/Blog"} >
                    <picture>
                        <source srcSet={state.blogPosts[0].webp} type="image/webp" width={state.blogPosts[0].width} height={state.blogPosts[0].height} />
                        <img className="blogImage" key={state.blogPosts[0].image} src={state.blogPosts[0].image} alt={state.blogPosts[0].alt} width={state.blogPosts[0].width} height={state.blogPosts[0].height} />
                    </picture>
                </Link>
            </LocationProvider>
        </Grid>
    </Grid>
</Box>
</Grid>

);

export default FooterBlog;