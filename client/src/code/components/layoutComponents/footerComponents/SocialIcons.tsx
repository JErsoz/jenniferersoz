import React from "react";
const Grid = React.lazy(() => import('@material-ui/core/Grid'));
import { LocationProvider, Link } from 'react-location';

const SocialIcons = ({ state }: any):any => (
    <Grid>
        <ul className="socialIcons">
        {state.socialIcons.map(icon => {
            return ( 
                <li key={icon.title}>
                    <LocationProvider>
                        <Link to={icon.link} target="_blank" rel="noopener"> 
                            <picture>
                                <source className="socialImage" srcSet={ icon.webp} type="image/webp" width={icon.width} height={icon.height} />
                                <img className="socialImage" key={icon.image} src={icon.image} alt={icon.alt} width={icon.width} height={icon.height} />
                            </picture>
                        </Link>
                    </LocationProvider>
                </li>
            );
        })}
        </ul>
    </Grid>
);

export default SocialIcons;