const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: "./src/code/main.tsx",
  output: {
    filename: 'js/main.js',
    path: path.join(__dirname, "dist"),
  },
  plugins: [
	  new CleanWebpackPlugin(),
	  new HtmlWebpackPlugin({
		filename: "index.html",
		inject: true,
		template: "./src/index.html", 
		path: path.join(__dirname, "../dist/"),
	  })
  ]
}