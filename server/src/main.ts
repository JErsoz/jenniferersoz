import path from "path";
import fs from "fs";

import https from "https";
import http from "http";

import express, { Express, NextFunction, Request, Response } from "express";
const httpsRedirect = require('express-https-redirect');

import { serverInfo } from "./ServerInfo";
import * as SMTP from "./SMTP";

import { expressCspHeader, INLINE, NONE, SELF} from 'express-csp-header';

const options: any = {
    key:  fs.readFileSync(path.join(__dirname, "../../../Key&Cert/server.key"), 'utf8'),
    cert: fs.readFileSync(path.join(__dirname, "../../../Key&Cert/server.cert"), 'utf8'),
    etag: true,
    maxAge: '31556952000',

    redirect: true,
    setHeaders: function (res: any, path:any, stat: any){
        res.set({
            'x-timestamp': Date.now(),
            'jennifer-ersoz': 'stamped'
        });
        
    }
}
   
const app: Express = express();

app.use(express.json());

app.use('/', httpsRedirect(true));

app.get('*.js', function (req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    res.set('Content-Type', 'application/javascript');
    next();
  });

app.use("/", express.static(path.join(__dirname, "../../client/dist"), options));

app.use(function(inRequest: Request, inResponse: Response, inNext: NextFunction) {
    inResponse.header("Access-Control-Allow-Origin", "*");
    inResponse.header("Access-Control-Allow-Methods", "GET, POST, DELETE, OPTIONS");
    inResponse.header("Access-Control-Allow-Headers", "x-timestamp, jennifer-ersoz, Origin, X-Requested-With, Content-Type, Accept");
    inNext();
});

app.use(expressCspHeader({
    directives: {
        'default-src': [SELF],
        'img-src': [SELF, INLINE, 'https://www.google-analytics.com/'],
        'script-src': [SELF, INLINE, 'https://unpkg.com/react@17.0.1/umd/react.production.min.js', 'https://unpkg.com/react-dom@17.0.1/umd/react-dom.production.min.js', 'https://unpkg.com/axios@0.20.0/dist/axios.min.js', 'https://unpkg.com/immer@7.0.15/dist/immer.umd.production.min.js', 'https://www.google-analytics.com/', 'https://www.googletagmanager.com/', ],
        'style-src': [SELF, INLINE]
    }
}));

app.post("/messages", 
    async(inRequest: Request, inResponse: Response) => {
        try {
            const smtpWorker: SMTP.Worker = new SMTP.Worker(serverInfo);
            await smtpWorker.sendMessage(inRequest.body);
            inResponse.send("ok");

        } catch (inError) {
            inResponse.send("error");
        }
    });

    app.use('/*', (req, res) => {
        res.sendFile(path.join(__dirname,
            '../../client/dist/index.html'), (err) => {
                if(err){
                    console.log('throwing error on server')
                    res.status(500).send(err);
                    
                }
            });
            
    });



    app.get('*', (req, res, next) => {
        res.redirect("https://" + req.headers.hostname + req.path);
    });


    app.use('/*', (req, res, next) => {
        res.status(404).render('error/404.html');
    });
    
    app.listen(8060, "0.0.0.0", () => {
        console.log("server open for requests");
    });    

    http.createServer(app).listen(8070, "0.0.0.0", () => {
        console.log("http server open for requests");
    });


    https.createServer(options, app).listen(8050, "0.0.0.0", () => {
        console.log("https server open for requests");
    });

